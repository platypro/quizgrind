# Version 0.2.1 (In Development)

- Added more seeding tools
- PDF Rendering fixes

# Version 0.2.0 (Released 2022-01-04)

- Added all widgets to PDF
- Version number now embedded into build
- Added basic logging to http daemon
- Fixed segfault when problem set is selected that does not exist.
- Added raster image support

# Version 0.1.0 (Released 2021-12-13)

- Initial Stable Release