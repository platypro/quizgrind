.. _api-reference:

API Documentation
=================

.. toctree::
	:maxdepth: 2
	:titlesonly:
	:caption: API Documentation
	
	widget/index
	json/template
	json/exercise
	json/pset 
	lua

