.. _widget-image:

Image Widget
============

An image widget shows an image to the user. Only SVG images are supported so far.

Example Code
############

.. code-block:: lua
    :linenos:

    -- Create an image from a SVG template

    _mkWidget("image")
        :set_image_svg(_mkTemplate("square.svg", {color="#FF0000"}))

Lua API
#######

.. lua:class:: Widget_Image

    A choice widget created with '_mkWidget("image")'.
    
    .. lua:method:: set_image(source)
    
        Set the image
        
        :param source: The image path relative to the current exercise directory.
        :type source: string

    .. lua:method:: set_image_svg(source)
    
        Set the image to a SVG
        
        :param source: The source of the svg.
        :type source: string or QuizgrindTemplate

JSON Structure
##############
.. jsonschema::

    {
        "title": "Quizgrind Image Widget",
        "description": "An image widget.",
        "type": "object",
        "properties": {
            "src": {
                "type": "string",
                "description": "| The image path relative to the current exercise directory.\n| This option has a higher priority than both svg and svg_template"
            },
            "svg": {
                "type": "string",
                "description": "| The svg source for this widget.\n| This option has a higher priority than svg_template"
            },
            "svg_template": {
                "$ref": "#/definitions/template",
                "description": "| The svg template source for this widget.\n| This option has a lower priority than svg"
            }
        }
    }
