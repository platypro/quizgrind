.. _widget-lines:

Lines Widget
============

A lines widget allows multi-line text input.

Example Code
############

.. code-block:: lua
    :linenos:

    -- Create an long-answer question

    lines = _mkWidget("lines")
    lines.caption = "Write me some text about mailboxes"
    lines.length = 3
    lines.marks = 15
    lines.key = "This answer should be a complete paragraph about mailboxes."

Lua API
#######

.. lua:class:: Widget_Lines

    A lines widget created with '_mkWidget("lines")'.
    
    .. lua:attribute:: length:number

        The minimum length in lines this widget should accommodate.

    .. lua:attribute:: marks:number

        The number of marks that this widget is worth

    .. lua:attribute:: key:string

        Information about the correct answer for this widget.
        
    .. lua:attribute:: key:string

        A caption to show ahead of the text field.

JSON Structure
##############
.. jsonschema::

    {
        "title": "Quizgrind Image Widget",
        "description": "A lines widget.",
        "type": "object",
        "properties": {
            "length": {
                "type": "number",
                "description": "The minimum length in lines this widget should accommodate."
            },
            "marks": {
                "type": "number",
                "description": "The number of marks that this widget is worth"
            },
            "key": {
                "type": "string",
                "description": "Information about the correct answer for this widget."
            },
			"caption": {
                "type": "string",
                "description": "A caption to show ahead of the text field."
            }
        }
    }
