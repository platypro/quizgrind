.. _widget-choice:

Choice Widget
=============

A choice widget gives the user a selection of one or more choices. This widget supports multiple selection, randomization, and image choices.

Example Code
############

.. code-block:: lua
    :linenos:

    -- Create a new choice widget with three items.
    -- Both the second and third items must be selected
    -- but not the first one
    -- for full marks.

    choice = _mkWidget("choice");
    
    choice.numselections = 2;
    choice:add_choice("Choice #1", -1);
    choice:add_choice("Choice #2", 1);
    choice:add_choice("Choice #3", 1);

Lua API
#######

.. lua:class:: Widget_Choice

    A choice widget created with '_mkWidget("choice")'.
    
    .. lua:attribute:: numselections:number

        The total number of selections which the user can select.

    .. lua:attribute:: marks:number
    
        Total number of marks required for full credit. If zero, full credit is the maximum amount possible.
        
    .. lua:attribute:: randomize:boolean
    
        Determines whether elements added to this widget are displayed in a random order.
        
    .. lua:method:: add_choice(text, marks)
    
        Add a new choice to the choice widget.
        
        :param text: The text to show. This argument supports template types.
        :type text: string or QuizgrindTemplate
        
        :param marks: The number of marks this choice awards.
        :type marks: number
        
    .. lua:method:: add_choice_svg(src, marks)
    
        Adds a new choice containing a SVG image.
        
        :param src: The source of the svg. This argument supports template types.
        :type src: string or QuizgrindTemplate
        
        :param marks: The number of marks this choice awards.
        :type marks: number

JSON Structure
##############
.. jsonschema::

    {
        "title": "Quizgrind Choice Widget",
        "description": "A choice widget.",
        "type": "object",
        "properties": {
            "numSelections": {
                "type": "number",
                "description": "The total number of selections which the user can select."
            },
            "randomize": {
                "type": "boolean",
                "description": "Determines whether elements added to this widget are displayed in a random order."
            },
            "choices": {
                "type": "array",
                "description": "The choices available to the user.",
                "items": { "$ref": "#/definitions/widget_choice_choice" }
            },
            "marks":{
                "type": "number",
                "description": "Total number of marks required for full credit. If zero, full credit is the maximum amount possible."
            }
        }
    }
    
.. jsonschema::

    {
        "title": "Choice Widget Entry",
        "type": "object",
        "$$target": "#/definitions/widget_choice_choice",
        "description": "A selectable choice for a choice widget.",
        "properties": {
            "type": {
                "type": "string",
                "description": "Determines type of this choice. Either \"svg\" or \"text\" for wether this choice contains a SVG image or only text."
            },
            "text": {
                "type": "string",
                "description": "The source text used verbatim. If a template property is provided, this property is ignored."
            },
            "template": { 
                "$ref": "#/definitions/template",
                "description": "The template object to generate the contents of this entry from. If provided, this property overrides the \"text\" property."
            },
            "marks":{
                "type": "number",
                "description": "The number of marks this item awards."
            }
        }
    }
