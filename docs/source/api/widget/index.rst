.. _widget-matrix:

Widgets
=======

.. toctree::
   :hidden:

   choice
   element
   image
   lines

=======================  ===========
Widget Name              Description
=======================  ===========
:ref:`widget-choice`     Asks a multiple-choice question
:ref:`widget-element`    Asks a "Fill-in-the-blanks" style question with a mix of text and numerical answers.
:ref:`widget-image`      Shows an image
:ref:`widget-lines`      Accepts a long form multi-line answer.
=======================  ===========
