.. _widget-element:

Element Widget
==============

An element widget contains one or more fields which either display information to the test-taker or contain an input field. 

Example Code
############

.. code-block:: lua
	:linenos:
	
	-- This example adds a new "element" widget which 
	-- asks some questions about apples.
	
	wid = _mkWidget("element")
	
	-- An apple is a type of fruit. The correct answer
	-- here is "fruit" in all lower-case.
	wid:add_text("An apple is a type of ")
	wid:add_input_text()
		:set_caps(true)
		:add_answer("fruit", 2)

	-- This is a word problem asking about how much 
	-- four apples will cost.
	wid:add_text(". If a single apple costs $4.55," ..
		"then four apples will cost $")
	num = wid:add_input_number()
	
	-- The input has two digits after the decimal
	-- point, and has an answer of 18.2 with only 
	-- the exact answer allowed.
	num:set_mindp(2, 1)
	num:add_answer(18.2, 0, 2)
	
	-- Number inputs can accept different number
	-- formats. In this case only basic or 
	-- normalized exponential answers are allowed
	num:formats_add(_NUM_TYPE.BASIC)
	num:formats_add(_NUM_TYPE.NORMALIZED)
	
	-- The display format is what is shown in the 
	-- answer key.
	num:formats_set_display(_NUM_TYPE.BASIC)
	
	wid:add_text(".")
	
	-- The result is an element widget which asks "An 
	-- apple is a type of ______. If a single apple 
	-- costs $4.55, then four apples will cost $______."

Lua API
#######

.. lua:class:: Widget_Element
    
    An element widget created with '_mkWidget("element")'.
    
    .. lua:method:: add_text(text)
    
        Add a new text element to the widget.
        
        :param text: The text to display.
        :type text: string
    
    .. lua:method:: add_input_text()
    
        Add a new text input element to the widget.
        
        :return: The text input object.
        :rtype: Widget_Element_Input_Text
    
    .. lua:method:: add_input_number()
    
        Add a new number input element to the widget.
        
        :return: The number input object.
        :rtype: Widget_Element_Input_Number
        

.. lua:class:: Widget_Element_Input_Text

    A text input element in an element widget.
    
    .. lua:attribute:: caps:boolean

        Determines whether answers are case sensitive
    
    .. lua:method:: add_answer(value, marks)

        Adds an accepted answer to this element.

        :param text: The correct text to input.
        :type text: string
        
        :param marks: The number of marks this answer awards.
        :type marks: number

.. lua:class:: Widget_Element_Input_Number

    A number input element in an element widget

    .. lua:attribute:: mindp:number

        The minimum number of digits past the decimal point or significant figures required. If positive, answer is to the specified number of digits past the decimal point. If negative, answer is to the specified number of significant figures.

    .. lua:attribute:: mindp_penalty:number

        The number of marks removed for answering with the wrong number of decimal points or significant figures to a maximum of all marks.

    .. lua:method:: add_answer(value, error, marks)

        Adds an accepted answer to this element.

        :param value: The correct value
        :type format: number

        :param error: The amount of acceptable error for this answer
        :type format: number

        :param marks: The number of marks this answer awards
        :type format: number

    .. lua:method:: formats_add(format)

        Add an accepted answer format

        :param format: The format to add
        :type format: _NUM_TYPE

    .. lua:method:: formats_clear()

        Reset accepted formats for this element

    .. lua:method:: formats_set_display(format)

        Set the display format for inside solutions.

        :param format: The format to display in solutions
        :type format: _NUM_TYPE

JSON Structure
##############
.. jsonschema::

    {
        "title": "Quizgrind Element Widget",
        "description": "| A widget which contains fields interleaved with informational text.",
        "type": "object",
        "properties": {
            "elements": {
                "type": "array",
                "description": "| An array of all the elements contained within the widget.",
                "items": [
                    { "$ref": "#/definitions/widget_element_text"},
                    { "$ref": "#/definitions/widget_element_input_text"},
                    { "$ref": "#/definitions/widget_element_input_number"}
                ]
            }
        }
    }

.. jsonschema::
    
    {
        "title": "Element Widget Text Element",
        "type": "object",
        "$$target": "#/definitions/widget_element_text",
        "description": "| An element containing some text.",
        "properties": {
            "type": {
                "type": "string",
                "pattern": "text",
                "description": "The type of this widget. Must be set to 'text' in order to be a text element."
            },
            "text": {
                "type": "string",
                "description": "The text to display"
            }
        }
    }

.. jsonschema::
    :lift_definitions:
    :auto_reference:
    :auto_target:
    
    {
        "title": "Element Widget Text Input Element",
        "type": "object",
        "$$target": "#/definitions/widget_element_input_text",
        "description": "An element which requires the user to input some text.",
        "properties": {
            "type": {
                "type": "string",
                "pattern": "input_text",
                "description": "| The type of this widget. Must be set to 'input_text' in order to be a text input element."
            },
            "options": { "$ref": "#/definitions/text_input_options" },
            "answers": {
                "type": "array",
                "description": "| The accepted answers for this widget",
                "items": { "$ref": "#/definitions/text_input_answer" }
            }
        },
        "definitions": {
            "text_input_options": {
                "type": "object",
                "description": "| Options for the text input element",
                "properties": {
                    "caps" : {
                        "type": "string",
                        "description": "| Determines whether the answer is case sensitive"
                    }
                }
            },
            "text_input_answer": {
                "type":"object",
                "properties": {
                    "marks":{
                        "type": "number",
                        "description": "| The number of marks awarded for this answer."
                    },
                    "value":{
                        "type": "number",
                        "description": "| The text value of this answer."
                    }
                }
            }
        }
    }
    
.. jsonschema::
    :lift_definitions:
    :auto_reference:
    :auto_target:
    
    {
        "title": "Element Widget Number Input Element",
        "type": "object",
        "$$target": "#/definitions/widget_element_input_number",
        "description": "| An element which requires the user to input a number.",
        "properties": {
            "type": {
                "type": "string",
                "pattern": "input_number",
                "description": "| The type of this widget. Must be set to 'input_number'\n| in order to be a number input element."
            },
            "options": { "$ref": "#/definitions/number_input_options" },
            "answers": {
                "type": "array",
                "description": "The accepted answers for this widget",
                "items": { "$ref": "#/definitions/number_input_answer" }
            }
        },
        "definitions": {
            "number_input_options": {
                "type": "object",
                "description": "Options for this widget",
                "properties": {
                    "mindp" : {
                        "type": "number",
                        "description": "| The minimum number of digits past the decimal point or significant\n| figures required. If positive, answer is to the specified number of digits\n| past the decimal point. If negative, answer is to the specified number\n| of significant figures."
                    },
                    "mindp_penalty": {
                        "type": "number",
                        "description": "| The number of marks removed for answering with the wrong number\n| of decimal points or significant figures to a maximum of all marks."
                    },
                    "format": {
                        "type": "array",
                        "description": "| The accepted formats for this answer field. Information about available\n| formats is available in :lua:class:`_NUM_TYPE`",
                        "items": [ 
                            {"type":"string", "format":"basic"}, 
                            {"type":"string", "format":"exponential"}, 
                            {"type":"string", "format":"engineering"}, 
                            {"type":"string", "format":"normalized"}, 
                            {"type":"string", "format":"si"}
                        ]
                    },
                    "format_display": {
                        "type": "string",
                        "description": "| The format to display the answer as in the solution."
                    }
                }
            },
            "number_input_answer": {
                "type":"object",
                "properties": {
                    "marks":{
                        "type": "number",
                        "description": "| The number of marks awarded for this answer."
                    },
                    "value":{
                        "type": "number",
                        "description": "| The numeric value of this answer."
                    },
                    "error":{
                        "type": "number",
                        "description": "| The accepted error of this answer.\n| eg. if value=5 and error=0.1 then the answer is acceptable between 4.9 and 5.1 inclusive."
                    }
                }
            }
        }
    }
