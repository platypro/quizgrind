exercise.json Reference
=======================

The exercise.json file contains basic info about the exercise. It is the only file required to implement an exercise. Using the "widgets" property static questions may be created.

.. jsonschema::

    {
        "title": "Exercise Object",
        "type": "object",
        "description": "Contains basic info about an exercise",
        "properties": {
            "name": {
                "type": "string",
                "description": "The name of the exercise."
            },
            "calculator": {
                "type": "string",
                "description": "The recommended type of calculator allowed to solve this exercise.",
                "pattern": "^(none|basic|scientific|graphing)$"
            },
            "preamble": {
            	"type": "string",
            	"description": "| A small piece of hint text to place at the start of the question.\n| If the \"numQuestions\" property is specified in the problem set,\n| this hint text may only be diplayed once for the whole question,\n| and not each individual one. This is useful for PDF quizzes where\n| multiple questions may be generated yet not every question needs\n| to display a copy of the same information."
            },
            "options": {
            	"type": "object",
            	"description": "| The default options for this exercise. These may be overridden by\n| the problem set or within \"init.lua\". The final values are \n| accessible within \"script.lua\" through the \"_options\" global."
            },
            "variables": {
            	"type": "object",
            	"description": "| Numeric values to be generated for each question and forwarded to\n| script.lua. If specified as a number, it signifies the highest\n| value for that variable. For example, if a variable is defined\n| here named \"var1\" with a value of \"5\", \"_variables[\"var1\"]\"\n| will contain a random number from 1 to 5 which you can use in your\n| script. If specified as an object, the variables within will be\n| treated with the opposite \"Variable mode\" as a block. i.e. if\n| the variable mode for this exercise is set as sum, the variable\n| mode within all sub-objects will be product."
            },
            "variable_mode": {
            	"type": "string",
            	"pattern": "^(sum|product)$",
            	"description": "| This property changes how variables are chosen for each question.\n| If specified as \"sum\", variables are treated as mutually\n| exclusive. i.e. only one variable will be set and all others will\n| be zero. It is called \"sum\" since the total number of questions\n| possible are the sum of the variable values.\n| \n| The other option for this is \"product\". If set to this, all\n| combinations of variable values are possible. It is called\n| \"product\" since the total number of questions possible are the\n| product of the variable values."
            },
            "static": {
            	"type": "object",
            	"description": "Static data available within \"init.lua\" and \"script.lua\" through the \"_static\" global",
            },
            "hints": {
            	"type": "array",
            	"items": { "type": "string"	},
            	"description": "Hints for this question, to be revealed in order."
            },
            "widgets": {
            	"type": "array",
            	"items": { "$ref": "#/definitions/exercise_widget" },
            	"description": "| The widgets to populate the question with.\n| Use this to create static questions without any script smarts."
            }
        }
    }

.. jsonschema::

    {
        "title": "Exercise Widget",
        "type": "object",
        "$$target": "#/definitions/exercise_widget",
        "description": "A widget definition. See :ref:`widget-matrix` for available widget types and option syntax.",
        "properties": {
    		"type": {
    			"type": "string",
    			"description": "The widget type."
    		},
      		"options": {
      			"type": "object",
      			"description": "Options for this widget."
      		}
        }
    }