JSON Template Objects
=====================

.. jsonschema::

    {
        "title": "Template Object",
        "type": "object",
        "$$target": "#/definitions/template",
        "description": "Generates a template using an implementation of the `Mustache <https://mustache.github.io/>`_ template system.",
        "properties": {
            "name": {
                "type": "string",
                "description": "| The template to load. QuizGrind will load a file of this name from a subfolder\n| of your exercise named \"template\"."
            },
            "substitutions": {
                "type": "string",
                "description": "| A JSON object with the substitutions to make within the template. All mustache\n| features are available except for \"Partials\" and \"Change Delimiter\"."
            }
        }
    }
