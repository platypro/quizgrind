pset.json reference
===================

.. jsonschema::

    {
        "title": "Problem Set",
        "type": "object",
        "description": "Contans a list of exercises and their options for creating a quiz out of.",
        "properties": {
            "name": {
                "type": "string",
                "description": "The name of the problem set."
            },
            "calculator": {
                "type": "string",
                "description": "The recommended type of calculator allowed to solve this problem set.",
                "pattern": "^(none|basic|scientific|graphing)$"
            },
            "questions": {
           		"type": "array",
           		"description": "The questions in this problem set",
           		"items": { "$ref": "#/definitions/pset_question" }
            }
        }
    }

.. jsonschema::

	{
        "title": "Problem Set Question",
        "type": "object",
        "$$target": "#/definitions/pset_question",
        "description": "A question to generate as part of a problem set",
        "properties": {
        	"id": {
				"type": "string",
                "description": "| The ID of the exercise to generate. \n| The ID is the name of the exercise within the \"exercise\" folder.",
        	},
      		"options": {
      			"type": "object",
      			"description": "Options to override for this exercise."
      		},
      		"numQuestions": {
      			"type": "object",
      			"description": "| The number of questions to generate. If this number is\n| greater than the total possible number of questions for\n| this exercise, then all possible questions will be generated."
      		}
        }
	}