Lua API
=======

This lua API is availble within "init.lua" and "script.lua" which are located within each exercise. Both have slightly different features, which are all documented below:

Functions
#########

.. lua:function:: _mkWidget(typ)
    
    Create a new widget. A matrix of all widgets is available at :ref:`widget-matrix`.

    :param typ: The type of widget to create. Built-in types are \"choice\", \"element\", \"lines\", and \"image\". Return type depends on this argument.
    :type typ: str

    :return: The new widget created. Return type depends on argument.

.. lua:function:: _mkHint(text)

    Add a hint. Each time this is called a new hint is added to the list of hints. In the web interface, these hints will be given in the order created before revealing the solution to the problem.
    
    :param text: The text of the hint.
    :type text: str

.. lua:function:: _mkTemplate(name,substitutions)

    Generate a template using an implementation of the `Mustache <https://mustache.github.io/>`_ template system.

    :param name: The template to load. QuizGrind will load a file of this name from a subfolder of your exercise named "template".
    :type name: str

    :param substitutions: A lua table with the substitutions to make within the template. All mustache features are available except for "Partials" and "Change Delimiter".
    :type substitutions: table

    :return: A template object which may be passed into any quizgrind function which supports it.
    :rtype: QuizgrindTemplate

.. lua:function:: _number_format(num, dp, typ)

    Format a number.
    
    :param num: The number to format.
    :type num: number
    
    :param dp: The number of decimal points to render.
    :type dp: number
    
    :param typ: Number type to format as. See :lua:class:`_NUM_TYPE`
    :type typ: number
    
    :return: The formatted number
    :rtype: str
    
.. lua:function:: _number_parse(str)

    Parse a number.
    
    :param str: The number to parse
    :type str: str
    
    :return: The parsed number
    :rtype: ParsedNumber

.. lua:function:: _unirand_seed(top)

    Initialize a new random number generator which chooses between values such that any value is chosen only once. The script must keep track of how many numbers have already been generated itself.

    :param top: The number of values to choose between.
    :type top: number
    
    :return: A unirand object which may be passed into :lua:meth:`_unirand` to generate numbers.
    :rtype: Unirand

.. lua:function:: _unirand(unirand, idx)

    Generate a new unirand number from a previously seeded unirand object.
    
    :param unirand: The unirand object to generate numbers from.
    :type unirand: Unirand
    
    :param idx: The random number to generate.
    :type idx: number

.. lua:function:: _number_type_toString(number_type)

    Gets the string representation of a :lua:class:`_NUM_TYPE` object.

    :param number_type: The number type to convert.
    :type number_type: number
    
    :return: A string represenation of the number.
    :rtype: str

.. _api-classes:

Classes
#######
.. lua:class:: QuizgrindExercise
    
    A class containing various properties related to the quizgrind exercise. An instance of this class is available through the "_exercise" global within init.lua
    
    .. lua:attribute:: name: str
    
        The name of the exercise
    
    .. lua:attribute:: preamble: str
    
        A piece of text to place before the question. If multiple questions are generated, this message is placed one before any questions are generated.
        
    .. lua:attribute:: calculator: number
    
        The type of calculator allowed for this question. Values for this attribute can be found in the :lua:class:`_CALC_TYPE` enum.
        
    .. lua:attribute:: variable_mode: number
    
        The mode for which variables are generated for this exercise. Values for this attribute can be found in the :lua:class:`_VARIABLE_MODE` enum.

.. lua:class:: ParsedNumber

    A number parsed using :lua:meth:`_number_parse`.
    
    .. lua:attribute:: type:number

        A bitfield containing all the possible types of number which the just parsed number can be. Use a bitwise 'and' to compare with values from :lua:class:`_NUM_TYPE`
        
    .. lua:attribute:: dp:number
    
        The number of digits after the decimal point which were parsed.
    
    .. lua:attribute:: sigfig:number
    
        The number of significant figures parsed.
        
    .. lua:attribute:: value:number
    
        The value of the parsed number.

.. lua:class:: Unirand

    An instance of unirand generated from :lua:meth:`_unirand_seed`. To be used within :lua:meth:`_unirand`

.. lua:class:: QuizgrindTemplate

    A template generated by :lua:meth:`_mkTemplate`

Globals
#######
.. lua:attribute:: _options : table
    
    Contains contents of the "options" object in 'exercise.json', which may be further overridden by values in the json file for the current problem set.

.. lua:attribute:: _static : table
    
    Contains contents of the "static" object in 'exercise.json'

.. lua:attribute:: _variables : table
    
    The variables for this exercise. Within 'init.lua', these override values set within 'exercise.json'. Within 'script.lua' this attribute contains the variables with their instanced values.

.. lua:attribute:: _exercise : QuizgrindExercise

    Global settings for the current exercise. These values may be set within "init.lua" to override 'exercise.json'

Enums
#####
Quizgrind has a number of "enums" which contain several possible values to pass into various functions. These enums are represented as lua tables containing number constants for each enumeration.

.. lua:class:: _CALC_TYPE
    
    Calculator Type.

    .. lua:attribute:: BANNED: number

        Calculators are banned for this exercise

    .. lua:attribute:: NONE: number

        Any calculator may be used for this exercise.

    .. lua:attribute:: BASIC: number

        Only basic (plus,minus,mul,div) calculators allowed for this exercise.

    .. lua:attribute:: GRAPHING: number

        Graphing calculators are allowed for this exercise.

    .. lua:attribute:: SCIENTIFIC: number

        Scientific calculators are allowed for this exercise.

.. lua:class:: _NUM_TYPE

    Number Type

    .. lua:attribute:: BASIC: number

        A basic number. No exponentials or metric suffixes.

    .. lua:attribute:: EXPONENTIAL: number
    
        This type of number is expressed in exponential notation. "eg. 1.234E3, 1.234e3, 1.234x10^3, 1.234*10^3"

    .. lua:attribute:: ENGINEERING: number

        Similar to exponential, but the exponent must be a power of three.

    .. lua:attribute:: NORMALIZED: number

        Similar to exponential, but the mantissa must be >= 1 and <10.

    .. lua:attribute:: SI: number

        This type of number uses metric suffixes (eg. 1.234k = 1234).

.. lua:class:: _VARIABLE_MODE

    Variable Mode
    
    .. lua:attribute:: SUM : number

        The variables of this exercise are chosen mutual exclusively.

    .. lua:attribute:: PRODUCT : number

        The variables of this exercise are chosen in all combinations.
