.. QuizGrind documentation master file, created by
   sphinx-quickstart on Sun Feb 14 13:24:12 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to QuizGrind's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   api/index

This is the documentation file for QuizGrind.

   Some basic information about how to use QuizGrind may be found in the `README.md <https://gitlab.com/platypro/quizgrind/-/blob/master/README.md/>`_ file on GitLab.

   For API Documentation see :ref:`api-reference`  
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
