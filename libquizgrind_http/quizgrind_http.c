/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "quizgrind_http.h"
#include "homepage_gen.h"
#include "libsoup/soup-message-body.h"
#include "libsoup/soup-message-headers.h"
#include "libsoup/soup-message.h"
#include "libsoup/soup-server.h"
#include "libsoup/soup-session.h"
#include "libsoup/soup-status.h"
#include "libsoup/soup-types.h"
#include "libsoup/soup-uri.h"
#include "pathbuf.h"

#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <libquizgrind/quizgrind.h>

#define C_KEY(k) ("_" k)
#define STR(a) #a
#define XSTR(a) STR(a)

struct quizgrind_daemon_http_internal_t_ {
  SoupMessage* message;
  SoupSession* session;
  SoupServer* server;

};

void quizgrind_daemon_http_log_write(quizgrind_daemon_http_t* state, const char* fmt, ...)
{
  va_list args;
  va_start (args, fmt);
  GDateTime* dt = g_date_time_new_now(state->log_time_zone);

  g_output_stream_printf(G_OUTPUT_STREAM(state->log_writer), NULL, NULL, NULL,
      "[%.4d-%.2d-%.2d %.2d:%.2d:%.2d] ",
      g_date_time_get_year(dt), g_date_time_get_month(dt),
      g_date_time_get_day_of_month(dt),
      g_date_time_get_hour(dt), g_date_time_get_minute(dt),
      g_date_time_get_second(dt));
  g_output_stream_vprintf(G_OUTPUT_STREAM(state->log_writer),
      NULL, NULL, NULL, fmt, args);

  g_output_stream_write(G_OUTPUT_STREAM(state->log_writer), "\n", 1,
      NULL, NULL);

  g_date_time_unref(dt);
  va_end (args);
}

quizgrind_daemon_http_user_t** quizgrind_daemon_http_get_user(quizgrind_daemon_http_t* state, quizgrind_daemon_http_uid_t uid)
{
  g_mutex_lock(&state->userMutex);
  //Search for user
  guint8 basebucket = (uid & 0xFF);
  quizgrind_daemon_http_user_t** bucket = &state->users[basebucket];
  quizgrind_daemon_http_user_t** result = NULL;
  
  while(!result)
  {
    if(*bucket)
    {
      if((*bucket)->uid == uid)
      {
        result = bucket;
        (*bucket)->touchTime = time(NULL);
      }
      else
      {
        if((*bucket)->next)
        {
          if(
             difftime((*bucket)->next->touchTime, time(NULL)) > QUIZGRIND_DAEMON_HTTP_USER_TIMEOUT_TIME
          && (*bucket)->next->uid != uid
          )
          {
            quizgrind_daemon_http_user_t* oldbucket = (*bucket)->next;
            (*bucket)->next = oldbucket->next;
            free(oldbucket);
          }
          if((*bucket)->next)
            bucket = &(*bucket)->next;
        }
        else
          result = &(*bucket)->next;
      }
    } 
    else 
      result = bucket;
  }
  g_mutex_unlock(&state->userMutex);
  return result;
}

void quizgrind_daemon_http_write_error(WJWriter w, char* error)
{
  WJWString(KEY_ACTION, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_ERROR, true, w);
  WJWString(KEY_ERROR, error, true, w);
}

quizgrind_daemon_http_user_t* quizgrind_daemon_http_get_user_and_assert(quizgrind_daemon_http_t* state, WJElement request, WJWriter w)
{
  guint32 uid = WJEUInt32(request, C_KEY(KEY_UID), WJE_GET, 0);
  quizgrind_daemon_http_user_t* u = *quizgrind_daemon_http_get_user(state, uid);
  if(!u)
    quizgrind_daemon_http_write_error(w, "User no longer available");
  
  return u;
}

void quizgrind_daemon_http_write_problem_set(quizgrind_daemon_http_t* state, WJWriter response)
{
  GList* problem_set_list = state->init.project->problem_sets;
  WJWOpenArray(KEYLIST(KEY_PSET), response);
  while(problem_set_list)
  {
    quizgrind_problem_set_t* problem_set = (quizgrind_problem_set_t*) problem_set_list->data;

    WJWOpenObject(NULL, response);
    WJWString(KEY_NAME, problem_set->name, true, response);
    WJWString(KEY_ID, problem_set->id, true, response);
    WJWCloseObject(response);
    problem_set_list = problem_set_list->next;
  }
  WJWCloseArray(response);
}

gboolean quizgrind_daemon_http_next_question(quizgrind_daemon_http_user_t* u, WJWriter w)
{
  //Generate question
  if(!quizgrind_question_next(&u->scope))
  {
    quizgrind_daemon_http_write_error(w, "Could not Generate question!");
    return false;
  }

  if(u->scope.current_question->hints)
  {
    u->hintAt = u->scope.current_question->hints->child;
    WJWUInt32(KEYCOUNT(KEY_HINT), u->scope.current_question->hints->count, w);
  } else WJWUInt32(KEYCOUNT(KEY_HINT), 0, w);
  WJWString(KEY_CALCULATOR, quizgrind_exercise_ref_get_calc_type(u->scope.current_exercise_ref), true, w);
  WJWString(KEY_NAME, u->scope.current_exercise_ref->name, true, w);
  quizgrind_question_write_json(&u->scope, w, KEYLIST(KEY_WIDGET), QUIZGRIND_WIDGET_JSON_QUESTION);
  return true;
}

void session_common_init(quizgrind_daemon_http_t* state)
{
  g_mutex_init(&state->userMutex);
}

#define SCP_ASSERT(val, msg) \
    if(!(val)) \
    { quizgrind_daemon_http_write_error(response, msg);goto close_session_common_process; }

gboolean session_common_process(quizgrind_daemon_http_t* state, WJElement request, WJWriter response)
{
  WJWOpenObject(NULL, response);
  char* action = WJEString(request, C_KEY(KEY_ACTION), WJE_GET, NULL);
  SCP_ASSERT(action, "No action!");
  
  if(!strcmp(action, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_USER_NEW_))
  {
    quizgrind_daemon_http_log_write(state, "New User Requested");
    WJWString(KEY_ACTION, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_USER_NEW_, true, response);
    
    if(WJEBool(request, C_KEY(KEY_GIVEQSET), WJE_GET, FALSE) == TRUE)
      quizgrind_daemon_http_write_problem_set(state, response);
    
    //New User
    guint32 uid = 0;
    quizgrind_daemon_http_user_t** u = NULL;
    do 
    {
      uid = g_random_int();
      u = quizgrind_daemon_http_get_user(state, uid);
    }
    while(*u != NULL);
    
    *u = calloc(1, sizeof(quizgrind_daemon_http_user_t));

    (*u)->scope.current_project = state->init.project;
    (*u)->uid = uid;
    (*u)->touchTime = time(NULL);
    WJWUInt32(KEY_UID, uid, response);
  }
  else if(!strcmp(action, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_USER_TOUCH_))
  {
    if(quizgrind_daemon_http_get_user_and_assert(state, request, response))
      WJWString(KEY_ACTION, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_USER_TOUCH_, true, response); 
  }
  else if(!strcmp(action, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUIZ_NEW))
  {
    quizgrind_daemon_http_user_t* u;
    if((u = quizgrind_daemon_http_get_user_and_assert(state, request, response)))
    {
      XplBool giveFirst = WJEBool(request, C_KEY(KEY_GIVEFIRST), WJE_NEW, false);
      char* pset   = WJEString(request, C_KEY(KEY_PSET), WJE_GET, NULL);
      SCP_ASSERT(pset, "Missing problem set!");

      quizgrind_gen_mode_t genmode = quizgrind_enum_match_str(&quizgrind_gen_mode, WJEString(request, C_KEY(KEY_GENMODE), WJE_GET, NULL));
      quizgrind_quiz_t* quiz = quizgrind_quiz_next(&u->scope, pset, genmode, 5);
      
      if(!quiz)
        { SCP_ASSERT(pset, "Invalid problem set!"); }

      quizgrind_daemon_http_log_write(state, "Quiz issued, id:%p.", quiz);

      WJWUInt32(KEYCOUNT(KEY_QUESTION), quiz->questionsLeft, response);
      if(giveFirst && quizgrind_daemon_http_next_question(u, response))
        WJWString(KEY_ACTION, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUIZ_NEW, true, response);
    }
  }
  else if(!strcmp(action, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUESTION_NEXT_))
  {
    quizgrind_daemon_http_user_t* u;
    if((u = quizgrind_daemon_http_get_user_and_assert(state, request, response))
    &&  quizgrind_daemon_http_next_question(u, response))
    {
      quizgrind_daemon_http_log_write(state, "Question requested for quiz %p.", u->scope.current_quiz);
      WJWString(KEY_ACTION, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUESTION_NEXT_, true, response);
    }
  }
  else if(!strcmp(action, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUESTION_HINT))
  {
    quizgrind_daemon_http_user_t* u;
    if((u = quizgrind_daemon_http_get_user_and_assert(state, request, response)))
    {
      quizgrind_daemon_http_log_write(state, "Hint requested for quiz %p.", u->scope.current_quiz);
      WJWString(KEY_ACTION, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUESTION_HINT, true, response);
      if(u->hintAt)
      {
        WJEWriteDocument(u->hintAt, response, KEY_HINT);
        u->hintAt = u->hintAt->next;
      }
      else
      {
        quizgrind_question_write_json(&u->scope, response, KEYLIST(KEY_SOLUTION), QUIZGRIND_WIDGET_JSON_SOLUTION);
      }
    }
  }
  else if(!strcmp(action, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUESTION_CHECK))
  {
    quizgrind_daemon_http_user_t* u;
    if((u = quizgrind_daemon_http_get_user_and_assert(state, request, response)) && u->scope.current_question)
    {
      quizgrind_daemon_http_log_write(state, "Question check for quiz %p.", u->scope.current_quiz);
      WJElement usolution = WJEArray(request, KEY_SOLUTION, WJE_GET);

      SCP_ASSERT(usolution, "No answer given!");
      WJWString(KEY_ACTION, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUESTION_CHECK, true, response);
      quizgrind_question_check_solutions_json(&u->scope, usolution, response);
    }
  }
  else if(!strcmp(action, QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_PROBLEM_SET_GET_))
  {
    quizgrind_daemon_http_write_problem_set(state, response);
  }

close_session_common_process:
  WJWCloseObject(response);
  
  return true;
}

void session_common_cleanup(quizgrind_daemon_http_t* state)
{
  int i = QUIZGRIND_DAEMON_HTTP_USER_MAX;
  quizgrind_daemon_http_user_t** user = state->users;
  while(i)
  {
    if(*user)
    {
      while(*user)
      {
        quizgrind_daemon_http_user_t* user_next = (*user)->next;
        free(*user);
        (*user) = user_next;
      }
    }
    user++;
    i--;
  }
  g_mutex_clear(&state->userMutex);
}

size_t session_common_wjw_callback(char *data, size_t size, void *res_)
{
  struct quizgrind_daemon_http_internal_t_* res = 
      (struct quizgrind_daemon_http_internal_t_*) res_;
  soup_message_body_append(res->message->response_body, SOUP_MEMORY_COPY, data, size);
  soup_server_unpause_message (res->server, res->message);
  return size;
}

gboolean quizgrind_daemon_http_parse_bool(const char* text)
{
  if(!strcmp(text, "true")) return true;
  else return false;
}

void session_http_procpost(gpointer key_, gpointer value_, gpointer elem_)
{
  const char* key = (const char*) key_;
  const char* value = (const char*) value_;
  WJElement elem = (WJElement) elem_;
  if(*key == 'a')
  {
    if(!WJEObject(elem, KEY_SOLUTION, WJE_GET))
      WJEArray(elem, KEY_SOLUTION, WJE_NEW);
    WJElement sol = WJEObject(elem, KEY_SOLUTION "[$]", WJE_NEW);
    WJEUInt32(sol, KEY_ID, WJE_NEW, strtoul(key + 1, NULL, 0));
    WJEString(sol, KEY_KEY, WJE_NEW, value);
  }
  else if(*key == '_')
  {
    if(!strcmp(key, C_KEY(KEY_UID)))
      WJEUInt32(elem, key, WJE_NEW, strtoul(value, NULL, 0));
    else if(!strcmp(key, C_KEY(KEY_GIVEFIRST)))
      WJEBool(elem, key, WJE_NEW, quizgrind_daemon_http_parse_bool(value));
    else if(!strcmp(key, C_KEY(KEY_GIVEQSET)))
      WJEBool(elem, key, WJE_NEW, quizgrind_daemon_http_parse_bool(value));
    else
      WJEString(elem, key, WJE_NEW, value);
  }
}

extern char* QUIZGRIND_DATA_PATH;

static void
client_file_written (SoupMessage *msg, gpointer user_data)
{
  g_mapped_file_unref((GMappedFile*)user_data);
}

void client_write_file(SoupMessage* msg, path_t** pathbuf)
{
  if(!path_exists(pathbuf))
  {
    soup_message_set_response(msg, "text/plain", SOUP_MEMORY_STATIC, "404 Not found", 13);
    soup_message_set_status (msg, SOUP_STATUS_NOT_FOUND);
    return;
  }

  char* mime_type =
  g_content_type_guess (
    path_get_cstr(pathbuf), NULL, 0, NULL
  );

  GFile* file = g_file_new_for_path(path_get_cstr(pathbuf));

  GError* error = NULL;

  GMappedFile* filemapping = g_mapped_file_new (path_get_cstr(pathbuf), FALSE, &error);
  if (!filemapping) {
    soup_message_set_response(msg, "text/plain", SOUP_MEMORY_STATIC, "404 Not found", 13);
    soup_message_set_status (msg, SOUP_STATUS_NOT_FOUND);
    return;
  }

  g_signal_connect (msg, "wrote-body", G_CALLBACK (client_file_written), filemapping);

  soup_message_set_response(
    msg, mime_type, SOUP_MEMORY_TEMPORARY,
    g_mapped_file_get_contents (filemapping),
    g_mapped_file_get_length (filemapping));

  soup_message_set_status(msg, SOUP_STATUS_OK);

  g_object_unref(file);

  // Leave mapped file referenced
}

static void
session_http_proc (SoupServer        *server,
                 SoupMessage       *msg, 
                 const char        *path,
                 GHashTable        *query,
                 SoupClientContext *client,
                 gpointer           state_)
{
  quizgrind_daemon_http_t* state = (quizgrind_daemon_http_t*)state_;

  while(*path == '/')
    { path ++; }

  if(strstr(path, "qservice/") == path)
  {
    path += 8;
    struct quizgrind_daemon_http_internal_t_ res = 
      {.message = msg, .server = state->server};
    soup_message_headers_set_encoding(msg->response_headers, SOUP_ENCODING_CHUNKED);
    soup_message_headers_set_content_type(msg->response_headers, "text/json", NULL);
    WJElement request = WJEObject(NULL, NULL, WJE_NEW);
    WJWriter response = WJWOpenDocument(0, session_common_wjw_callback, &res);

    const char* querytext = soup_uri_get_query(soup_message_get_uri(msg));
    ((void)querytext);

    if(query)
      { g_hash_table_foreach(query, session_http_procpost, request); }
    session_common_process(state, request, response);
    
    WJECloseDocument(request);
    WJWCloseDocument(response);

    soup_message_set_status(msg, SOUP_STATUS_OK);
    soup_message_body_complete(msg->response_body);
    soup_server_unpause_message(state->server, msg);
  }
  else if(strstr(path, "store/") == path)
  {
    path += 5;
    path_t* pathbuf = path_new_from_str(path_get_cstr(&state->init.project->path_exercise), PATH_FLAG_NO_PARENT);
    while(*path)
      { path = path_push(&pathbuf, path); }

    char* extension = path_extension_get(&pathbuf);
    // Make sure extension is not json or lua
    if(extension && (!strcmp(extension, "json") || !strcmp(extension, "lua")))
    {
      soup_message_set_response(msg, "text/plain", SOUP_MEMORY_STATIC, "404 Not found", 13);
      soup_message_set_status (msg, SOUP_STATUS_NOT_FOUND);
    }
    else
    {
      client_write_file(msg, &pathbuf);
    }

    path_destroy(&pathbuf);
  }
#ifdef QUIZGRIND_DOCS_LOCATION
  else if(!strcmp(path, "docs"))
  {
    soup_message_set_redirect (msg,
                               303,
                               "docs/index.html");
  }
  else if(strstr(path, "docs/") == path)
  {
    path += 4;
    path_t* pathbuf = path_new_from_str(XSTR(QUIZGRIND_DOCS_LOCATION), PATH_FLAG_NO_PARENT);
    while(*path)
      { path = path_push(&pathbuf, path); }

    client_write_file(msg, &pathbuf);
    path_destroy(&pathbuf);
  }
#endif
  else if(!*path)
  {
    soup_message_set_response(msg, "text/html", SOUP_MEMORY_STATIC, state->homepageSrc, state->homepageLen);
  }
  else
  {
    path_t* pathbuf = path_new_from_str(QUIZGRIND_DATA_PATH, PATH_FLAG_NO_PARENT);
    path_push(&pathbuf, QUIZGRIND_DAEMON_HTTP_WEBAPP_DIR);
    while(*path)
      { path = path_push(&pathbuf, path); }

    client_write_file(msg, &pathbuf);

    path_destroy(&pathbuf);
  }
}

void session_http_do_cleanup(gpointer state_)
{
  quizgrind_daemon_http_t* state = state_;

  if(state->server)
  {
    g_object_unref(state->server);
    state->server = NULL;
    session_common_cleanup(state);
  }

  state->gloop = NULL;
}

gpointer session_http_do(gpointer state_)
{
  quizgrind_daemon_http_t* state = state_;
  GInetAddress* address_inet = g_inet_address_new_from_string(state->init.address);
  GSocketAddress* address_socket = g_inet_socket_address_new(address_inet, state->init.port);

  state->server = soup_server_new(NULL, NULL);
  soup_server_add_handler(state->server, "/", session_http_proc, state_, NULL);
  soup_server_listen(state->server, address_socket, 0, NULL);
  session_common_init(state);

  if (state->init.open_browser)
  {
      char* uri = g_strdup_printf("http://localhost:%d/", state->init.port);
      g_app_info_launch_default_for_uri(uri, NULL, NULL);
      free(uri);
  }
  g_main_loop_run(state->gloop);

  session_http_do_cleanup(state_);
  return NULL;
}

gboolean quizgrind_daemon_http_start(struct quizgrind_daemon_http_t_* daemon, quizgrind_daemon_http_init_t* init)
{
  daemon->init = *init;
  daemon->gloop = g_main_loop_new(NULL, FALSE);

  // Setup logging
  path_t* logpath = path_clone(&daemon->init.project->path_project);
  path_push(&logpath, "access.log");
  GFile* file = g_file_new_for_path(path_get_cstr(&logpath));
  daemon->log_time_zone = g_time_zone_new_local();
  daemon->log_writer = g_file_append_to(file, G_FILE_CREATE_NONE, NULL, NULL);
  path_destroy(&logpath);
  g_object_unref(file);

  daemon->homepageSrc = quizgrind_daemon_http_generate_homepage(init->html_template, &daemon->homepageLen);

  quizgrind_daemon_http_log_write(daemon, "Daemon Started");

  daemon->thread = g_thread_new("HTTP_DO", session_http_do, daemon);
  quizgrind_project_watch(daemon->init.project);
  return true;
}

gboolean quizgrind_daemon_http_join(struct quizgrind_daemon_http_t_* daemon)
{
  g_thread_join(daemon->thread);
  return true;
}

gboolean quizgrind_daemon_http_stop(quizgrind_daemon_http_t* state)
{
  if(state->gloop && g_main_loop_is_running(state->gloop))
    { 
      g_main_loop_quit(state->gloop);
      g_main_context_wakeup(g_main_loop_get_context(state->gloop));
    }

  g_output_stream_close(G_OUTPUT_STREAM(state->log_writer), NULL, NULL);
  g_object_unref(state->log_time_zone);

  free(state->homepageSrc);

  g_thread_join(state->thread);
  return true;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
