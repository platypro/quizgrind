/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INCLUDE_SESSION_H
#define INCLUDE_SESSION_H

#include <glib.h>
#include <time.h>
#include <wjelement.h>
#include <libsoup/soup.h>

#include <libquizgrind/quizgrind.h>

#define QUIZGRIND_DAEMON_HTTP_WEBAPP_DIR "webroot"

#define QUIZGRIND_DAEMON_HTTP_DEFAULT_PORT 1245
#define QUIZGRIND_DAEMON_HTTP_DEFAULT_IPC_PATH APP_IPC_PATH "/socket"

#define QUIZGRIND_DAEMON_HTTP_USER_MAX 256
#define QUIZGRIND_DAEMON_HTTP_USER_TIMEOUT_TIME 1000

struct quizgrind_cmd_daemon_t_;

typedef guint32 quizgrind_daemon_http_uid_t;

typedef struct quizgrind_daemon_http_user_t_
{
  struct quizgrind_daemon_http_user_t_* next;
  quizgrind_daemon_http_uid_t uid;
  time_t touchTime;

  struct quizgrind_scope_t_ scope;
  WJElement hintAt;

} quizgrind_daemon_http_user_t;

typedef struct quizgrind_daemon_http_init_t_ { 
  quizgrind_project_t* project;

  int port;
  char* address;

  bool open_browser;

  char* html_template;

} quizgrind_daemon_http_init_t;

typedef struct quizgrind_daemon_http_t_
{
  quizgrind_daemon_http_init_t init;
  
  GThread* thread;
  GMutex userMutex;
  quizgrind_daemon_http_user_t* users[QUIZGRIND_DAEMON_HTTP_USER_MAX];

  GMainLoop * gloop;

  GFileOutputStream* log_writer;
  GTimeZone* log_time_zone;

  char* homepageSrc;
  size_t homepageLen;

  //HTTP stuff
  SoupServer * server;

} quizgrind_daemon_http_t;

#define QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_USER_NEW_        "userNew"
#define QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_USER_TOUCH_      "userTouch"
#define QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_PROBLEM_SET_GET_ "qsetQuery"
#define QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUESTION_NEXT_   "questionNext"
#define QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUESTION_HINT    "questionHint"
#define QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUESTION_CHECK   "questionCheck"
#define QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUIZ_SUMMARY     "quizSummary"
#define QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_QUIZ_NEW         "quizNew"
#define QUIZGRIND_DAEMON_HTTP_REQUEST_TYPE_ERROR            "error"

extern gboolean quizgrind_daemon_http_start(struct quizgrind_daemon_http_t_* daemon, struct quizgrind_daemon_http_init_t_* init);
extern gboolean quizgrind_daemon_http_join(struct quizgrind_daemon_http_t_* daemon);
extern gboolean quizgrind_daemon_http_stop(struct quizgrind_daemon_http_t_* daemon);

#endif /*INCLUDE_SESSION_H*/

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
