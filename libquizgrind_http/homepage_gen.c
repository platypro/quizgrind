/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of libpmustache.
 */

#include "quizgrind_http.h"
#include "homepage_gen.h"
#include <pmustache/template.h>
#include <glib.h>
#include <pathbuf.h>
#include <string.h>

extern char* QUIZGRIND_DATA_PATH;

struct quizgrind_daemon_http_lookup_base
{
  char* headers_src;
  char* body_src;
};

PMUS_OBJECT quizgrind_daemon_http_generate_homepage_callback_get(gpointer base_, char* name)
{
    struct quizgrind_daemon_http_lookup_base* base = (struct quizgrind_daemon_http_lookup_base*) base_;
    PMUS_OBJECT result = {0};
    result.type = PMUS_TYPE_STRING;

    if(!strcmp(name, "QuizgrindHeaders"))
        { result.stringValue = base->headers_src; }
    else if(!strcmp(name, "QuizgrindApp"))
        { result.stringValue = base->body_src; }
    else
        { result.stringValue = ""; }

    return result;
}

char* quizgrind_daemon_http_generate_homepage(char* html_template, size_t* len)
{
    // Setup homepage template
    path_t* tpath = path_new_from_str(QUIZGRIND_DATA_PATH, 0);
    path_push(&tpath, QUIZGRIND_DAEMON_HTTP_WEBAPP_DIR);

    struct quizgrind_daemon_http_lookup_base lookup_base = {0};

    char* file_src = NULL;

    // Try loading specified html template
    if(html_template)
    {
        file_src = mustache_eatFile(html_template);

        if(!file_src)
            { g_print("Failed to load html template %s, reverting to default.\n", html_template); }
    }  
    
    if(file_src == NULL)
    {
        path_push(&tpath, "quizgrind.html");
        file_src = mustache_eatFile(path_get_cstr(&tpath));
        path_pop(&tpath);
    }

    PMUS_PROVIDER provider = {
        .getValue = quizgrind_daemon_http_generate_homepage_callback_get,
        .changeContext = NULL
    };

    PMUS_BUILDER builder = {
        .template = mustache_mkIndex(file_src, 0),
        .provider = &provider,
        .escape = pmus_escape_copy,
        .baseContext = &lookup_base
    };

    path_push(&tpath, "quizgrind_headers.html");
    lookup_base.headers_src = mustache_eatFile(path_get_cstr(&tpath));
    path_pop(&tpath);

    path_push(&tpath, "quizgrind_app.html");
    lookup_base.body_src = mustache_eatFile(path_get_cstr(&tpath));
    
    path_destroy(&tpath);

    char* result = mustache_generate_mem(&builder);

    free(lookup_base.headers_src);
    free(lookup_base.body_src);
    free(file_src);

    *len = strlen(result);

    return result;    
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
