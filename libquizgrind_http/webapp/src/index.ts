/* Aeden McClain (c) 2019-2020
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

import {Widget} from './widget'
import {ChoiceInput} from "./widget/choiceInput";
import {ImageView} from "./widget/imageView";
import {Element_W} from "./widget/element";

export const QSERVICE_PATH = "/qservice/?";

(function() {

var widgets : Widget[] = [ChoiceInput, ImageView, Element_W];

var $ = function( id : string ) { return document.getElementById( id ); };

const enum AnswerCircleState {
  Incorrect,
  Correct,
  Hint
}

const enum ViewMode {
  Menu,
  Quiz
}

const enum AnswerStatus {
  Correct,
  Incorrect
}

let uid = 0;
let questionAt = 0;
let hintCount = 0;

const BTN_CONTINUE_CHECK   = "Check Answer";
const BTN_CONTINUE_PROCEED = "Proceed";
const BTN_CONTINUE_SUMMARY = "Show Summary";

function append_to_form(form : string, key : string, val: string)
{
  if(form.length > 0) 
    { form = form + "&" }
  form = form + encodeURIComponent(key) + "=" + encodeURIComponent(val)

  return form;
}

function clearElement(elem : Element)
{
  while(elem.firstChild)
    elem.removeChild(elem.firstChild);
}

function setHints(hints : number)
{
  const btn_getHint = $("btn_getHint") as HTMLInputElement;
  switch(hintCount)
  {
    case -1:
      btn_getHint.disabled = true;
      btn_getHint.value = "No hints left";
      break;
    case 0:
      btn_getHint.value = "Show solution";
      btn_getHint.disabled = false;
      break;
    default:
      btn_getHint.value = hintCount + " hint" + 
        (hintCount > 1 ? "s":"") + " left";
      btn_getHint.disabled = false;
  }
}

function setAnswerCircle(state : AnswerCircleState)
{
  const qCircle = $("qid" + questionAt) as HTMLDivElement;
  if(state == AnswerCircleState.Incorrect)
    qCircle.className = "qDot qActive qIncorrect";
  else if(qCircle.className == "qDot qActive")
  {
    if(state == AnswerCircleState.Correct)
      qCircle.classList.add("qCorrect");
    else if(state == AnswerCircleState.Hint)
      qCircle.classList.add("qHint");
  }
}

function setAnswerStatus(text:string, status:AnswerStatus)
{
  const answerStatus = $("answerStatus") as HTMLDivElement;
  if(status == AnswerStatus.Correct)
  {
    answerStatus.classList.remove("label_bad");
    answerStatus.classList.add("label_good");
  }
  else
  {
    answerStatus.classList.remove("label_good");
    answerStatus.classList.add("label_bad"); 
  }
  answerStatus.innerHTML = text;
}

function setView(view : ViewMode)
{
  if(view == ViewMode.Menu)
  {
    ($("btn_return") as HTMLInputElement).disabled = true;
    ($("quizIndex") as HTMLDivElement).classList.remove("invisible");
    ($("quizArea") as HTMLDivElement).classList.add("invisible");
  } 
  else
  {
    ($("btn_return") as HTMLInputElement).disabled = false;
    ($("quizIndex") as HTMLDivElement).classList.add("invisible");
    ($("quizArea") as HTMLDivElement).classList.remove("invisible");
  }
}

function processRequest()
{
  if(this.status == 503 || this.status == 404)
  {
    ($("headerErrorArea") as HTMLDivElement).innerText = "Could not connect to backend!";
    ($("quizOptions") as HTMLDivElement).classList.add("invisible");
    return;
  }
  
  const data = JSON.parse(this.responseText);
  const questionTypes = ($("questionTypes") as HTMLDivElement);
  switch(data.action)
  {
  case "error":
  {
    setAnswerStatus(data.error, AnswerStatus.Incorrect);
    break;
  }
  case "userNew":
  {
    uid = data.uid;

    ($("quizOptions") as HTMLDivElement).classList.remove("invisible");
    
    data.psets.forEach(function(set : any)
    {
      const cbox = document.createElement("input");
      const label = document.createElement("label");
      cbox.name    = "psetSelect";
      cbox.type    = "radio";
      cbox.id      = set.id;
      cbox.defaultChecked = true;
      label.htmlFor = set.id;
      label.innerHTML = set.name;
      questionTypes.appendChild(cbox);
      questionTypes.appendChild(label);
    });
    break;
  }
  case "quizNew":
  {
    setView(ViewMode.Quiz);
    const questionsLeft = ($("questionsLeft") as HTMLDivElement);
    ($("btn_continue") as HTMLInputElement).disabled = false;
    clearElement(questionsLeft);
    questionAt = data.questionCount;
    for(let i = data.questionCount - 1; i > -1; i--)
    {
      const qCircle = document.createElement("div");
      qCircle.classList.add("qDot");
      qCircle.id = "qid" + i;
      questionsLeft.appendChild(qCircle);
    }
  }
  //Fall through...
  case "questionNext":
  {
    const questionArea = ($("questionArea") as HTMLDivElement);
    const eCalcImage = ($("eCalcImage") as HTMLImageElement);
    const eCalcText  = ($("eCalcText") as HTMLDivElement);
    let focus = false; //< Store whether a widget has set focus yet

    ($("btn_continue") as HTMLInputElement).value = BTN_CONTINUE_CHECK;
    
    let calc = ""; 
    let img  = "";
    switch(data.calculator)
    {
      case "banned":
        calc = "No";
        img  = "none";
        break;
      case "basic":
      case "scientific":
      case "graphing":
        img  = data.calculator;
        calc = data.calculator[0].toUpperCase()
             + data.calculator.substring(1);
        break;
      case "none":
        // nothing to do
        break;
    }

    if(img != "")
    {
      eCalcImage.src = "media/calc_" + img + ".svg";
      eCalcText.innerHTML = calc + " calculators allowed";
    } 
    else
    {
      eCalcText.innerHTML = "";
      eCalcImage.src = "";
    }

    ($("eNameText") as HTMLDivElement).innerHTML = data.name;
    clearElement($("hintArea"));

    clearElement(questionArea);
    hintCount = data.hintCount;
    setHints(data.hintCount);
    
    const oldq = ($("qid" + questionAt) as HTMLDivElement);
    if(oldq) oldq.classList.remove("qActive");
    questionAt--;
    ($("qid" + questionAt) as HTMLDivElement).classList.add("qActive");

    data.widgets.forEach(function(widget : any)
    {
      let elem;
      for(let widgetEl of widgets)
      {
        if(widgetEl.name === widget.type)
          elem = widgetEl.gen(widget.id, widget.options);
      }

      if(!elem)
        ($("headerErrorArea") as HTMLDivElement).innerText = 
          "Some widget types sent by the server seem to be unavailable here. Expect fails.";
      else
      {
        elem.id = widget.id;
        elem.setAttribute("data_widget", widget.type);
        questionArea.appendChild(elem);
      }
    });

    // Focus any valid inputs
    for(let el of <any>questionArea.getElementsByTagName("input"))
    {
      if((el as HTMLInputElement).type == "text" 
      || (el as HTMLInputElement).type == "number")
      {
        el.focus();
        break;
      }
    }

    break;
  }
  case "questionCheck":
  {
    let oldelement;
    while(oldelement = document.querySelector(".controlError"))
    {
      oldelement.classList.remove("controlError");
    }
    
    if(data.correct === true)
    {
      setAnswerStatus(data.result, AnswerStatus.Correct);

      ($("btn_continue") as HTMLInputElement).value = questionAt ?
        BTN_CONTINUE_PROCEED : BTN_CONTINUE_SUMMARY;

      setAnswerCircle(AnswerCircleState.Correct);
    }
    else if(data.correct === false)
    {
      setAnswerStatus(data.result, AnswerStatus.Incorrect);

      data.marks.forEach(function(errelement: any)
      {
        const el = $(errelement.id);
        if(errelement.marks != errelement.marks_max)
        for(let widgetEl of widgets)
        {
          if(widgetEl.name == el.getAttribute("data_widget"))
          {
            widgetEl.error(el);
            break;
          }
        }
      });

      setAnswerCircle(AnswerCircleState.Incorrect);
    }
    break;
  }
  case "questionHint":
  {
    const hintArea = $("hintArea") as HTMLDivElement;

    hintCount --;

    if(hintCount > -1)
    {
      setAnswerCircle(AnswerCircleState.Hint);
      const elem = document.createElement("p");
      elem.innerText = data.hint;
      setHints(hintCount);
      hintArea.appendChild(elem);
    }
    else
    {
      setAnswerCircle(AnswerCircleState.Incorrect);
      const solTitle = document.createElement("strong");
      solTitle.innerText = "Solution:";
      hintArea.appendChild(solTitle);
      setHints(-1);
      for(let sol of data.solutions)
      {
        const elem = document.createElement("div");

        for(let widgetEl of widgets)
        {
          if(widgetEl.name == $(sol.id).getAttribute("data_widget"))
          {
            if('show_answer' in widgetEl)
            {
              widgetEl.show_answer(elem, sol.options);
            }
            break;
          }
        }
        hintArea.appendChild(elem);
      }
    }
    break;
  }
  }
}

function sendRequest(action : string, fun: (formData: string) => string)
{
  let request = new XMLHttpRequest();
  let formData = "";
  formData = append_to_form(formData, "_action", action)

  formData = fun(formData);

  request.addEventListener("load", processRequest);
  request.open("POST", QSERVICE_PATH + formData);
  //request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  request.send();
}

function sendUID(formData: string)
{
  return append_to_form(formData, "_uid", uid.toString())
}

function btn_continue_click()
{
  const btn_continue = $("btn_continue") as HTMLButtonElement
  const questionArea = $("questionArea") as HTMLDivElement;
  switch(btn_continue.value)
  {
  case BTN_CONTINUE_PROCEED:
  {
    setAnswerStatus("", AnswerStatus.Correct);
    sendRequest("questionNext", sendUID);
    break;
  }
  case BTN_CONTINUE_SUMMARY:
  {
    clearElement(questionArea);
    let elem = document.createElement("b");
    elem.innerHTML =
      "There are no sumaries yet...<br> Feel free to start a new quiz!";
    ($("eCalcText") as HTMLDivElement).innerHTML = "";
    ($("eNameText") as HTMLDivElement).innerHTML = "";
    ($("eCalcImage") as HTMLImageElement).src = "";
    questionArea.appendChild(elem);
    btn_continue.disabled = true;
    setHints(-1);
    break;
  }
  case BTN_CONTINUE_CHECK:
  {
    sendRequest("questionCheck", function(formData: string) {
      formData = append_to_form(formData,"_uid", uid.toString());

      for(let element of <any>questionArea.children)
      {
        for(let widgetEl of widgets)
        {
          if(widgetEl.name == element.getAttribute("data_widget"))
          {
            const chk = widgetEl.check(element)
            if(chk !== null)
            {
              formData = append_to_form(formData,"a" + element.id, <string>chk);
            }
            break;
          }
        }
      }

      return formData;
    });
  }
  }
}

function newQuiz()
{
  sendRequest("quizNew", function(formData: string) {
    const questionTypes = $("questionTypes") as HTMLDivElement;

    formData = append_to_form(formData,"_uid", uid.toString());
    formData = append_to_form(formData,"_giveFirst", "true");
    switch(document.querySelector('input[name = "quizTypeSelect"]:checked').id)
    {
      case "opt_unordered":
        formData = append_to_form(formData,"_genMode", "unordered");
        break;
      case "opt_ordered":
        formData = append_to_form(formData,"_genMode", "ordered");
        break;
      default: formData = append_to_form(formData,"_genMode", "random"); 
    }

    for (let child of <any>questionTypes.children)
    {
      if((child as HTMLInputElement).checked)
      {
        formData = append_to_form(formData,"_pset", child.id);
        ($("headerNameArea") as HTMLDivElement).innerText = child.id; 
        break;
      }
    }

    return formData;
  });
}

function userNew()
{
  sendRequest("userNew", function(formData: string) {
    return append_to_form(formData,"_giveQset", "true");
  });
}

function getHint()
{
  sendRequest("questionHint", sendUID);
}

window.addEventListener("keydown", function(event) {
  if (event.keyCode === 13) 
  {
    btn_continue_click();
  }
});

window.addEventListener("load", function() {
  window.setInterval(function() {
    //Keep alive!!!!
    let request = new XMLHttpRequest();
    let form = ""
    form = append_to_form(form, "_uid", uid.toString());
    form = append_to_form(form, "_action", "keepAlive");
    request.open("POST", QSERVICE_PATH);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    request.send(form);
  }, 900000);

  userNew();

  ($("btn_newQuiz") as HTMLInputElement)
    .addEventListener("click",newQuiz);
  ($("btn_continue") as HTMLInputElement)
    .addEventListener("click",btn_continue_click);
  ($("btn_getHint") as HTMLInputElement)
    .addEventListener("click",getHint);
  ($("btn_return") as HTMLInputElement)
    .addEventListener("click", function(){
      setAnswerStatus("", AnswerStatus.Correct);
      ($("headerNameArea") as HTMLDivElement).innerText = "Main Menu"; 
      setView(ViewMode.Menu);
    })
})

})();

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
 