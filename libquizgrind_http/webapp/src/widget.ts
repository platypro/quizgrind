/* Aeden McClain (c) 2019-2020
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of Quiz Grinder.
 */

export class Widget {
  name: string;
  gen: (id: string, options: any) => HTMLElement;
  check: (elem: HTMLElement) => (string | void);
  error: (elem: HTMLElement) => void;
  show_answer: (out: HTMLElement, answer: any) => void;
}

export function createTooltip(text : string)
{
  const ttip = document.createElement("span");
  ttip.classList.add("tooltip");
  ttip.innerText = text;
  return ttip;
}

export function default_error(element : HTMLElement)
{
  element.classList.add("controlError");
}

export function default_check(elem: HTMLElement) : (string | void) 
  { return; }

export function default_show_answer(out: HTMLElement, answer: any) : (void) 
  { return; }

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
 