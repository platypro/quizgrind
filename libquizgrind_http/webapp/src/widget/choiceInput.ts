/* Aeden McClain (c) 2019-2020
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

import * as W from '../widget'

function drawelement(element: HTMLElement, choice: any)
{
  if(choice.type == "svg")
  {
    const svg = document.createElement("svg");
    svg.innerHTML = choice.text;
    element.appendChild(svg);
  }
  else element.innerText = choice.text;

  return element;
}

export const ChoiceInput : W.Widget = {
  name: "choice",

  gen(id: string, options: any)
  {
    const elem = document.createElement("div");
    elem.classList.add("choiceInput");
    
    if(options.display == "svg") elem.classList.add("choiceSVG");
                   
    const choiceInfo = document.createElement("em");
    if(options.numSelections > 1)
      choiceInfo.innerText = "Choose " + options.numSelections + ":";
    else
      choiceInfo.innerText = "Choose 1:";
    
    elem.appendChild(choiceInfo);

    options.choices.forEach(function(choice : any, i: number)
    {
      const choiceEl = document.createElement("div");
      choiceEl.id = choice.id.toString();
      choiceEl.classList.add("choiceItem");

      const input = document.createElement("input");
      if(options.numSelections > 1)
        input.type = "checkbox";
      else input.type = "radio";
      input.id   = "_" + choiceEl.id;
      input.name = id;
      
      const label = document.createElement("label");
      drawelement(label, choice);
      label.htmlFor = input.id;

      const qletter = document.createElement("span");
      qletter.classList.add("letter");
      qletter.innerText = String.fromCharCode(97 + i) + ")";
      label.appendChild(qletter);
      
      choiceEl.appendChild(input);
      choiceEl.appendChild(label);
      elem.appendChild(choiceEl);
    });
    
    return elem;
  },

  check(elem: HTMLElement) : (string | void)
  {
    let ans = "";
    let opts = elem.getElementsByClassName("choiceItem")
    for (let i = 0; i < opts.length; i++)
    {
      if((document.getElementById("_" + opts.item(i).id) as HTMLInputElement).checked)
      {
        if(ans != "") ans += ",";
        ans += opts.item(i).id.toString();
      }
    }
    return ans;
  },

  show_answer(out: HTMLElement, answer: any)
  {
    let highmark = 0;
    answer.choices.forEach(function(choice : any)
    {
      if(choice.marks > highmark)
        { highmark = choice.marks; }
    });

    answer.choices.forEach(function(choice : any)
    {
      if(choice.marks == highmark)
      { 
        const div = document.createElement("div");
        drawelement(div, choice);
        div.classList.add("choiceInputSolution");
        out.appendChild(div);
      }
    });
  },

  error: W.default_error
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
 