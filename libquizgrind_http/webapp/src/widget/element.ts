/* Aeden McClain (c) 2019-2020
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

import * as W from '../widget'

export const Element_W : W.Widget = {
  name: "element",

  gen(id: string, options: any)
  {
    const out = document.createElement("label");
    
    options.elements.forEach(function(element : any)
    {
    	switch(element.type)
    	{
    		case "text":
    		{
    			const text = document.createElement("span");
	    		text.innerHTML = element.text;
	    		out.appendChild(text);
	    		break;
	    	}
	    	case "input_text":
	    	{
	    	  const input = document.createElement("input");
	    	  input.type = "text";
			    input.id = "_" + id;
			    out.appendChild(input);
			    if(element.options.caps)
			   		{ out.appendChild(W.createTooltip("This answer is CaSe SeNsItIvE.")); }
			    
	    		break;
	    	}
	    	case "input_number":
	    	{
	    		const input = document.createElement("input");
	    	  input.type = "text";
			    input.id = "_" + id;
			    out.appendChild(input);
			    
			    if(element.options.mindp)
			    {
			      out.appendChild(
			        W.createTooltip(
			            "Answer to a minimum of " + element.options.mindp +" decimal place" + (element.options.mindp > 1 ? "s." : ".")
			            ));
			    }
	    		break;
	    	}
	    }
    });
    
    return out;
  },

  check(elem: HTMLElement) : (string | void)
  {
    var ans = "";
    for(let i = 0; i < elem.children.length; i++)
    {
    	let child = elem.children.item(i); 
    	if(child.tagName.toLowerCase() == "input")
    	{
			  ans += (child as HTMLInputElement).value;
  			ans += ";";
		  }
    }
    return ans;
  },

  show_answer(out: HTMLElement, answer: any)
  {
    answer.elements.forEach(function(element : any)
    {
      const text = document.createElement("span");
      switch(element.type)
      {
        case "text":
        {
          text.innerHTML = element.text;
          break;
        }
        case "input_text":
        case "input_number":
        {
          text.classList.add("solutionUser");
          let maxmarks = 0;
          element.answers.forEach(function(answer : any)
          {
            if(answer.marks > maxmarks)
            {
              maxmarks = answer.marks;
              text.innerHTML = " " + answer.value;
            }

            if(element.type == "input_number" && (answer.error != 0))
              { text.innerHTML += "&#177;" + answer.error; }
          });

          break;
        }
      }
      out.appendChild(text);
    });
  },

  error: W.default_error
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
 