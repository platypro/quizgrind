/* Aeden McClain (c) 2019-2020
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

import * as W from '../widget'

export const ImageView : W.Widget = {
  name: "image",

  gen(id: string, options: any)
  {
    if('svg' in options)
    {
      const elem = document.createElement("svg");
      elem.innerHTML = options.svg;
      return elem;
    }
    else if('src' in options)
    {
      const elem = document.createElement("img");
      elem.src = "../store/" + options.src;
      return elem;
    }
  },

  check: W.default_check,
  error: W.default_error,
  show_answer: W.default_show_answer
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
 