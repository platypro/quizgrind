var path = require('path')
module.exports = {
  mode: 'production',
  entry: "./src/index.ts",
  output: {
      path: ".",
      filename: 'bundle.js'
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".json"]
  },
  module: {
    rules: [{ 
      test: /\.ts?$/, 
      include: path.resolve(__dirname, "src/"),
      use: "ts-loader", 
      exclude: /node_modules/ 
    }],
  },
}
