<img src="libquizgrind_http/webapp/media/icon.svg" alt="drawing" width="50"/> QuizGrind
===

QuizGrind is a programmable quiz generator.

# What can QuizGrind do?
Quizgrind can:

 - Dynamically create unique quizzes without needing to do a single calculation 
manually.
 - Easily reuse questions for multiple quizzes, term tests, and courses.
 - Run on the web, complete with hints and automatic marking.
 - (WIP) Export quizzes and solution manuals as a PDF.
 - Be completely audited and modified under the terms of the GNU GPL V3.

# How does it work?
At the heart of QuizGrind are *exercises*. An exercise contains everything 
required to create a question of a certain type. Exercises accept *options*, 
which let you change how the exercise is generated without worrying about 
implementation.

Exercises are collected in *problem sets*. A problem set represents a single 
quiz or test and contains a list of exercises along with options for each one.

Exercises and problem sets are contained within a *project*. Quizgrind is always
run within a project.

After setting things up, a PDF or JSON file may be generated for a single
problem set, or a web server may be hosted for an entire project.

# How do I use it?

QuizGrind may be downloaded from the "Releases" section of this gitlab, or built
yourself. See *Building* below for more details on that.

Quizgrind is a command-line application. 

- On Windows, the executable is located under "bin/quizgrind" within the zip
  file. It is recommended to place the "bin" folder in your PATH.
- On Linux the executable is an [AppImage](https://appimage.org/). To run, make
  sure the executable bit is set, then execute it in your terminal.

The executable itself is self documenting, so you can always pass *--help* to
find out more about available commands and options.

- To get started with a new project run "quizgrind init" within
  an empty folder, which will create a new project.
- "quizgrind new" can be used to create new exercises and problem
  sets.
- "quizgrind gen" can be used to generate a JSON or PDF file from a problem set.
- "quizgrind daemon --browser" can be used to host a web server for your 
  project, and opens QuizGrind in your default browser. Any exercises or
  problem sets you create or edit will automatically update, no need to re-run
  QuizGrind

Complete docs on all QuizGrind features are not available yet, but an example 
project can be found in the *content* directory of this repository. Some widget 
documentation can be found under *src/widget/\<widget name\>/doc.json*.

# Building
QuizGrind is built using CMake and compiles on both Windows and Linux.

- On Windows, I recommend using MSYS2 as the full toolchain and libraries are 
  installable through it's provided package manager. If packaging the windows 
  version, make sure to copy all required dlls (use ldd on the executable), 
  as well as "gspawn-win64-helper-console.exe" to the bin directory after 
  install.

- On linux, I recommend using the "org.gnome.Sdk" flatpak with the 
  "org.freedesktop.Sdk.Extension.node14" extension as it provides everything 
  needed to build QuizGrind and works across many distributions. Some helper 
  scripts for setting this up, and generating an appimage, are available in 
  the "packaging" directory.




# Demo
Want to see a demo? There is a sample server running at 
<https://www.quizgrind.com/> which uses the test project included in this
repository.

# Developers
* Aeden McClain (aeden@platypro.net)
