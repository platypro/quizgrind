/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include <libquizgrind/quizgrind.h>

#include <libquizgrind_pdf/quizgrind_pdf.h>
#include <libquizgrind_pdf/widget.h>

#define STR(a) #a
#define XSTR(a) STR(a)

typedef struct quizgrind_pdf_render_questions_t_
{
  quizgrind_scope_t scope;
  GList* questions;
  cairo_t* cairo;
  cairo_matrix_t matrix;
  cairo_surface_t* surface;
  PangoContext* textContext;
  PangoLayout* textLayout;
  PangoFontDescription* fontDescription;
  quizgrind_pdf_t* opts;

  char* function;   /* The function to run for generating the problem */
  guint32 pageflag; /* The flag for determining page breaks. */

} quizgrind_pdf_render_questions_t;

static void quizgrind_pdf_register_widget(
    char* name,
    quizgrind_widget_function_t init,
    quizgrind_widget_function_t generate,
    quizgrind_widget_function_t generate_solution,
    quizgrind_widget_function_t cleanup)
{
  quizgrind_widget_interface_t* iface = quizgrind_widget_interface_get(name);
  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_PDF_INIT, init);
  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_PDF_GENERATE, generate);
  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_PDF_GENERATE_SOLUTION, generate_solution);
  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_PDF_CLEANUP, cleanup);
}

/* This function may be used to do specialized drawing operations but for now
 * is coded to only draw an underscore field for users to insert their answer. */
void quizgrind_pdf_shape_draw(
    cairo_t *cairo,
    PangoAttrShape *attr,
    gboolean do_path,
    gpointer data)
{
  double  dx, dy;
  cairo_get_current_point(cairo, &dx, &dy);

  cairo_move_to(cairo,
                dx + (attr->ink_rect.x) / PANGO_SCALE,
                dy - 2 + (attr->ink_rect.y + attr->ink_rect.height) / PANGO_SCALE);
  cairo_line_to(cairo,
                dx + (attr->ink_rect.x + attr->ink_rect.width) / PANGO_SCALE,
                dy - 2 + (attr->ink_rect.y + attr->ink_rect.height) / PANGO_SCALE);

  cairo_set_line_width(cairo, 0.5);

  cairo_stroke(cairo);
}

void quizgrind_pdf_push_height(quizgrind_pdf_rendered_question_t* question, guint32* height_page, guint32* height_question, const guint32 PAGE_BOTTOM, gboolean is_solution)
{
  const guint32 PAGE_FLAG = is_solution ?
      QUIZGRIND_PDF_RENDERED_FLAG_PAGE_BREAK_SOLUTION : QUIZGRIND_PDF_RENDERED_FLAG_PAGE_BREAK;
  if((*height_question) > PAGE_BOTTOM && question->widgets)
  {
    GList* widget_at = question->widgets;
    while(widget_at)
    {
      quizgrind_pdf_rendered_widget_t* widget = widget_at->data;
      const guint32 WIDGET_HEIGHT = (is_solution ? widget->height_solution : widget->height)
          + QUIZGRIND_PDF_WIDGET_PADDING;
      if(((*height_page) + WIDGET_HEIGHT) > PAGE_BOTTOM)
      {
        if(widget_at == question->widgets)
          { question->flags |= PAGE_FLAG; }
        else
          { widget->flags |= PAGE_FLAG; }

        (*height_page) = WIDGET_HEIGHT;
      }
      else
        { *height_page += WIDGET_HEIGHT; }

      widget_at = widget_at->next;
    }
  }
  else if((*height_page + *height_question) > PAGE_BOTTOM)
  {
    question->flags |= PAGE_FLAG;
    *height_page = *height_question;
  }
  else
    { *height_page += *height_question; }
}

void quizgrind_pdf_render_questions(quizgrind_pdf_render_questions_t* args)
{
  uint32_t question_at = 0;
  uint32_t subquestion_at = 0;
  for(GList* questions_iter = args->questions; questions_iter; questions_iter = questions_iter->next)
  {
    quizgrind_pdf_rendered_question_t* rendered_question =
        (quizgrind_pdf_rendered_question_t*) questions_iter->data;

    args->scope.current_question = rendered_question->question;

    pango_cairo_update_context(args->cairo, args->textContext);
    pango_layout_context_changed(args->textLayout);

    /* Go to the next page if needed */
    if(rendered_question->flags & args->pageflag)
    {
      cairo_show_page(args->cairo);
      args->matrix.x0 = args->opts->margin;
      args->matrix.y0 = args->opts->margin;
      cairo_set_matrix(args->cairo, &args->matrix);
    }

    /* Draw question number */
    if(rendered_question->flags & QUIZGRIND_PDF_RENDERED_FLAG_QUESTION)
    {
      args->matrix.x0 = args->opts->margin;
      cairo_set_matrix(args->cairo, &args->matrix);
      char exerNumber[15] = {0};
      question_at ++;
      subquestion_at = 0;

      snprintf(exerNumber, 5, "%d.", question_at);
      pango_layout_set_text(args->textLayout, exerNumber, -1);
      pango_cairo_show_layout(args->cairo, args->textLayout);
    }

    args->matrix.x0 = args->opts->margin + QUIZGRIND_PDF_QNUM_WIDTH;
    cairo_set_matrix(args->cairo, &args->matrix);

    // Draw preamble
    if(rendered_question->flags & QUIZGRIND_PDF_RENDERED_FLAG_QUESTION)
    {
      pango_layout_set_width(args->textLayout, rendered_question->width_available * PANGO_SCALE);
      pango_layout_set_line_spacing(args->textLayout, 1.05);
      GSList* preamble = rendered_question->question->eref->preamble;
      while(preamble)
      {
        pango_layout_set_text(args->textLayout, preamble->data, -1);
        pango_cairo_show_layout(args->cairo, args->textLayout);
        int height;
        pango_layout_get_size(args->textLayout, NULL, &height);
        args->matrix.y0 += height / PANGO_SCALE + QUIZGRIND_PDF_WIDGET_PADDING;
        cairo_set_matrix(args->cairo, &args->matrix);

        preamble = g_slist_next(preamble);
      }
    }

    if(rendered_question->flags & QUIZGRIND_PDF_RENDERED_FLAG_SUBQUESTION)
    {
      char exerNumber[15] = {0};

      snprintf(exerNumber, 5, "%c)",
          ((subquestion_at < 26) ? subquestion_at : 0) + 'a');
      pango_layout_set_text(args->textLayout, exerNumber, -1);
      pango_cairo_show_layout(args->cairo, args->textLayout);

      args->matrix.x0 = args->opts->margin + (2 * QUIZGRIND_PDF_QNUM_WIDTH);
      cairo_set_matrix(args->cairo, &args->matrix);

      subquestion_at++;
    }

    for(GList* widgets_iter = rendered_question->widgets; widgets_iter; widgets_iter = widgets_iter->next)
    {
      /* Draw widget */
      quizgrind_pdf_generate_t generate = {0};
      quizgrind_pdf_rendered_widget_t* rendered_widget =
          (quizgrind_pdf_rendered_widget_t*) widgets_iter->data;

      /* Go to the next page if needed */
      if(rendered_widget->flags & args->pageflag)
      {
        cairo_show_page(args->cairo);
        /* Note: not resetting x0 here is intentional. */
        args->matrix.y0 = args->opts->margin;
        cairo_set_matrix(args->cairo, &args->matrix);
      }

      args->scope.current_widget = rendered_widget->widget;

      cairo_rectangle(args->cairo, 0.0, 0.0, rendered_widget->width, rendered_widget->height);
      cairo_clip(args->cairo);

      generate.surface    = args->surface;
      generate.cairo      = args->cairo;
      generate.textLayout = args->textLayout;
      if(args->pageflag == QUIZGRIND_PDF_RENDERED_FLAG_PAGE_BREAK)
      {
        generate.height     = rendered_widget->height;
        generate.width      = rendered_widget->width;
      }
      else if(args->pageflag == QUIZGRIND_PDF_RENDERED_FLAG_PAGE_BREAK_SOLUTION)
      {
        generate.height     = rendered_widget->height_solution;
        generate.width      = rendered_widget->width_solution;
      }
      generate.uData      = rendered_widget->uData;

      cairo_set_source_rgb(generate.cairo, 1.0, 1.0, 1.0);
      cairo_paint(generate.cairo);
      cairo_set_source_rgb(generate.cairo, 0, 0, 0);

      quizgrind_widget_interface_run_handler(
          rendered_widget->widget->type, args->function, &args->scope, &generate);

      cairo_reset_clip(args->cairo);
      cairo_set_source_rgb(args->cairo, 0.0, 0.0, 0.0);
      args->matrix.y0 += generate.height + QUIZGRIND_PDF_WIDGET_PADDING;
      cairo_set_matrix(args->cairo, &args->matrix);
    }

    cairo_set_source_rgb(args->cairo, 0.0, 0.0, 0.0);
  }
}

void quizgrind_pdf_init_context(quizgrind_pdf_render_questions_t* ctx, path_t** path)
{
  /* Set up PDF */
  ctx->surface = cairo_pdf_surface_create(path_get_cstr(path), ctx->opts->pageWidth, ctx->opts->pageHeight);
  ctx->cairo = cairo_create(ctx->surface);
  cairo_translate(ctx->cairo, ctx->opts->margin, ctx->opts->margin);
  ctx->matrix = (cairo_matrix_t){0};
  cairo_get_matrix(ctx->cairo, &ctx->matrix);

  /* Set up fonts */
  ctx->textContext = pango_cairo_create_context(ctx->cairo);
  pango_cairo_context_set_shape_renderer (
      ctx->textContext,
      quizgrind_pdf_shape_draw,
      NULL,
      NULL);
  ctx->textLayout = pango_layout_new(ctx->textContext);
  ctx->fontDescription = pango_font_description_from_string("Sans " XSTR(QUIZGRIND_PDF_TEXT_HEIGHT));
  pango_layout_set_font_description(ctx->textLayout, ctx->fontDescription);
}

void quizgrind_pdf_cleanup_context(quizgrind_pdf_render_questions_t* ctx)
{
  pango_font_description_free(ctx->fontDescription);
  g_object_unref(ctx->textLayout);
  g_object_unref(ctx->textContext);
  cairo_destroy(ctx->cairo);
  cairo_surface_destroy(ctx->surface);
}

gboolean quizgrind_pdf_gen(
    struct quizgrind_project_t_* project,
    quizgrind_pdf_t* opts)
{
  quizgrind_pdf_render_questions_t renderer = {0};
  renderer.scope = QUIZGRIND_SCOPE_NEW(project);
  renderer.opts = opts;

  const int PAGE_BOTTOM = opts->pageHeight - (2*opts->margin);
  const int PAGE_RIGHT = opts->pageWidth - (2*opts->margin);

  /* Set up PDF. This is functioned off since we may have a seperate
   * file for the solution. */
  quizgrind_pdf_init_context(&renderer, &opts->file);

  /* Register functions */
  quizgrind_pdf_register_widget(
      QUIZGRIND_WIDGET_ELEMENT,
      quizgrind_widget_element_pdf_init,
      quizgrind_widget_element_pdf_generate,
      quizgrind_widget_element_pdf_generate_solution,
      quizgrind_widget_element_pdf_cleanup);

  quizgrind_pdf_register_widget(
      QUIZGRIND_WIDGET_LINES,
      quizgrind_widget_lines_pdf_init,
      quizgrind_widget_lines_pdf_generate,
      quizgrind_widget_lines_pdf_generate_solution,
      quizgrind_widget_lines_pdf_cleanup);

  quizgrind_pdf_register_widget(
      QUIZGRIND_WIDGET_IMAGE,
      quizgrind_widget_image_pdf_init,
      quizgrind_widget_image_pdf_generate,
      quizgrind_widget_image_pdf_generate_solution,
      quizgrind_widget_image_pdf_cleanup);

  quizgrind_pdf_register_widget(
      QUIZGRIND_WIDGET_CHOICE,
      quizgrind_widget_choice_pdf_init,
      quizgrind_widget_choice_pdf_generate,
      quizgrind_widget_choice_pdf_generate_solution,
      quizgrind_widget_choice_pdf_cleanup);

  /* Prepare "Solutions" text */
  PangoLayout* layout_solutions = pango_layout_new(renderer.textContext);
  int layout_solutions_height = 0;
  pango_layout_set_text(layout_solutions, "Solutions", -1);
  pango_font_description_set_size(renderer.fontDescription, QUIZGRIND_PDF_TITLE_HEIGHT * PANGO_SCALE);
  pango_layout_set_font_description(layout_solutions, renderer.fontDescription);
  pango_layout_set_width(layout_solutions, PAGE_RIGHT * PANGO_SCALE);
  pango_layout_set_alignment(layout_solutions, PANGO_ALIGN_CENTER);
  pango_layout_get_size(layout_solutions, NULL, &layout_solutions_height);
  layout_solutions_height /= PANGO_SCALE;
  pango_font_description_set_size(renderer.fontDescription, QUIZGRIND_PDF_TEXT_HEIGHT * PANGO_SCALE);

  /* This list will contain the render list for PDF generation */
  renderer.questions = NULL;

  /* Render Questions */
  guint32 height_page = 0;
  guint32 height_page_solution = layout_solutions_height; // Make room for solutions header
  quizgrind_quiz_t* quiz = quizgrind_quiz_next(&renderer.scope, opts->problem_set, opts->gen_mode, opts->gen_num);
  quizgrind_question_t* question = NULL;

  while((question = quizgrind_question_next(&renderer.scope)))
  {
    quizgrind_pdf_rendered_question_t* rendered_question
      = calloc(1, sizeof(quizgrind_pdf_rendered_question_t));

    quizgrind_widget_iter_t iterator;
    quizgrind_widget_t* widget;
    rendered_question->width_available = 0;
    guint32 height_question = 0;
    guint32 height_question_solution = 0;
    rendered_question->question = question;

    quizgrind_question_ref(question);

    /* Determine the level of the question. (eg. 1 vs 1a) */
    if(quiz->sequenceID < 2)
    {
      rendered_question->width_available = PAGE_RIGHT - QUIZGRIND_PDF_QNUM_WIDTH;
      rendered_question->flags |= QUIZGRIND_PDF_RENDERED_FLAG_QUESTION;
    }

    if(quiz->sequenceID > 0)
    {
      rendered_question->width_available = PAGE_RIGHT - (2 * QUIZGRIND_PDF_QNUM_WIDTH);
      rendered_question->flags |= QUIZGRIND_PDF_RENDERED_FLAG_SUBQUESTION;
    }

    if(rendered_question->flags & QUIZGRIND_PDF_RENDERED_FLAG_QUESTION)
    {
      pango_layout_set_width(renderer.textLayout, rendered_question->width_available * PANGO_SCALE);
      pango_layout_set_line_spacing(renderer.textLayout, 1.05);
      GSList* preamble = rendered_question->question->eref->preamble;
      while(preamble)
      {
        int height;
        pango_layout_set_text(renderer.textLayout, preamble->data, -1);
        pango_layout_get_size(renderer.textLayout, NULL, &height);
        height_page += height / PANGO_SCALE + QUIZGRIND_PDF_WIDGET_PADDING;
        height_page_solution += height / PANGO_SCALE + QUIZGRIND_PDF_WIDGET_PADDING ;

        preamble = g_slist_next(preamble);
      }
    }

    /* Widget Pass #1 */
    quizgrind_widget_iter(&iterator, &renderer.scope);
    while((widget = quizgrind_widget_iter_next(&iterator, &renderer.scope)))
    {
      quizgrind_pdf_rendered_widget_t* rendered_widget
        = calloc(1, sizeof(quizgrind_pdf_rendered_widget_t));

      quizgrind_pdf_init_t init = {0};
      init.width = rendered_question->width_available;
      init.width_solution = rendered_question->width_available;
      init.baseLayout = renderer.textLayout;
      quizgrind_widget_interface_run_handler(widget->type, QUIZGRIND_WIDGET_FUN_PDF_INIT, &renderer.scope, &init);

      /* This will have some smarts later, but for now just go ahead and copy */
      rendered_widget->uData = init.uData;
      rendered_widget->height = init.height;
      rendered_widget->width = init.width;

      rendered_widget->height_solution = init.height_solution;
      rendered_widget->width_solution = init.width_solution;

      rendered_widget->widget = widget;
      rendered_widget->flags |= init.flags;

      rendered_question->widgets = g_list_prepend(rendered_question->widgets, rendered_widget);

      height_question += rendered_widget->height + QUIZGRIND_PDF_WIDGET_PADDING;
      height_question_solution += rendered_widget->height_solution + QUIZGRIND_PDF_WIDGET_PADDING;
    }

    rendered_question->widgets = g_list_reverse(rendered_question->widgets);

    quizgrind_pdf_push_height(rendered_question, &height_page,
        &height_question, PAGE_BOTTOM, FALSE);

    quizgrind_pdf_push_height(rendered_question, &height_page_solution,
        &height_question_solution, PAGE_BOTTOM, TRUE);

    renderer.questions = g_list_prepend(renderer.questions, rendered_question);
  }
  renderer.questions = g_list_reverse(renderer.questions);

  /* Render questions */
  renderer.function = QUIZGRIND_WIDGET_FUN_PDF_GENERATE;
  renderer.pageflag = QUIZGRIND_PDF_RENDERED_FLAG_PAGE_BREAK;
  quizgrind_pdf_render_questions(&renderer);

  if(opts->sfile)
  {
    // Swap out contexts to solutions file
    quizgrind_pdf_cleanup_context(&renderer);
    quizgrind_pdf_init_context(&renderer, &opts->sfile);
  }
  else
    { cairo_show_page(renderer.cairo); }

  renderer.matrix.x0 = opts->margin;
  renderer.matrix.y0 = opts->margin;
  cairo_set_matrix(renderer.cairo, &renderer.matrix);

  pango_cairo_show_layout(renderer.cairo, layout_solutions);
  renderer.matrix.y0 += layout_solutions_height;
  cairo_set_matrix(renderer.cairo, &renderer.matrix);

  /* Render solutions */
  renderer.function = QUIZGRIND_WIDGET_FUN_PDF_GENERATE_SOLUTION;
  renderer.pageflag = QUIZGRIND_PDF_RENDERED_FLAG_PAGE_BREAK_SOLUTION;
  quizgrind_pdf_render_questions(&renderer);

  for(GList* questions_iter = renderer.questions; questions_iter; questions_iter = questions_iter->next)
  {
    quizgrind_pdf_rendered_question_t* rendered_question =
        (quizgrind_pdf_rendered_question_t*) questions_iter->data;
    for(GList* widgets_iter = rendered_question->widgets; widgets_iter; widgets_iter = widgets_iter->next)
      { free(widgets_iter->data); }
    g_list_free(rendered_question->widgets);
    quizgrind_question_unref(&renderer.scope, rendered_question->question);
    free(rendered_question);
  }
  g_list_free(renderer.questions);

  cairo_show_page(renderer.cairo);

  /* Clean things up */
  pango_cairo_font_map_set_default(NULL);
  quizgrind_pdf_cleanup_context(&renderer);

  return true;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
