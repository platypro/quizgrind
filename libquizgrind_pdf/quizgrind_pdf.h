/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

/* Quizgrind PDF system
 *
 *  The Quizgrind PDF system uses the Cairo graphics library to generate PDF files.
 *  It interfaces with the libquizgrind API directly.
 *  The system makes two passes of the widgets for each question generated:
 *
 *  The first pass looks at the widgets and makes decisions in relation to position and size. This is
 *  for complex features such as placing widgets to the side of the main content, or spreading a question
 *  across an entire page. It is also used for determining total marks for a quiz or how many pages will
 *  be generated.
 *
 *  The second pass runs through the widgets again and gets them to write their contents into the PDF.
 */

/* The PDF system is currently being rewritten from scratch, some inline to-dos are here: */
/* TODO: [PDF] Add title blocks */
/* TODO: [PDF] Add title pages */
/* TODO: [PDF] Show marks for each question and total */
/* TODO: [PDF] Sidebar widgets */
/* TODO: [PDF] Full-page questions */
/* TODO: [PDF] Add Page Numbers */

#ifndef LIBQUIZGRIND_PDF_QUIZGRIND_PDF_H_
#define LIBQUIZGRIND_PDF_QUIZGRIND_PDF_H_

#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>
#include <librsvg/rsvg.h>
#include <pango/pango.h>
#include <pango/pangocairo.h>

/* Height of text in PDF. */
#define QUIZGRIND_PDF_TEXT_HEIGHT 12

/* Height of titles in PDF */
#define QUIZGRIND_PDF_TITLE_HEIGHT 20

/* Width of question number on left. */
#define QUIZGRIND_PDF_QNUM_WIDTH 40

/* Vertical padding between widgets */
#define QUIZGRIND_PDF_WIDGET_PADDING 12

#define QUIZGRIND_PDF_DEFAULT_HEADERS "Name,Student #;Block"

#define QUIZGRIND_PDF_DEFAULT_MARGIN 50
#define QUIZGRIND_PDF_DEFAULT_PAGE_WIDTH 612
#define QUIZGRIND_PDF_DEFAULT_PAGE_HEIGHT 792

typedef struct quizgrind_pdf_t_
{
  char* headers;
  double pageWidth;
  double pageHeight;
  double margin;

  char* problem_set;

  path_t* file;
  path_t* sfile;

  guint32 gen_num;
  quizgrind_gen_mode_t gen_mode;

} quizgrind_pdf_t;

extern gboolean quizgrind_pdf_gen(
    struct quizgrind_project_t_* project,
    quizgrind_pdf_t* opts);

#endif /* LIBQUIZGRIND_PDF_QUIZGRIND_PDF_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
