/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_PDF_WIDGET_WIDGET_H_
#define LIBQUIZGRIND_PDF_WIDGET_WIDGET_H_

#define QUIZGRIND_PDF_RENDERED_FLAG_SUBQUESTION         (1<<0)
#define QUIZGRIND_PDF_RENDERED_FLAG_QUESTION            (1<<1)
#define QUIZGRIND_PDF_RENDERED_FLAG_PAGE_BREAK          (1<<2)
#define QUIZGRIND_PDF_RENDERED_FLAG_PAGE_BREAK_SOLUTION (1<<3)
#define QUIZGRIND_PDF_RENDERED_FLAG_SIDEBAR             (1<<4)
#define QUIZGRIND_PDF_RENDERED_FLAG_HAS_SOLUTION        (1<<5)

#define QUIZGRIND_WIDGET_FUN_PDF_INIT              "pdf_init"
#define QUIZGRIND_WIDGET_FUN_PDF_GENERATE          "pdf_generate"
#define QUIZGRIND_WIDGET_FUN_PDF_GENERATE_SOLUTION "pdf_generate_solution"
#define QUIZGRIND_WIDGET_FUN_PDF_CLEANUP           "pdf_cleanup"

typedef guint32 quizgrind_pdf_rendered_flags_t;

typedef struct quizgrind_pdf_rendered_widget_t_
{
  quizgrind_pdf_rendered_flags_t flags;

  quizgrind_widget_t* widget;
  void* uData;

  guint32 width;
  guint32 height;

  guint32 width_solution;
  guint32 height_solution;

} quizgrind_pdf_rendered_widget_t;

typedef struct quizgrind_pdf_rendered_question_t_
{
  quizgrind_pdf_rendered_flags_t flags;
  GList* widgets;

  quizgrind_question_t* question;

  guint32 width_sidebar;

  // Width available to question for drawing
  guint32 width_available;

} quizgrind_pdf_rendered_question_t;

typedef struct quizgrind_pdf_init_t_
{
  void* uData;

  PangoLayout* baseLayout;
  quizgrind_pdf_rendered_flags_t flags;

  gint32 width;
  gint32 height;

  gint32 width_solution;
  gint32 height_solution;

} quizgrind_pdf_init_t;

typedef struct quizgrind_pdf_generate_t_
{
  void* uData;

  cairo_surface_t* surface;
  cairo_t* cairo;

  PangoLayout* textLayout;

  guint32 width;
  guint32 height;

} quizgrind_pdf_generate_t;

extern guint32 quizgrind_widget_element_pdf_init(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_element_pdf_generate(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_element_pdf_generate_solution(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_element_pdf_cleanup(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_lines_pdf_init(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_lines_pdf_generate(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_lines_pdf_generate_solution(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_lines_pdf_cleanup(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_image_pdf_init(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_image_pdf_generate(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_image_pdf_generate_solution(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_image_pdf_cleanup(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_choice_pdf_init(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_choice_pdf_generate(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_choice_pdf_generate_solution(
    struct quizgrind_scope_t_* scope, void* data_);

extern guint32 quizgrind_widget_choice_pdf_cleanup(
    struct quizgrind_scope_t_* scope, void* data_);


#endif /* LIBQUIZGRIND_PDF_WIDGET_WIDGET_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
