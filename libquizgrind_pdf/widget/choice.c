/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#define QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_MARKS 25
#define QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_QNUM  12
#define QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_PADDING 5

#include <libquizgrind/quizgrind.h>
#include <libquizgrind/widget/choice.h>
#include <libquizgrind_pdf/quizgrind_pdf.h>
#include <libquizgrind_pdf/widget.h>

#include <pmus-input-stream.h>

char* rnumerals[] = {"i.", "ii.", "iii.", "iv.", "v.", "vi.", "vii.", "viii.", "viv.", "x"};

typedef struct quizgrind_widget_choice_pdf_draw_
{
  struct quizgrind_widget_choice_pdf_draw_* next;

  enum {
   QUIZGRIND_WIDGET_CHOICE_PDF_DRAW_TEXT,
   QUIZGRIND_WIDGET_CHOICE_PDF_DRAW_SVG
  } type;
  char* text;
  RsvgHandle* svg;

  gint32 marks;

} quizgrind_widget_choice_pdf_draw;

typedef struct quizgrind_widget_choice_pdf_priv_
{
  quizgrind_widget_choice_pdf_draw* draw;

  PangoLayout* marksLayout;

} quizgrind_widget_choice_pdf_priv;

guint32 quizgrind_widget_choice_pdf_init(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_pdf_init_t* data = (quizgrind_pdf_init_t*) data_;
  quizgrind_widget_choice_t* widget = (quizgrind_widget_choice_t*) scope->current_widget;

  quizgrind_widget_choice_pdf_priv* priv = calloc(1, sizeof(quizgrind_widget_choice_pdf_priv));
  data->uData = priv;

  priv->marksLayout = pango_layout_copy(data->baseLayout);
  PangoAttrList* attrList = pango_attr_list_new();
  PangoAttribute* marksWeight = pango_attr_weight_new(PANGO_WEIGHT_BOLD);
  pango_attr_list_insert(attrList, marksWeight);
  pango_layout_set_attributes(priv->marksLayout, attrList);
  pango_attr_list_unref(attrList);

  data->height = 0;

  GList* item_ = widget->choices->tail;
  while(item_)
  {
    quizgrind_widget_choice_item_t* item = item_->data;
    quizgrind_widget_choice_pdf_draw* draw = calloc(1, sizeof(quizgrind_widget_choice_pdf_draw));
    draw->next = priv->draw;
    draw->marks = item->marks;
    priv->draw = draw;
    switch(item->type)
    {
    case QUIZGRIND_WIDGET_CHOICE_TYPE_TEXT:
    {
      switch(item->format)
      {
      case QUIZGRIND_WIDGET_CHOICE_FORMAT_TEMPLATE:
        draw->text = mustache_generate_mem(item->template);
        break;
      case QUIZGRIND_WIDGET_CHOICE_FORMAT_NORMAL:
        draw->text = item->text;
        break;
      }

      gint32 height;
      pango_layout_set_width(data->baseLayout, data->width * PANGO_SCALE
          - (QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_MARKS * PANGO_SCALE)
          - (QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_QNUM * PANGO_SCALE));
      pango_layout_set_text(data->baseLayout, draw->text, -1);
      pango_layout_get_size(data->baseLayout, NULL, &height);
      data->height += height / PANGO_SCALE;

      pango_layout_set_width(data->baseLayout, data->width_solution * PANGO_SCALE
          - (QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_MARKS * PANGO_SCALE));
      pango_layout_get_size(data->baseLayout, NULL, &height);
      data->height_solution += height / PANGO_SCALE;
      break;
    }
    case QUIZGRIND_WIDGET_CHOICE_TYPE_SVG:
    {
      GInputStream* stream;
      GError* err = NULL;

      switch(item->format)
      {
      case QUIZGRIND_WIDGET_CHOICE_FORMAT_TEMPLATE:
        stream = G_INPUT_STREAM(pmus_input_stream_open(item->template, &err));
        break;
      case QUIZGRIND_WIDGET_CHOICE_FORMAT_NORMAL:
        stream = G_INPUT_STREAM(g_memory_input_stream_new_from_data(item->text, -1, NULL));
        break;
      }

      draw->svg =
        rsvg_handle_new_from_stream_sync (
          stream,
          NULL,
          RSVG_HANDLE_FLAG_KEEP_IMAGE_DATA,
          NULL,
          &err);

      if(!draw->svg)
      {
       printf("Error in SVG: %s\n", err->message);
       return 0;
      }

      gdouble height;
      rsvg_handle_get_intrinsic_size_in_pixels(draw->svg, NULL, &height);

      data->height += height;
      data->height_solution += height;

      g_object_unref(stream);
      break;
    }
    }
    item_ = item_->prev;
    if(item_)
    {
      data->height += QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_PADDING;
      data->height_solution += QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_PADDING;
    }
  }
  return 0;
}

guint32 quizgrind_widget_choice_pdf_generate(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_pdf_generate_t* data = (quizgrind_pdf_generate_t*) data_;
  quizgrind_widget_choice_pdf_priv* priv = data->uData;
  quizgrind_widget_choice_pdf_draw* draw = priv->draw;
  guint32 qnum = 0;
  while(draw)
  {
    pango_layout_set_width(data->textLayout, (QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_QNUM * PANGO_SCALE));
    pango_layout_set_text(data->textLayout, (qnum < 10) ? rnumerals[qnum] : ".", -1);
    pango_cairo_show_layout(data->cairo, data->textLayout);

    cairo_translate(data->cairo, QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_QNUM, 0);

    if(draw->text)
    {
      pango_layout_set_width(data->textLayout, data->width * PANGO_SCALE
          - (QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_QNUM * PANGO_SCALE));
      pango_layout_set_text(data->textLayout, draw->text, -1);
      pango_cairo_show_layout(data->cairo, data->textLayout);
      gint32 height;
      pango_layout_get_size(data->textLayout, NULL, &height);
      cairo_translate(data->cairo, 0, height / PANGO_SCALE);
    }
    else if(draw->svg)
    {
      gdouble w, h;
      rsvg_handle_get_intrinsic_size_in_pixels(draw->svg, &w, &h);
      RsvgRectangle viewport = {.x=0, .y=0, .width=w, .height=h};
      rsvg_handle_render_document(draw->svg, data->cairo, &viewport, NULL);
      cairo_translate(data->cairo, 0, h);
    }
    draw = draw->next;
    qnum++;
    cairo_translate(data->cairo,
        -QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_QNUM,
        QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_PADDING);
  }
  return 0;
}

guint32 quizgrind_widget_choice_pdf_generate_solution(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_pdf_generate_t* data = (quizgrind_pdf_generate_t*) data_;
  quizgrind_widget_choice_pdf_priv* priv = data->uData;
  quizgrind_widget_choice_pdf_draw* draw = priv->draw;
  guint32 qnum = 0;
  while(draw)
  {
    pango_layout_set_width(data->textLayout, (QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_QNUM / PANGO_SCALE));
    pango_layout_set_text(data->textLayout, (qnum < 10) ? rnumerals[qnum] : ".", -1);
    pango_cairo_show_layout(data->cairo, data->textLayout);

    cairo_translate(data->cairo, QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_QNUM, 0);
    gint32 height = 0;
    if(draw->text)
    {
      pango_layout_set_width(data->textLayout, data->width * PANGO_SCALE
          - (QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_QNUM * PANGO_SCALE)
          - (QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_MARKS * PANGO_SCALE));
      pango_layout_set_text(data->textLayout, draw->text, -1);
      pango_cairo_show_layout(data->cairo, data->textLayout);

      pango_layout_get_size(data->textLayout, NULL, &height);
      height /= PANGO_SCALE;
    }
    else if(draw->svg)
    {
      gdouble w, h;
      rsvg_handle_get_intrinsic_size_in_pixels(draw->svg, &w, &h);
      height = h;
      RsvgRectangle viewport = {.x=0, .y=0, .width=w, .height=h};
      rsvg_handle_render_document(draw->svg, data->cairo, &viewport, NULL);
    }
    cairo_translate(data->cairo,
            data->width - QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_QNUM - QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_MARKS,
            0);

    char buf[20];
    snprintf(buf, 20, "(%+d)", draw->marks);

    pango_layout_set_text(priv->marksLayout, buf, -1);
    pango_cairo_show_layout(data->cairo, priv->marksLayout);

    cairo_translate(data->cairo,
        (double)QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_MARKS - (double)data->width,
        QUIZGRIND_WIDGET_CHOICE_PDF_MARGIN_PADDING + height);

    draw = draw->next;
    qnum++;
  }
  return 0;
}

guint32 quizgrind_widget_choice_pdf_cleanup(
    struct quizgrind_scope_t_* scope, void* data_)
{
  return 0;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
