/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include <libquizgrind/quizgrind.h>
#include <libquizgrind/widget/element.h>
#include <libquizgrind_pdf/quizgrind_pdf.h>
#include <libquizgrind_pdf/widget.h>

typedef struct quizgrind_widget_element_pdf_priv_
{
  PangoLayout* textLayout;
  PangoLayout* textLayout_answer;

} quizgrind_widget_element_pdf_priv;

guint32 quizgrind_widget_element_pdf_init(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_pdf_init_t* data = (quizgrind_pdf_init_t*) data_;
  quizgrind_widget_element_t* widget = (quizgrind_widget_element_t*) scope->current_widget;

  quizgrind_widget_element_pdf_priv* priv = calloc(1, sizeof(quizgrind_widget_element_pdf_priv));
  data->uData = (void*) priv;
  data->flags = 0;

  priv->textLayout = pango_layout_copy(data->baseLayout);
  priv->textLayout_answer = pango_layout_copy(data->baseLayout);

  GString* string = g_string_new(NULL);
  GString* string_answer = g_string_new(NULL);
  PangoAttrList* attrs = pango_attr_list_new();
  PangoAttrList* attrs_answer = pango_attr_list_new();

  quizgrind_widget_element_part_t* part = widget->part;

  while(part)
  {
    switch(part->type)
    {
    case QUIZGRIND_ELEMENT_TYPE_TEXT:
      g_string_append(string, part->text.text);
      g_string_append(string_answer, part->text.text);
      break;
    case QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER:
    case QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT:
    {
      PangoFontMetrics* metrics = pango_context_get_metrics (
          pango_layout_get_context(priv->textLayout), NULL, NULL);
      PangoRectangle rect = (PangoRectangle){
       0, 0,
       50 * PANGO_SCALE, 0 };

      PangoAttribute* shape = pango_attr_shape_new(&rect, &rect);
      PangoAttribute* rise = pango_attr_rise_new(-pango_font_metrics_get_descent(metrics));
      shape->end_index = string->len + 1;
      shape->start_index = string->len;
      rise->end_index = shape->end_index;
      rise->start_index = shape->start_index;
      pango_attr_list_insert(attrs, shape);
      pango_attr_list_insert(attrs, rise);
      pango_font_metrics_unref(metrics);
      g_string_append(string, " ");

      PangoAttribute* ans_bold = pango_attr_weight_new(PANGO_WEIGHT_BOLD);
      ans_bold->start_index = string_answer->len;

      if(part->type == QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER)
      {
        quizgrind_widget_element_part_input_number_answer_t* ans =
            part->input_number.answers;
        while(ans)
        {
          g_string_append_printf(string_answer, "%.*f", part->input_number.mindp,
              ans->value);
          ans = ans->next;
          if(ans) g_string_append_printf(string_answer, " or ");
        }
      }
      else if(part->type == QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT)
      {
        quizgrind_widget_element_part_input_text_answer_t* ans =
            part->input_text.answers;
        while(ans)
        {
          g_string_append_printf(string_answer, "%s", ans->value);
          ans = ans->next;
          if(ans) g_string_append_printf(string_answer, " or ");
        }
      }

      ans_bold->end_index = string_answer->len;
      pango_attr_list_insert(attrs_answer, ans_bold);

      break;
    }
    }
    part = part->next;
  }

  pango_layout_set_width(priv->textLayout, data->width * PANGO_SCALE);
  pango_layout_set_text(priv->textLayout, string->str, -1);
  pango_layout_set_attributes(priv->textLayout, attrs);
  pango_attr_list_unref(attrs);
  g_string_free(string, TRUE);

  pango_layout_set_width(priv->textLayout_answer, data->width * PANGO_SCALE);
  pango_layout_set_text(priv->textLayout_answer, string_answer->str, -1);
  pango_layout_set_attributes(priv->textLayout_answer, attrs_answer);
  pango_attr_list_unref(attrs_answer);
  g_string_free(string_answer, TRUE);

  /* rect.width stays the same */
  pango_layout_get_size(priv->textLayout, NULL, &data->height);
  data->height /= PANGO_SCALE;

  pango_layout_get_size(priv->textLayout_answer, NULL, &data->height_solution);
  data->height_solution /= PANGO_SCALE;
  return 0;
}

guint32 quizgrind_widget_element_pdf_generate(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_pdf_generate_t* data = (quizgrind_pdf_generate_t*) data_;
  quizgrind_widget_element_pdf_priv* uData = (quizgrind_widget_element_pdf_priv*)data->uData;
  pango_cairo_show_layout(data->cairo, uData->textLayout);
  return 0;
}

guint32 quizgrind_widget_element_pdf_generate_solution(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_pdf_generate_t* data = (quizgrind_pdf_generate_t*) data_;
  quizgrind_widget_element_pdf_priv* uData = (quizgrind_widget_element_pdf_priv*)data->uData;
  pango_cairo_show_layout(data->cairo, uData->textLayout_answer);
  return 0;
}

guint32 quizgrind_widget_element_pdf_cleanup(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_element_pdf_priv* data = (quizgrind_widget_element_pdf_priv*)data_;

  g_object_unref(data->textLayout);
  g_object_unref(data->textLayout_answer);
  free(data);
  return 0;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
