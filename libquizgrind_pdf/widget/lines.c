/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include <libquizgrind/quizgrind.h>
#include <libquizgrind/widget/lines.h>
#include <libquizgrind_pdf/quizgrind_pdf.h>
#include <libquizgrind_pdf/widget.h>

typedef struct quizgrind_widget_lines_pdf_priv_
{
  PangoLayout* keyLayout;

} quizgrind_widget_lines_pdf_priv;

guint32 quizgrind_widget_lines_pdf_init(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_lines_t* lines = (quizgrind_widget_lines_t*) scope->current_widget;
  quizgrind_pdf_init_t* data = (quizgrind_pdf_init_t*) data_;
  quizgrind_widget_lines_pdf_priv* priv = calloc(1, sizeof(quizgrind_widget_lines_pdf_priv));
  priv->keyLayout = pango_layout_copy(data->baseLayout);

  data->uData = priv;

  PangoFontMetrics* metrics = pango_context_get_metrics (
      pango_layout_get_context(data->baseLayout), NULL, NULL);

  data->height = lines->length * pango_font_metrics_get_height(metrics) / PANGO_SCALE;

  if(lines->caption)
  {
    pango_layout_set_text(priv->keyLayout, lines->caption, -1);
    int captHeight = 0;
    pango_layout_get_size(priv->keyLayout, NULL, &captHeight);
    data->height += captHeight / PANGO_SCALE;
  }

  if(lines->key)
  {
    pango_layout_set_text(priv->keyLayout, lines->key, -1);
    pango_layout_get_size(priv->keyLayout, NULL, &data->height_solution);
    data->height_solution /= PANGO_SCALE;
  }

  return 0;
}

guint32 quizgrind_widget_lines_pdf_generate(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_lines_t* lines = (quizgrind_widget_lines_t*) scope->current_widget;
  if(lines->caption)
  {
    quizgrind_pdf_generate_t* data = (quizgrind_pdf_generate_t*) data_;
    quizgrind_widget_lines_pdf_priv* priv = (quizgrind_widget_lines_pdf_priv*)data->uData;
    pango_layout_set_text(priv->keyLayout, lines->caption, -1);
    pango_cairo_show_layout(data->cairo, priv->keyLayout);
  }
  return 0;
}

guint32 quizgrind_widget_lines_pdf_generate_solution(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_lines_t* lines = (quizgrind_widget_lines_t*) scope->current_widget;
  if(lines->key)
  {
    quizgrind_pdf_generate_t* data = (quizgrind_pdf_generate_t*) data_;
    quizgrind_widget_lines_pdf_priv* priv = (quizgrind_widget_lines_pdf_priv*)data->uData;
    pango_layout_set_text(priv->keyLayout, lines->key, -1);
    pango_cairo_show_layout(data->cairo, priv->keyLayout);
  }
  return 0;
}

guint32 quizgrind_widget_lines_pdf_cleanup(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_lines_pdf_priv* data = (quizgrind_widget_lines_pdf_priv*)data_;

  g_object_unref(data->keyLayout);
  free(data);
  return 0;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
