/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include <libquizgrind/quizgrind.h>
#include <libquizgrind/widget/image.h>
#include <libquizgrind_pdf/quizgrind_pdf.h>
#include <libquizgrind_pdf/widget.h>

#include <pmus-input-stream.h>
#include <gdk/gdk.h>

typedef struct quizgrind_widget_image_pdf_priv_
{
  double svg_scale;
  RsvgHandle* svg;
  GdkPixbuf* raster;

} quizgrind_widget_image_pdf_priv;

extern guint32 quizgrind_widget_image_pdf_init(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_pdf_init_t* data = (quizgrind_pdf_init_t*) data_;
  quizgrind_widget_image_t* widget = (quizgrind_widget_image_t*) scope->current_widget;
  quizgrind_widget_image_pdf_priv* priv = calloc(1, sizeof(quizgrind_widget_image_pdf_priv));
  data->uData = (void*) priv;
  if(widget->format == QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG_TEMPLATE || widget->format == QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG)
  {
    GError* err = NULL;
    GInputStream* stream = NULL;

    switch(widget->format)
    {
    case QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG_TEMPLATE:
      stream = G_INPUT_STREAM(pmus_input_stream_open(widget->svg_template, &err));
      break;
    case QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG:
      stream = G_INPUT_STREAM(g_memory_input_stream_new_from_data(widget->svg, -1, NULL));
      break;
    default: break;
    }

    priv->svg =
     rsvg_handle_new_from_stream_sync (
       stream,
       NULL,
       RSVG_HANDLE_FLAG_KEEP_IMAGE_DATA,
       NULL,
       &err);

    g_object_unref(stream);

    if(!priv->svg)
    {
     printf("Error in SVG: %s\n", err->message);
     return 0;
    }
    else
    {
      double height, width;
      rsvg_handle_get_intrinsic_size_in_pixels(priv->svg, &width, &height);
      if(width > data->width)
      {
        priv->svg_scale = data->width / width;
        height *= priv->svg_scale;
      }
      else
      {
        priv->svg_scale = 1.0;
      }

      data->height = height;
      data->height_solution = 0;
    }
  }
  else if(widget->format == QUIZGRIND_WIDGET_IMAGE_FORMAT_RASTER)
  {
    path_t* imgpath = path_clone(&scope->current_project->path_exercise);
    path_push(&imgpath, scope->current_exercise->id);
    path_push(&imgpath, widget->raster);

    priv->raster = gdk_pixbuf_new_from_file_at_scale(path_get_cstr(&imgpath), -1, 75, TRUE, NULL);
    data->height = gdk_pixbuf_get_height(priv->raster);
    data->height_solution = 0;

    path_destroy(&imgpath);
  }
  return 0;
}

extern guint32 quizgrind_widget_image_pdf_generate(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_pdf_generate_t* data = (quizgrind_pdf_generate_t*) data_;
  quizgrind_widget_image_pdf_priv* priv = (quizgrind_widget_image_pdf_priv*) data->uData;
  if(priv->svg)
  {
    gdouble w, h;
    rsvg_handle_get_intrinsic_size_in_pixels(priv->svg, &w, &h);
    RsvgRectangle viewport = {.x=0, .y=0, .width=w, .height=h};
    cairo_scale(data->cairo, priv->svg_scale, priv->svg_scale);
    rsvg_handle_render_document(priv->svg, data->cairo, &viewport, NULL);
  }
  else if(priv->raster)
  {
    gdk_cairo_set_source_pixbuf(data->cairo, priv->raster, 0, 0);
    cairo_rectangle(data->cairo, 0, 0,
        gdk_pixbuf_get_width(priv->raster),
        gdk_pixbuf_get_height(priv->raster));
    cairo_fill(data->cairo);
  }
  return 0;
}

extern guint32 quizgrind_widget_image_pdf_generate_solution(
    struct quizgrind_scope_t_* scope, void* data_)
{
  return 0;
}

extern guint32 quizgrind_widget_image_pdf_cleanup(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_pdf_generate_t* data = (quizgrind_pdf_generate_t*) data_;
  quizgrind_widget_image_pdf_priv* priv = (quizgrind_widget_image_pdf_priv*) data->uData;
  if(priv->svg)
  {
    g_object_unref(priv->svg);
  }
  return 0;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
