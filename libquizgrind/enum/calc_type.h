/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_CORE_CALC_TYPE_H_
#define LIBQUIZGRIND_CORE_CALC_TYPE_H_

#include "util/enum.h"

typedef enum quizgrind_calc_type_t_
{
  QUIZGRIND_CALC_TYPE_BANNED,
  QUIZGRIND_CALC_TYPE_BASIC,
  QUIZGRIND_CALC_TYPE_GRAPHING,
  QUIZGRIND_CALC_TYPE_NONE,
  QUIZGRIND_CALC_TYPE_SCIENTIFIC

} quizgrind_calc_type_t;

extern quizgrind_enum_t quizgrind_calc_type;

#ifdef QUIZGRIND_ENUM_DEFINITION
quizgrind_enum_element_t quizgrind_calc_type_index[] = {
    {"banned",     QUIZGRIND_CALC_TYPE_BANNED},
    {"basic",      QUIZGRIND_CALC_TYPE_BASIC},
    {"graphing",   QUIZGRIND_CALC_TYPE_GRAPHING},
    {"none",       QUIZGRIND_CALC_TYPE_NONE},
    {"scientific", QUIZGRIND_CALC_TYPE_SCIENTIFIC}
};

quizgrind_enum_t quizgrind_calc_type =
{
    .elements = quizgrind_calc_type_index,
    .elements_count = NUMELEMENTS(quizgrind_calc_type_index)
};
#endif

#endif /* LIBQUIZGRIND_CORE_CALC_TYPE_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
