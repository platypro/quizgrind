/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_CORE_GEN_MODE_H_
#define LIBQUIZGRIND_CORE_GEN_MODE_H_

typedef enum quizgrind_gen_mode_t_
{
  QUIZGRIND_GEN_MODE_RANDOM,
  QUIZGRIND_GEN_MODE_ORDERED,
  QUIZGRIND_GEN_MODE_UNORDERED

} quizgrind_gen_mode_t;

extern quizgrind_enum_t quizgrind_gen_mode;

#ifdef QUIZGRIND_ENUM_DEFINITION
quizgrind_enum_element_t quizgrind_gen_mode_index[] = {
    {"ordered",   QUIZGRIND_GEN_MODE_ORDERED},
    {"random",    QUIZGRIND_GEN_MODE_RANDOM},
    {"unordered", QUIZGRIND_GEN_MODE_UNORDERED}
};

quizgrind_enum_t quizgrind_gen_mode =
{
    .elements = quizgrind_gen_mode_index,
    .elements_count = NUMELEMENTS(quizgrind_gen_mode_index)
};
#endif

#endif /* LIBQUIZGRIND_CORE_GEN_MODE_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
