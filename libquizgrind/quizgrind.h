/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_QUIZGRIND_H_
#define LIBQUIZGRIND_QUIZGRIND_H_

#include <glib.h>
#include <gio/gio.h>

#ifdef _WIN32
#include <windows.h>
#endif

#include "core/scope.h"
#include "keys.h"

#include "core/project.h"
#include "core/quiz.h"
#include "core/question.h"
#include "core/problem_set.h"
#include "core/exercise_ref.h"
#include "core/exercise.h"
#include "core/widget.h"
#include "core/script.h"
#include "core/variable.h"
#include "enum/calc_type.h"
#include "enum/gen_mode.h"

#include "quizgrind_version.h"

#endif /* LIBQUIZGRIND_QUIZGRIND_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
