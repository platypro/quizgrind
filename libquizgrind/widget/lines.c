/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "quizgrind.h"
#include "widget/lines.h"

extern guint32 quizgrind_widget_lines_init(
    struct quizgrind_scope_t_* scope, void* data)
{
  return 0;
}

extern guint32 quizgrind_widget_lines_cleanup(
    struct quizgrind_scope_t_* scope, void* data)
{
  quizgrind_widget_lines_t* widget = (quizgrind_widget_lines_t*) scope->current_widget;

  if(widget->key)
    { free(widget->key); }

  if(widget->caption)
    { free(widget->caption); }

  return 0;
}

extern guint32 quizgrind_widget_lines_from_json(
    struct quizgrind_scope_t_* scope, void* element_)
{
  WJElement element = (WJElement) element_;
  quizgrind_widget_lines_t* widget = (quizgrind_widget_lines_t*) scope->current_widget;
  widget->key = g_strdup(WJEString(element, KEY_LINEINPUT_KEY, WJE_GET, NULL));
  widget->length = WJEUInt32(element, KEY_LINEINPUT_LENGTH, WJE_GET, 0);
  widget->marks = WJEUInt32(element, KEY_LINEINPUT_MARKS, WJE_GET, 0);
  widget->caption = g_strdup(WJEString(element, KEY_LINEINPUT_CAPTION, WJE_GET, NULL));
  return true;
}

guint32 quizgrind_widget_lines_answer_max(
    struct quizgrind_scope_t_* scope, void* data)
{
  quizgrind_widget_lines_t* widget = (quizgrind_widget_lines_t*) scope->current_widget;

  return widget->marks;
}

guint32 quizgrind_widget_lines_to_json(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_to_json_t* data = (quizgrind_widget_to_json_t*) data_;
  quizgrind_widget_lines_t* widget = (quizgrind_widget_lines_t*) scope->current_widget;

  WJWString(KEY_LINEINPUT_KEY, widget->key, true, data->writer);
  WJWInt32(KEY_LINEINPUT_LENGTH, widget->length, data->writer);
  WJWInt32(KEY_LINEINPUT_MARKS, widget->marks, data->writer);
  return 0;
}

static int quizgrind_widget_lines__index(lua_State* L)
{
  quizgrind_widget_lines_t* widget = *(quizgrind_widget_lines_t**)lua_topointer(L, 1);
  const char* name = lua_tostring(L, 2);

  if(!strcmp(name, KEY_LINEINPUT_LENGTH))
    { lua_pushnumber(L, widget->length); }
  else if(!strcmp(name, KEY_LINEINPUT_KEY))
    { lua_pushstring(L, widget->key); }
  else if(!strcmp(name, KEY_LINEINPUT_MARKS))
    { lua_pushnumber(L, widget->marks); }

  return 1;
}

static int quizgrind_widget_lines__newindex(lua_State* L)
{
  quizgrind_widget_lines_t* widget = *(quizgrind_widget_lines_t**)lua_topointer(L, 1);
  const char* name = lua_tostring(L, 2);

  if(!strcmp(name, KEY_LINEINPUT_LENGTH))
    { widget->length = lua_tonumber(L, 3); }
  else if(!strcmp(name, KEY_LINEINPUT_KEY))
  {
    if(widget->key)
      { free(widget->key); }
    widget->key = g_strdup(lua_tostring(L, 3));
  }
  else if(!strcmp(name, KEY_LINEINPUT_MARKS))
    { widget->marks = lua_tonumber(L, 3); }
  else if(!strcmp(name, KEY_LINEINPUT_CAPTION))
  {
    if(widget->caption)
      { free(widget->caption); }
    widget->caption = g_strdup(lua_tostring(L, 3));
  }

  return 0;
}

guint32 quizgrind_widget_lines_setup_script(
    struct quizgrind_scope_t_* scope, void* data_)
{ 
  quizgrind_widget_setup_script_t* data = (quizgrind_widget_setup_script_t*) data_;

  lua_pushstring(data->L, "__index");
  lua_pushcfunction(data->L, quizgrind_widget_lines__index);
  lua_settable(data->L, data->idx);

  lua_pushstring(data->L, "__newindex");
  lua_pushcfunction(data->L, quizgrind_widget_lines__newindex);
  lua_settable(data->L, data->idx);

  return 0;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
