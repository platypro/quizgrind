/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "lua.h"
#include "quizgrind.h"
#include "widget/element.h"

#include <ctype.h>
#include <string.h>

#include "core/exercise.h"
#include "core/widget.h"

quizgrind_widget_element_part_t* quizgrind_widget_element_add_part(
    quizgrind_widget_element_t* widget, quizgrind_widget_element_type_t type)
{
  *widget->part_end = calloc(1, sizeof(quizgrind_widget_element_part_t));
  quizgrind_widget_element_part_t* result = *widget->part_end;
  result->type = type;
  widget->part_end = &(*widget->part_end)->next;

  return result;
}

guint32 quizgrind_widget_element_init(
    struct quizgrind_scope_t_* scope, void* data)
{
  quizgrind_widget_element_t* widget = (quizgrind_widget_element_t*) scope->current_widget;

  widget->part_end = &widget->part;

  return 0;
}

guint32 quizgrind_widget_element_cleanup(
    struct quizgrind_scope_t_* scope, void* data)
{
  quizgrind_widget_element_t* widget = (quizgrind_widget_element_t*) scope->current_widget;
  while(widget->part)
  {
    quizgrind_widget_element_part_t* next = widget->part->next;
    switch(widget->part->type)
    {
    case QUIZGRIND_ELEMENT_TYPE_TEXT:
    {
      free(widget->part->text.text);
      break;
    }
    case QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT:
    {
      quizgrind_widget_element_part_input_text_answer_t* answer = widget->part->input_text.answers;
      while(answer)
      {
        quizgrind_widget_element_part_input_text_answer_t* next = answer->next;
        free(answer->value);
        free(answer);
        answer = next;
      }
      break;
    }
    case QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER:
    {
      quizgrind_widget_element_part_input_number_answer_t* answer = widget->part->input_number.answers;
      while(answer)
      {
        quizgrind_widget_element_part_input_number_answer_t* next = answer->next;
        free(answer);
        answer = next;
      }
      break;
    }
    }

    free(widget->part);
    widget->part = next;
  }

  return 0;
}

guint32 quizgrind_widget_element_from_json(
    struct quizgrind_scope_t_* scope, void* options_)
{
  WJElement options = (WJElement) options_;
  quizgrind_widget_element_t* widget = (quizgrind_widget_element_t*) scope->current_widget;
  WJElement element = NULL;
  while((element = _WJEObject(options, KEY_ELEMENT_ELEMENTS "[]", WJE_GET, &element)))
  {
    char* type = WJEString(element, KEY_ELEMENT_TYPE, WJE_GET, NULL);
    if(!strcmp(type, QUIZGRIND_ELEMENT_TYPE_TEXT_))
    {
      quizgrind_widget_element_part_t* part = quizgrind_widget_element_add_part(widget, QUIZGRIND_ELEMENT_TYPE_TEXT);
      part->text.text = g_strdup(WJEString(element, KEY_ELEMENT_PART_TEXT_TEXT, WJE_GET, NULL));
    }
    else if(!strcmp(type, QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER_))
    {
      quizgrind_widget_element_part_t* part = quizgrind_widget_element_add_part(widget, QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER);
      part->input_number.mindp = WJEUInt32(element, KEYLIST(KEY_OPTION) "." KEY_ELEMENT_PART_INPUT_NUMBER_MINDP, WJE_GET, 0);
      part->input_number.mindp_penalty = WJEUInt32(element, KEYLIST(KEY_OPTION) "." KEY_ELEMENT_PART_INPUT_NUMBER_MINDP_PENALTY, WJE_GET, 0);
      WJElement last = NULL;
      char* text;
      while((text = _WJEString(element, KEYLIST(KEY_OPTION) "." KEY_ELEMENT_PART_INPUT_NUMBER_FORMAT "[]", WJE_GET, &last, NULL)))
        { part->input_number.format |= quizgrind_enum_match_str(&number_num_type, text); }
      part->input_number.format_display = quizgrind_enum_match_str(&number_num_type,
          WJEString(element, KEYLIST(KEY_OPTION) "." KEY_ELEMENT_PART_INPUT_NUMBER_FORMAT_DISPLAY, WJE_GET, NULL));
      while((last = _WJEObject(element, KEY_ELEMENT_PART_INPUT_NUMBER_ANSWERS "[]", WJE_GET, &last)))
      {
        quizgrind_widget_element_part_input_number_answer_t* answer = calloc(1, sizeof(quizgrind_widget_element_part_input_number_answer_t));

        answer->error = WJEDouble(last, KEY_ELEMENT_PART_INPUT_NUMBER_ANSWER_ERROR, WJE_GET, 0);
        answer->marks = WJEUInt32(last, KEY_ELEMENT_PART_INPUT_NUMBER_ANSWER_MARKS, WJE_GET, 0);
        answer->value = WJEDouble(last, KEY_ELEMENT_PART_INPUT_NUMBER_ANSWER_VALUE, WJE_GET, 0);

        answer->next = part->input_number.answers;
        part->input_number.answers = answer;
      }
    }
    else if(!strcmp(type, QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT_))
    {
      quizgrind_widget_element_part_t* part = quizgrind_widget_element_add_part(widget, QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT);
      part->input_text.caps = WJEBool(element, KEYLIST(KEY_OPTION) "." KEY_ELEMENT_PART_INPUT_TEXT_CAPS, WJE_GET, 0);
      WJElement last = NULL;
      while((last = _WJEObject(element, KEY_ELEMENT_PART_INPUT_TEXT_ANSWERS "[]", WJE_GET, &last)))
      {
        quizgrind_widget_element_part_input_text_answer_t* answer = calloc(1, sizeof(quizgrind_widget_element_part_input_text_answer_t));

        answer->marks = WJEUInt32(last, KEY_ELEMENT_PART_INPUT_TEXT_ANSWER_MARKS, WJE_GET, 0);
        answer->value = g_strdup(WJEString(last, KEY_ELEMENT_PART_INPUT_TEXT_ANSWER_VALUE, WJE_GET, NULL));

        answer->next = part->input_text.answers;
        part->input_text.answers = answer;
      }
    }
  }
  return true;
}

/** Adds a new numerical answer to an input_number part
 *  \param number The correct number.
 *  \param error  The acceptable error
 *  \param marks  The marks to award for a correct answer.
 */
static int quizgrind_widget_element_input_number_add_answer(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 4);
  quizgrind_script_assert_argtype(L, 2, LUA_TNUMBER);
  quizgrind_script_assert_argtype(L, 3, LUA_TNUMBER);
  quizgrind_script_assert_argtype(L, 4, LUA_TNUMBER);
  quizgrind_widget_element_part_input_number_t* part =
      *(quizgrind_widget_element_part_input_number_t**)lua_topointer(L, 1);

  quizgrind_widget_element_part_input_number_answer_t* answer = calloc(1, sizeof(quizgrind_widget_element_part_input_number_answer_t));

  answer->value = lua_tonumber(L, 2);
  answer->error = lua_tonumber(L, 3);
  answer->marks = lua_tonumber(L, 4);

  answer->next = part->answers;
  part->answers = answer;

  lua_pop(L, 3);
  return 1;
}

/** Allows an input format for an input_number part
 *  \param format  The name of the format to add (exponential, engineering, normalized, si, basic)
 */
static int quizgrind_widget_element_input_number_formats_add(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 2);
  quizgrind_script_assert_argtype(L, 2, LUA_TNUMBER);
  quizgrind_widget_element_part_input_number_t* part =
      *(quizgrind_widget_element_part_input_number_t**)lua_topointer(L, 1);

  part->format |= (guint32)lua_tonumber(L, 2);

  lua_pop(L, 1);
  return 1;
}

/** Clears all input formats for an input_number part
 */
static int quizgrind_widget_element_input_number_formats_clear(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 1);
  quizgrind_widget_element_part_input_number_t* part =
      *(quizgrind_widget_element_part_input_number_t**)lua_topointer(L, 1);

  part->format = 0;

  return 1;
}

/** Sets the format to display the answer as for an input_number part
 *  \param format  The format to use.
 */
static int quizgrind_widget_element_input_number_formats_set_display(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 2);
  quizgrind_script_assert_argtype(L, 2, LUA_TNUMBER);
  quizgrind_widget_element_part_input_number_t* part =
      *(quizgrind_widget_element_part_input_number_t**)lua_topointer(L, 1);

  part->format_display = lua_tonumber(L, 2);

  lua_pop(L, 1);
  return 1;
}

/** Adds a new answer for an input_text part
 *  \param value The correct answer.
 *  \param marks  The marks to award for a correct answer.
 */
static int quizgrind_widget_element_input_text_add_answer(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 3);
  quizgrind_script_assert_argtype(L, 2, LUA_TSTRING);
  quizgrind_script_assert_argtype(L, 3, LUA_TNUMBER);
  quizgrind_widget_element_part_input_text_t* part =
      *(quizgrind_widget_element_part_input_text_t**)lua_topointer(L, 1);

  quizgrind_widget_element_part_input_text_answer_t* answer = calloc(1, sizeof(quizgrind_widget_element_part_input_text_answer_t));

  answer->value = g_strdup(lua_tostring(L, 2));
  answer->marks = lua_tonumber(L, 3);

  answer->next = part->answers;
  part->answers = answer;

  lua_pop(L, 2);
  return 1;
}

/** Add a new text part
 *  \param text  The text to show
 */
static int quizgrind_widget_element_add_text(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 2);
  quizgrind_script_assert_argtype(L, 2, LUA_TSTRING);
  quizgrind_widget_element_t* widget = *(quizgrind_widget_element_t**)lua_topointer(L, 1);

  quizgrind_widget_element_part_text_t** part = lua_newuserdata(L, sizeof(quizgrind_widget_element_part_text_t*));
  quizgrind_widget_element_part_t* new = quizgrind_widget_element_add_part(widget, QUIZGRIND_ELEMENT_TYPE_TEXT);

  new->text.text = g_strdup(lua_tostring(L, 2));
  *part = (&new->text);

  return 1;
}

static int quizgrind_widget_element_input_text__index(lua_State* L)
{
  quizgrind_widget_element_part_input_text_t* widget = 
    *(quizgrind_widget_element_part_input_text_t**)lua_topointer(L, 1);
  const char* name = lua_tostring(L, 2);
  if(!strcmp(name, FUN_ELEMENT_INPUT_TEXT_ADD_ANSWER))
    { lua_pushcfunction(L, quizgrind_widget_element_input_text_add_answer); }
  else if(!strcmp(name, KEY_ELEMENT_PART_INPUT_TEXT_CAPS))
    { lua_pushboolean(L, widget->caps); }
  else return 0;

  return 1;
}

static int quizgrind_widget_element_input_text__newindex(lua_State* L)
{
  quizgrind_widget_element_part_input_text_t* widget = 
    *(quizgrind_widget_element_part_input_text_t**)lua_topointer(L, 1);
  const char* name = lua_tostring(L, 2);

  if(!strcmp(name, KEY_ELEMENT_PART_INPUT_TEXT_CAPS))
    { widget->caps = lua_toboolean(L, 3); }
  else return 0;

  return 0;
}

/** Add a new input_text widget
 *  \param text  The text to show
 *  \param regex Whether to use REGEX. (Optional, default false)
 */
static int quizgrind_widget_element_add_input_text(lua_State* L)
{
  quizgrind_widget_element_t* widget = *(quizgrind_widget_element_t**)lua_topointer(L, 1);

  quizgrind_widget_element_part_input_text_t** part = lua_newuserdata(L, sizeof(quizgrind_widget_element_part_input_text_t*));
  quizgrind_widget_element_part_t* new = quizgrind_widget_element_add_part(widget, QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT);
  *part = &new->input_text;

  lua_createtable(L, 1, 0);

  lua_pushstring(L, "__index");
  lua_pushcfunction(L, quizgrind_widget_element_input_text__index);
  lua_settable(L, -3);

  lua_pushstring(L, "__newindex");
  lua_pushcfunction(L, quizgrind_widget_element_input_text__newindex);
  lua_settable(L, -3);

  lua_setmetatable(L, -2);

  return 1;
}

static int quizgrind_widget_element_input_number__index(lua_State* L)
{
  quizgrind_widget_element_part_input_number_t* widget = 
    *(quizgrind_widget_element_part_input_number_t**)lua_topointer(L, 1);
  const char* name = lua_tostring(L, 2);

  if(!strcmp(name, FUN_ELEMENT_INPUT_NUMBER_ADD_ANSWER))
    { lua_pushcfunction(L, quizgrind_widget_element_input_number_add_answer); }
  else if(!strcmp(name, KEY_ELEMENT_PART_INPUT_NUMBER_MINDP))
    { lua_pushnumber(L, widget->mindp); }
  else if(!strcmp(name, KEY_ELEMENT_PART_INPUT_NUMBER_MINDP_PENALTY))
    { lua_pushnumber(L, widget->mindp_penalty); }
  else if(!strcmp(name, FUN_ELEMENT_INPUT_NUMBER_FORMATS_ADD))
    { lua_pushcfunction(L, quizgrind_widget_element_input_number_formats_add); }
  else if(!strcmp(name, FUN_ELEMENT_INPUT_NUMBER_FORMATS_CLEAR))
    { lua_pushcfunction(L, quizgrind_widget_element_input_number_formats_clear); }
  else if(!strcmp(name, FUN_ELEMENT_INPUT_NUMBER_FORMATS_SET_DISPLAY))
    { lua_pushcfunction(L, quizgrind_widget_element_input_number_formats_set_display); }
  else return 0;

  return 1;
}

static int quizgrind_widget_element_input_number__newindex(lua_State* L)
{
  quizgrind_widget_element_part_input_number_t* widget = 
    *(quizgrind_widget_element_part_input_number_t**)lua_topointer(L, 1);
  const char* name = lua_tostring(L, 2);

  if(!strcmp(name, KEY_ELEMENT_PART_INPUT_NUMBER_MINDP))
    { widget->mindp = lua_tonumber(L, 3); }
  else if(!strcmp(name, KEY_ELEMENT_PART_INPUT_NUMBER_MINDP_PENALTY))
    { widget->mindp_penalty = lua_tonumber(L, 3); }

  return 0;
}

/** Add a new input_number widget
 *  \param text  The text to show
 */
static int quizgrind_widget_element_add_input_number(lua_State* L)
{
  quizgrind_widget_element_t* widget = *(quizgrind_widget_element_t**)lua_topointer(L, 1);

  quizgrind_widget_element_part_input_number_t** part = lua_newuserdata(L, sizeof(quizgrind_widget_element_part_input_number_t*));
  quizgrind_widget_element_part_t* new = quizgrind_widget_element_add_part(widget, QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER);
  *part = &new->input_number;

  lua_createtable(L, 1, 0);

  lua_pushstring(L, "__index");
  lua_pushcfunction(L, quizgrind_widget_element_input_number__index);
  lua_settable(L, -3);

  lua_pushstring(L, "__newindex");
  lua_pushcfunction(L, quizgrind_widget_element_input_number__newindex);
  lua_settable(L, -3);

  lua_setmetatable(L, -2);

  return 1;
}

guint32 quizgrind_widget_element_setup_script(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_setup_script_t* data = (quizgrind_widget_setup_script_t*) data_;

  lua_pushstring(data->L, "__index");
  lua_newtable(data->L);

  lua_pushstring(data->L, FUN_ELEMENT_ADD_TEXT);
  lua_pushcfunction(data->L, quizgrind_widget_element_add_text);
  lua_settable(data->L, -3);

  lua_pushstring(data->L, FUN_ELEMENT_ADD_INPUT_TEXT);
  lua_pushcfunction(data->L, quizgrind_widget_element_add_input_text);
  lua_settable(data->L, -3);

  lua_pushstring(data->L, FUN_ELEMENT_ADD_INPUT_NUMBER);
  lua_pushcfunction(data->L, quizgrind_widget_element_add_input_number);
  lua_settable(data->L, -3);

  lua_settable(data->L, data->idx);

  return 0;
}

guint32 quizgrind_widget_element_answer_max(
    struct quizgrind_scope_t_* scope, void* data)
{
  quizgrind_widget_element_t* widget = (quizgrind_widget_element_t*) scope->current_widget;
  uint32_t marks_total = 0;

  quizgrind_widget_element_part_t* part = widget->part;
  while(part)
  {
    uint32_t marks_this = 0;
    switch(part->type)
    {
    case QUIZGRIND_ELEMENT_TYPE_TEXT:
      { break; }
    case QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT:
    {
      quizgrind_widget_element_part_input_text_answer_t* answer = part->input_text.answers;
      while(answer)
      {
        if(answer->marks > marks_this)
          { marks_this = answer->marks; }
        answer = answer->next;
      }
      break;
    }
    case QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER:
    {
      quizgrind_widget_element_part_input_number_answer_t* answer = part->input_number.answers;
      while(answer)
      {
        if(answer->marks > marks_this)
          { marks_this = answer->marks; }
        answer = answer->next;
      }
      break;
    }
    }

    marks_total += marks_this;
    part = part->next;
  }
  return marks_total;
}

guint32 quizgrind_widget_element_answer_check(
    struct quizgrind_scope_t_* scope, void* uans_)
{
  char* uans = (char*) uans_;

  quizgrind_widget_element_t* widget = (quizgrind_widget_element_t*) scope->current_widget;
  gint32 marks_total = 0;
  quizgrind_widget_element_part_t* part = widget->part;
  while(part)
  {
    gint32 marks_this = 0;
    switch(part->type)
    {
    case QUIZGRIND_ELEMENT_TYPE_TEXT:
      { break; }
    case QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT:
    {
      quizgrind_widget_element_part_input_text_answer_t* answer = part->input_text.answers;
      while(answer)
      {
        char* uans_walk = uans;
        char* ans = answer->value;

        if(!part->input_text.caps)
        {
          while (tolower(*uans_walk) && tolower(*ans) && (tolower(*ans) == tolower(*uans_walk)))
            { ans++; uans_walk++; }
        }
        else
        {
          while (*uans_walk && *ans && (*ans == *uans_walk))
            { ans++; uans_walk++; }
        }

        if(*ans == 0 && (*uans_walk == ';' || *uans_walk == 0))
        { 
          if(answer->marks > marks_this) 
            { marks_this = answer->marks; }
        }

        answer = answer->next;
      }

      while(*uans != ';' && *uans != 0)
        { uans ++; }

      while(*uans == ';')
          { uans++; }
      break;
    }
    case QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER:
    {
      DOUBLERESULT value;

      DOUBLETYPE type = number_parse(&uans, &value);
      if(part->input_number.format && !(type & part->input_number.format)) { break; }

      quizgrind_widget_element_part_input_number_answer_t* answer = part->input_number.answers;
      while(answer)
      {
        if(value.value >= (answer->value - answer->error) && value.value <= (answer->value + answer->error))
        {
          if(answer->marks > marks_this)
          { 
            marks_this = answer->marks;
            if((part->input_number.mindp > 0 && value.dp < ((guint32)part->input_number.mindp))
            || (part->input_number.mindp < 0 && value.sigfig < ((guint32)(-part->input_number.mindp))))
              { marks_this -= part->input_number.mindp_penalty; }
          }
        }

        answer = answer->next;
      }

      while(*uans != ';' && *uans != 0)
        { uans ++; }

      while(*uans == ';')
          { uans++; }
    }
    }

    if(marks_this < 0)
      { marks_this = 0; }
    marks_total += marks_this;

    part = part->next;

    if(*uans == 0) return marks_total;
  }
  return marks_total;
}

guint32 quizgrind_widget_element_to_json(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_to_json_t* data = (quizgrind_widget_to_json_t*) data_;

  quizgrind_widget_element_t* widget = (quizgrind_widget_element_t*) scope->current_widget;
  quizgrind_widget_element_part_t* part = widget->part;

  WJWOpenArray(KEY_ELEMENT_ELEMENTS, data->writer);

  while(part)
  {
    WJWOpenObject(NULL, data->writer);
    switch(part->type)
    {
    case QUIZGRIND_ELEMENT_TYPE_TEXT:
    {
      WJWString(KEY_ELEMENT_TYPE, QUIZGRIND_ELEMENT_TYPE_TEXT_, true, data->writer);
      WJWString(KEY_ELEMENT_PART_TEXT_TEXT, part->text.text, true, data->writer);
      break;
    }
    case QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT:
    {
      WJWString(KEY_ELEMENT_TYPE, QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT_, true, data->writer);
      if(data->flags & QUIZGRIND_WIDGET_JSON_QUESTION)
      {
        WJWOpenObject(KEYLIST(KEY_OPTION), data->writer);
        WJWBoolean(KEY_ELEMENT_PART_INPUT_TEXT_CAPS, part->input_text.caps, data->writer);
        WJWCloseObject(data->writer);
      }
      if(data->flags & QUIZGRIND_WIDGET_JSON_SOLUTION)
      {
        quizgrind_widget_element_part_input_text_answer_t* answer = part->input_text.answers;
        WJWOpenArray(KEY_ELEMENT_PART_INPUT_TEXT_ANSWERS, data->writer);
        while(answer)
        {
          WJWOpenObject(NULL, data->writer);
          WJWUInt32(KEY_ELEMENT_PART_INPUT_TEXT_ANSWER_MARKS, answer->marks, data->writer);
          WJWString(KEY_ELEMENT_PART_INPUT_TEXT_ANSWER_VALUE, answer->value, true, data->writer);
          WJWCloseObject(data->writer);
          answer = answer->next;
        }
        WJWCloseArray(data->writer);
      }
      break;
    }
    case QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER:
    {
      WJWString(KEY_ELEMENT_TYPE, QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER_, true, data->writer);
      if(data->flags & QUIZGRIND_WIDGET_JSON_QUESTION)
      {
        WJWOpenObject(KEYLIST(KEY_OPTION), data->writer);
        WJWString(KEY_ELEMENT_PART_INPUT_NUMBER_FORMAT, quizgrind_enum_match_num(&number_num_type, part->input_number.format), true, data->writer);
        WJWString(KEY_ELEMENT_PART_INPUT_NUMBER_FORMAT_DISPLAY, quizgrind_enum_match_num(&number_num_type, part->input_number.format_display), true, data->writer);
        WJWInt32(KEY_ELEMENT_PART_INPUT_NUMBER_MINDP, part->input_number.mindp, data->writer);
        WJWUInt32(KEY_ELEMENT_PART_INPUT_NUMBER_MINDP_PENALTY, part->input_number.mindp_penalty, data->writer);
        WJWCloseObject(data->writer);
      }
      if(data->flags & QUIZGRIND_WIDGET_JSON_SOLUTION)
      {
        quizgrind_widget_element_part_input_number_answer_t* answer = part->input_number.answers;
        WJWOpenArray(KEY_ELEMENT_PART_INPUT_NUMBER_ANSWERS, data->writer);
        while(answer)
        {
          WJWOpenObject(NULL, data->writer);
          WJWUInt32(KEY_ELEMENT_PART_INPUT_NUMBER_ANSWER_MARKS, answer->marks, data->writer);
          WJWDouble(KEY_ELEMENT_PART_INPUT_NUMBER_ANSWER_ERROR, answer->error, data->writer);
          WJWDouble(KEY_ELEMENT_PART_INPUT_NUMBER_ANSWER_VALUE, answer->value, data->writer);
          WJWCloseObject(data->writer);
          answer = answer->next;
        }
        WJWCloseArray(data->writer);
      }
      break;
    }
    }

    WJWCloseObject(data->writer);
    part = part->next;
  }

  WJWCloseArray(data->writer);
  return false;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
