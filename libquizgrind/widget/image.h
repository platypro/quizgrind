/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INC_IMAGEVIEW_H
#define INC_IMAGEVIEW_H

#include "core/widget.h"
#include "core/exercise.h"
#include "core/template.h"

#define KEY_IMAGEVIEW_SVG "svg"
#define KEY_IMAGEVIEW_SRC "src"
#define KEY_IMAGEVIEW_SVG_TEMPLATE "svg_template"

#define FUN_IMAGEVIEW_SET_IMAGE_SVG "set_image_svg"
#define FUN_IMAGEVIEW_SET_IMAGE "set_image"

typedef enum quizgrind_widget_image_format_t_
{
  QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG,
  QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG_TEMPLATE,
  QUIZGRIND_WIDGET_IMAGE_FORMAT_RASTER

} quizgrind_widget_image_format_t;

typedef struct quizgrind_widget_image_t_
{
  quizgrind_widget_t base;

  quizgrind_widget_image_format_t format;
  union {
    char* svg;
    quizgrind_template_instance_t* svg_template;
    char* raster;
  };

} quizgrind_widget_image_t;

extern guint32 quizgrind_widget_image_init(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_image_cleanup(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_image_from_json(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_image_setup_script(
    struct quizgrind_scope_t_*, void*);

#define quizgrind_widget_image_answer_max    quizgrind_widget_default_answer_max

#define quizgrind_widget_image_answer_check  quizgrind_widget_default_answer_check

extern guint32 quizgrind_widget_image_to_json(
    struct quizgrind_scope_t_*, void*);

#endif /* INC_IMAGEVIEW_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
