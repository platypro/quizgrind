/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "lua.h"
#include "quizgrind.h"
#include "util/unirand.h"
#include "widget/choice.h"
#include "wjelement.h"

#include <string.h>
#include <pmus-input-stream.h>

guint32 quizgrind_widget_choice_init(
  struct quizgrind_scope_t_* scope, void* data) 
{
  quizgrind_widget_choice_t* widget = (quizgrind_widget_choice_t*) scope->current_widget;

  widget->choices = g_queue_new();
  widget->numSelections = 1;

  return 0;
}

void quizgrind_widget_choice_item_cleanup (gpointer data)
{
  quizgrind_widget_choice_item_t* item = (quizgrind_widget_choice_item_t*) data;
  if(item->text)
    { free(item->text); }

  free(item);
}

guint32 quizgrind_widget_choice_cleanup(
    struct quizgrind_scope_t_* scope, void* data)
{
  quizgrind_widget_choice_t* widget = (quizgrind_widget_choice_t*) scope->current_widget;
  g_queue_clear_full(widget->choices, quizgrind_widget_choice_item_cleanup);

  return 0;
}

guint32 quizgrind_widget_choice_from_json(
    struct quizgrind_scope_t_* scope, void* element_)
{
  quizgrind_widget_choice_t* widget = (quizgrind_widget_choice_t*) scope->current_widget;
  WJElement element = (WJElement) element_;

  widget->fullMarks = WJEUInt32(element, KEY_CHOICEINPUT_MARKS, WJE_GET, 0);
  widget->numSelections = WJEUInt32(element, KEY_CHOICEINPUT_NUMSELECTIONS, WJE_GET, 1);
  widget->randomize = WJEBool(element, KEY_CHOICEINPUT_RANDOMIZE, WJE_GET, FALSE);

  WJElement itemsarray = WJEArray(element, KEY_CHOICEINPUT_CHOICES, WJE_GET)->child;
  while(itemsarray)
  {
    quizgrind_widget_choice_item_t* item = calloc(1, sizeof(quizgrind_widget_choice_item_t));
    item->marks = WJEInt32(itemsarray, KEY_CHOICEINPUT_CHOICES_MARKS, WJE_GET, 0);
    item->id = g_random_int();

    char* type = WJEString(itemsarray, KEY_CHOICEINPUT_CHOICES_TYPE, WJE_GET, NULL);
    if(!type)
    {
      printf("Choice element is missing an item type!\n");
      free(item);
      return false;
    }

    char* text = WJEString(itemsarray, KEY_CHOICEINPUT_CHOICES_TEXT, WJE_GET, NULL);
    WJElement template = WJEObject(itemsarray, KEY_CHOICEINPUT_CHOICES_TEMPLATE, WJE_GET);

    if(text)
    {
      item->format = QUIZGRIND_WIDGET_CHOICE_FORMAT_NORMAL;
      item->text = g_strdup(text);
    }
    else if(template)
    {
      item->format = QUIZGRIND_WIDGET_CHOICE_FORMAT_TEMPLATE;
      item->template = quizgrind_template_instance_wje(scope, template);
    }
    else
    {
      printf("Choice element is missing either a text or template element!\n");
      free(item);
      return false;
    }

    if(!strcmp(type, QUIZGRIND_WIDGET_CHOICE_TYPE_SVG_))
      { item->type = QUIZGRIND_WIDGET_CHOICE_TYPE_SVG; }
    else if(!strcmp(type, QUIZGRIND_WIDGET_CHOICE_TYPE_TEXT_))
      { item->type = QUIZGRIND_WIDGET_CHOICE_TYPE_TEXT; }
    else
    {
      printf("Invalid choice element type %s\n", type);
      free(item);
      return false;
    }

    g_queue_push_tail(widget->choices, item);

    itemsarray = itemsarray->next;
  }

  return true;
}

void quizgrind_widget_choice_item_answer_max(gpointer item_, gpointer totalMarks_)
{
  quizgrind_widget_choice_item_t* item = (quizgrind_widget_choice_item_t*) item_;
  guint32* totalMarks = (guint32*) totalMarks_;
  if(item->marks > 0)
    { *totalMarks += item->marks; }
}

guint32 quizgrind_widget_choice_answer_max(
    struct quizgrind_scope_t_* scope, void* data)
{
  quizgrind_widget_choice_t* widget = (quizgrind_widget_choice_t*) scope->current_widget;
  if(widget->fullMarks)
    return widget->fullMarks;
  else
  {
    guint32 totalMarks = 0;
    g_queue_foreach(widget->choices, quizgrind_widget_choice_item_answer_max, &totalMarks);
    return totalMarks;
  } 
}

gboolean uans_find(char* uans, guint32 find)
{
  char* uans_next = uans;
  while(*uans_next)
  {
    guint32 ans = strtoul(uans, &uans_next, 10);

    if(ans == find)
      { return TRUE; }

    if(*uans_next)
      { uans = uans_next + 1; }
  }

  return FALSE;
}

guint32 uans_count(char* uans)
{
  guint32 result = 0;
  char* uans_next = uans;
  while(*uans_next)
  {
    strtoul(uans, &uans_next, 10);

    result ++;

    if(*uans_next)
      { uans = uans_next + 1; }
  }

  return result;
}

guint32 quizgrind_widget_choice_answer_check(
    struct quizgrind_scope_t_* scope, void* uans_)
{
  char* uans = (char*) uans_;
  quizgrind_widget_choice_t* widget = (quizgrind_widget_choice_t*) scope->current_widget;
  guint32 marks = 0;
  guint32 totalMarks = 0;

  // Fail question if more than maximum selections given
  if(uans_count(uans) > widget->numSelections)
    { return 0; }

  for(GList* baseelem_ = widget->choices->head; baseelem_; baseelem_ = baseelem_->next)
  {
    quizgrind_widget_choice_item_t* baseelem = 
      (quizgrind_widget_choice_item_t*) baseelem_->data;

    if(uans_find(uans, baseelem->id))
    { 
      if(baseelem->marks < 0 && marks <= -baseelem->marks)
        { marks = 0; }
      else
        { marks += baseelem->marks; }
    }

    if(baseelem->marks > 0)
      { totalMarks += baseelem->marks; }
  }
  
  if(widget->fullMarks)
  {
    if(totalMarks == marks)
      return widget->fullMarks;
    else return 0;
  }
  else return marks;
}

static gboolean quizgrind_widget_choice_update_item_from_lua(lua_State* L, int idx, quizgrind_widget_choice_item_t* item)
{
  switch(lua_type(L, idx))
  {
  case LUA_TLIGHTUSERDATA:
    item->format = QUIZGRIND_WIDGET_CHOICE_FORMAT_TEMPLATE;
    item->template = (quizgrind_template_instance_t*)lua_topointer(L, idx);
    return true;
  case LUA_TSTRING:
    item->format = QUIZGRIND_WIDGET_CHOICE_FORMAT_NORMAL;
    item->text = g_strdup(lua_tostring(L, idx));
    return true;
  default:
    luaL_error(L, "Invalid type for choice element source text.");
    return false;
  }
}

/** Add a new choice to the choiceInput
 *  \param text  The text to show.
 *  \param marks The number of marks this choice awards.
 */
static int quizgrind_widget_choice_fun_add_item(lua_State* L)
{
  quizgrind_widget_choice_t* widget = *(quizgrind_widget_choice_t**)lua_topointer(L, 1);

  quizgrind_widget_choice_item_t* item = calloc(1, sizeof(quizgrind_widget_choice_item_t));
  item->type = QUIZGRIND_WIDGET_CHOICE_TYPE_TEXT;
  item->id = g_random_int();
  item->marks = lua_tonumber(L, 3);

  if(quizgrind_widget_choice_update_item_from_lua(L, 2, item))
  {
    g_queue_push_tail(widget->choices, item);
  }
  else
    { free(item); }

  // Pop all arguments except self, then return self.
  lua_pop(L, 2);
  return 1;
}

/** Add a new SVG choice to the choiceInput
 *  \param src  The svg to show
 *  \param marks The number of marks this choice awards.
 */
static int quizgrind_widget_choice_fun_add_item_svg(lua_State* L)
{
  quizgrind_widget_choice_t* widget = *(quizgrind_widget_choice_t**)lua_topointer(L, 1);

  quizgrind_widget_choice_item_t* item = calloc(1, sizeof(quizgrind_widget_choice_item_t));
  item->type = QUIZGRIND_WIDGET_CHOICE_TYPE_SVG;
  item->id = g_random_int();
  item->marks = lua_tonumber(L, 3);

  if(quizgrind_widget_choice_update_item_from_lua(L, 2, item))
    { g_queue_push_tail(widget->choices, item); }
  else
    { free(item); }

  // Pop all arguments except self, then return self.
  lua_pop(L, 2);
  return 1;
}

static int quizgrind_widget_choice__index(lua_State* L)
{
  quizgrind_widget_choice_t* widget = *(quizgrind_widget_choice_t**)lua_topointer(L, 1);
  const char* name = lua_tostring(L, 2);

  if(!strcmp(name, KEY_CHOICEINPUT_NUMSELECTIONS))
    { lua_pushnumber(L, widget->numSelections); }
  else if(!strcmp(name, KEY_CHOICEINPUT_MARKS))
    { lua_pushnumber(L, widget->fullMarks); }
  else if(!strcmp(name, KEY_CHOICEINPUT_RANDOMIZE))
    { lua_pushboolean(L, widget->randomize); }
  else if(!strcmp(name, FUN_CHOICEINPUT_ADD_CHOICE))
    { lua_pushcfunction(L, quizgrind_widget_choice_fun_add_item); }
  else if(!strcmp(name, FUN_CHOICEINPUT_ADD_CHOICE_SVG))
    { lua_pushcfunction(L, quizgrind_widget_choice_fun_add_item_svg); }

  return 1;
}

static int quizgrind_widget_choice__newindex(lua_State* L)
{
  quizgrind_widget_choice_t* widget = *(quizgrind_widget_choice_t**)lua_topointer(L, 1);
  const char* name = lua_tostring(L, 2);

  if(!strcmp(name, KEY_CHOICEINPUT_NUMSELECTIONS))
    { widget->numSelections = lua_tonumber(L, 3); }
  else if(!strcmp(name, KEY_CHOICEINPUT_MARKS))
    { widget->fullMarks = lua_tonumber(L, 3); }
  else if(!strcmp(name, KEY_CHOICEINPUT_RANDOMIZE))
    { widget->randomize = lua_toboolean(L, 3); }

  return 0;
}

guint32 quizgrind_widget_choice_setup_script(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_setup_script_t* data = (quizgrind_widget_setup_script_t*) data_;

  lua_pushstring(data->L, "__index");
  lua_pushcfunction(data->L, quizgrind_widget_choice__index);
  lua_settable(data->L, data->idx);

  lua_pushstring(data->L, "__newindex");
  lua_pushcfunction(data->L, quizgrind_widget_choice__newindex);
  lua_settable(data->L, data->idx);

  return 0;
}

guint32 quizgrind_widget_choice_to_json(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_to_json_t* data = (quizgrind_widget_to_json_t*) data_;
  quizgrind_widget_choice_t* widget = (quizgrind_widget_choice_t*) scope->current_widget;

  guint32 numSelections_new = widget->numSelections ? widget->numSelections : 1;
  WJWUInt32(KEY_CHOICEINPUT_NUMSELECTIONS, numSelections_new, data->writer);
  WJWOpenArray(KEY_CHOICEINPUT_CHOICES, data->writer);

  struct unirand_t rand = {0};
  if(widget->randomize)
    { unirand_seed(&rand, widget->choices->length); }
  else
    { unirand_linear(&rand, widget->choices->length); }

  guint32 current_item = 0;
  while(current_item < widget->choices->length)
  {
    quizgrind_widget_choice_item_t* item = g_queue_peek_nth(widget->choices, unirand(&rand, &current_item));

    WJWOpenObject(NULL, data->writer);

    WJWUInt32(KEY_CHOICEINPUT_CHOICES_ID, item->id, data->writer);

    switch(item->type)
    {
    case QUIZGRIND_WIDGET_CHOICE_TYPE_TEXT:
      WJWString(KEY_CHOICEINPUT_CHOICES_TYPE, QUIZGRIND_WIDGET_CHOICE_TYPE_TEXT_, true, data->writer);
      break;
    case QUIZGRIND_WIDGET_CHOICE_TYPE_SVG:
      WJWString(KEY_CHOICEINPUT_CHOICES_TYPE, QUIZGRIND_WIDGET_CHOICE_TYPE_SVG_, true, data->writer);
      break;
    }

    switch(item->format)
    {
    case QUIZGRIND_WIDGET_CHOICE_FORMAT_TEMPLATE:
    {
      WJWString(KEY_CHOICEINPUT_CHOICES_TEXT, "", false, data->writer);
      mustache_generate(item->template, quizgrind_widget_writeWJW, data->writer);
      WJWString(NULL, "", true, data->writer);
      break;
    }
    case QUIZGRIND_WIDGET_CHOICE_FORMAT_NORMAL:
    {
      WJWString(KEY_CHOICEINPUT_CHOICES_TEXT, item->text, true, data->writer);
      break;
    }
    }

    if(data->flags & QUIZGRIND_WIDGET_JSON_SOLUTION)
      { WJWInt32(KEY_CHOICEINPUT_CHOICES_MARKS, item->marks, data->writer); }

    WJWCloseObject(data->writer);
  }

  WJWCloseArray(data->writer);
  return true;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
