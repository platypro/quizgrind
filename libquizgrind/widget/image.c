/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "quizgrind.h"
#include "widget/image.h"

#include "core/template.h"

#include <pmus-input-stream.h>

guint32 quizgrind_widget_image_init(
    struct quizgrind_scope_t_* scope, void* data)
{
  return 0;
}

guint32 quizgrind_widget_image_cleanup(
    struct quizgrind_scope_t_* scope, void* data)
{
  quizgrind_widget_image_t* widget = (quizgrind_widget_image_t*) scope->current_widget;
  if(widget->svg)
    { g_free(widget->svg); }

  return 0;
}

guint32 quizgrind_widget_image_from_json(
    struct quizgrind_scope_t_* scope, void* element_)
{
  WJElement element = (WJElement) element_;
  quizgrind_widget_image_t* widget = (quizgrind_widget_image_t*) scope->current_widget;

  char* svg = WJEString(element, KEY_IMAGEVIEW_SVG, WJE_GET, NULL);
  char* src = WJEString(element, KEY_IMAGEVIEW_SRC, WJE_GET, NULL);
  WJElement svg_template = WJEObject(element, KEY_IMAGEVIEW_SVG_TEMPLATE, WJE_GET);
  if(src)
  {
    widget->format = QUIZGRIND_WIDGET_IMAGE_FORMAT_RASTER;
    widget->raster = g_strdup(src);
  }
  else if(svg)
  {
    widget->format = QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG;
    widget->svg = g_strdup(svg);
  }
  else if(svg_template)
  {
    widget->format = QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG_TEMPLATE;
    widget->svg_template = quizgrind_template_instance_wje(scope, svg_template);
  }
  else
  {
    printf("Image element is missing a source!\n");
    return false;
  }

  return true;
}

guint32 quizgrind_widget_image_to_json(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_to_json_t* data = (quizgrind_widget_to_json_t*) data_;
  quizgrind_widget_image_t* widget = (quizgrind_widget_image_t*) scope->current_widget;

  if(data->flags & QUIZGRIND_WIDGET_JSON_QUESTION)
  {
    switch(widget->format)
    {
    case QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG_TEMPLATE:
    {
      WJWString(KEY_IMAGEVIEW_SVG, "", false, data->writer);
      mustache_generate(widget->svg_template, quizgrind_widget_writeWJW, data->writer);
      WJWString(NULL, "", true, data->writer);
      break;
    }
    case QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG:
    {
      WJWString(KEY_IMAGEVIEW_SVG, widget->svg, true, data->writer);
      break;
    }
    case QUIZGRIND_WIDGET_IMAGE_FORMAT_RASTER:
    {
      path_t* path = NULL;
      if(data->flags & QUIZGRIND_WIDGET_JSON_FULLPATH)
        { path = path_clone(&scope->current_project->path_exercise); }
      else
        { path = path_new_from_str("", 0); }

      path_push(&path, scope->current_exercise->id);
      path_push(&path, widget->raster);
      path_make_absolute(&path);
      WJWString(KEY_IMAGEVIEW_SRC, path_get_cstr(&path), true, data->writer);
      path_destroy(&path);
      break;
    }
    }
  }

  return true;
}

/** Set the image of the imageView to a svg.
 *  \param image The source of the SVG image.
 */
static int quizgrind_widget_image_set_image_svg(lua_State* L)
{
  quizgrind_widget_image_t* widget = *(quizgrind_widget_image_t**)lua_topointer(L, 1);

  if(widget->svg)
    { g_free(widget->svg); }

  switch(lua_type(L, 2))
  {
  case LUA_TLIGHTUSERDATA:
    widget->format = QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG_TEMPLATE;
    widget->svg_template = (quizgrind_template_instance_t*)lua_topointer(L, 2);
    break;
  case LUA_TSTRING:
    widget->format = QUIZGRIND_WIDGET_IMAGE_FORMAT_SVG;
    widget->svg = g_strdup(lua_tostring(L, 2));
    break;
  default:
    luaL_error(L, "Invalid type for image element svg.");
    break;
  }

  lua_pop(L, 1);

  return 1;
}

/** Set the image of the imageView to a raster image.
 *  \param image The image path.
 */
static int quizgrind_widget_image_set_image(lua_State* L)
{
  quizgrind_widget_image_t* widget = *(quizgrind_widget_image_t**)lua_topointer(L, 1);

  if(widget->raster)
    { g_free(widget->raster); }

  switch(lua_type(L, 2))
  {
  case LUA_TSTRING:
    widget->format = QUIZGRIND_WIDGET_IMAGE_FORMAT_RASTER;
    widget->raster = g_strdup(lua_tostring(L, 2));
    break;
  default:
    luaL_error(L, "Image path must be a string!");
    break;
  }

  lua_pop(L, 1);

  return 1;
}

guint32 quizgrind_widget_image_setup_script(
    struct quizgrind_scope_t_* scope, void* data_)
{
  quizgrind_widget_setup_script_t* data = (quizgrind_widget_setup_script_t*) data_;

  lua_pushstring(data->L, "__index");
  lua_newtable(data->L);

  lua_pushstring(data->L, FUN_IMAGEVIEW_SET_IMAGE_SVG);
  lua_pushcfunction(data->L, quizgrind_widget_image_set_image_svg);
  lua_settable(data->L, -3);

  lua_pushstring(data->L, FUN_IMAGEVIEW_SET_IMAGE);
  lua_pushcfunction(data->L, quizgrind_widget_image_set_image);
  lua_settable(data->L, -3);

  lua_settable(data->L, data->idx);

  return 0;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
