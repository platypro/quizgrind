/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INC_ELEMENT_H
#define INC_ELEMENT_H

#include "../../../libquizgrind/core/widget.h"
#include "../../../libquizgrind/util/number.h"

#define KEY_ELEMENT_ELEMENTS "elements"

#define KEY_ELEMENT_PART_TEXT_TEXT "text"

#define KEY_ELEMENT_PART_INPUT_TEXT_ANSWER_VALUE "value"
#define KEY_ELEMENT_PART_INPUT_TEXT_ANSWER_MARKS "marks"
#define KEY_ELEMENT_PART_INPUT_TEXT_CAPS         "caps"
#define KEY_ELEMENT_PART_INPUT_TEXT_ANSWERS      "answers"

#define KEY_ELEMENT_PART_INPUT_NUMBER_ANSWER_VALUE   "value"
#define KEY_ELEMENT_PART_INPUT_NUMBER_ANSWER_ERROR   "error"
#define KEY_ELEMENT_PART_INPUT_NUMBER_ANSWER_MARKS   "marks"
#define KEY_ELEMENT_PART_INPUT_NUMBER_ANSWERS        "answers"
#define KEY_ELEMENT_PART_INPUT_NUMBER_MINDP          "mindp"
#define KEY_ELEMENT_PART_INPUT_NUMBER_MINDP_PENALTY  "mindp_penalty"
#define KEY_ELEMENT_PART_INPUT_NUMBER_FORMAT         "format"
#define KEY_ELEMENT_PART_INPUT_NUMBER_FORMAT_DISPLAY "format_display"

#define FUN_ELEMENT_ADD_TEXT "add_text"
#define FUN_ELEMENT_ADD_INPUT_TEXT "add_input_text"
#define FUN_ELEMENT_ADD_INPUT_NUMBER "add_input_number"

#define FUN_ELEMENT_INPUT_NUMBER_ADD_ANSWER    "add_answer"
#define FUN_ELEMENT_INPUT_NUMBER_FORMATS_ADD   "formats_add"
#define FUN_ELEMENT_INPUT_NUMBER_FORMATS_CLEAR "formats_clear"
#define FUN_ELEMENT_INPUT_NUMBER_FORMATS_SET_DISPLAY "formats_set_display"

#define FUN_ELEMENT_INPUT_TEXT_ADD_ANSWER "add_answer"

typedef enum quizgrind_widget_element_type_t_
{
  QUIZGRIND_ELEMENT_TYPE_TEXT,
  QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT,
  QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER

} quizgrind_widget_element_type_t;

#define KEY_ELEMENT_TYPE "type"
#define QUIZGRIND_ELEMENT_TYPE_TEXT_         "text"
#define QUIZGRIND_ELEMENT_TYPE_INPUT_TEXT_   "input_text"
#define QUIZGRIND_ELEMENT_TYPE_INPUT_NUMBER_ "input_number"

typedef struct quizgrind_widget_element_part_text_t_
{
  char* text;

} quizgrind_widget_element_part_text_t;

typedef struct quizgrind_widget_element_part_input_text_answer_t_
{
  struct quizgrind_widget_element_part_input_text_answer_t_* next;

  char* value;
  uint32_t marks;

} quizgrind_widget_element_part_input_text_answer_t;

typedef struct quizgrind_widget_element_part_input_text_t_
{
  gboolean caps;
  quizgrind_widget_element_part_input_text_answer_t* answers;

} quizgrind_widget_element_part_input_text_t;

typedef struct quizgrind_widget_element_part_input_number_answer_t_
{
  struct quizgrind_widget_element_part_input_number_answer_t_* next;

  double value;
  double error;
  uint32_t marks;

} quizgrind_widget_element_part_input_number_answer_t;

typedef struct quizgrind_widget_element_part_input_number_t_
{
  quizgrind_widget_element_part_input_number_answer_t* answers;

  /* If positive, answer is to the specified number of decimal points.
   * If negative, answer is to the specified number of significant figures. */
  int32_t mindp;

  /* The marks penalty for an answer not provided to the correct
   * decimal points/sig figs of mindp */
  uint32_t mindp_penalty;

  DOUBLETYPE format;
  DOUBLETYPE format_display;

} quizgrind_widget_element_part_input_number_t;

typedef struct quizgrind_widget_element_part_t_
{
  struct quizgrind_widget_element_part_t_* next;
  quizgrind_widget_element_type_t type;

  union {
    quizgrind_widget_element_part_text_t text;
    quizgrind_widget_element_part_input_text_t input_text;
    quizgrind_widget_element_part_input_number_t input_number;
  };

} quizgrind_widget_element_part_t;

typedef struct quizgrind_widget_element_t_
{
  quizgrind_widget_t base;

  struct quizgrind_widget_element_part_t_* part;
  struct quizgrind_widget_element_part_t_** part_end;

} quizgrind_widget_element_t;

extern guint32 quizgrind_widget_element_init(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_element_cleanup(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_element_from_json(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_element_setup_script(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_element_answer_max(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_element_answer_check(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_element_to_json(
    struct quizgrind_scope_t_*, void*);

#endif /* INC_ELEMENT_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
