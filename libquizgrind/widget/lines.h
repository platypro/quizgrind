/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INC_LINEINPUT_H
#define INC_LINEINPUT_H

#include "../../../libquizgrind/core/widget.h"

#define KEY_LINEINPUT_LENGTH  "length"
#define KEY_LINEINPUT_MARKS   "marks"
#define KEY_LINEINPUT_KEY     "key"
#define KEY_LINEINPUT_CAPTION "caption"

typedef struct quizgrind_widget_lines_t_
{
  quizgrind_widget_t base;

  guint32 length;
  guint32 marks;
  char* key;
  char* caption;

} quizgrind_widget_lines_t;

extern guint32 quizgrind_widget_lines_init(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_lines_cleanup(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_lines_from_json(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_lines_answer_max(
    struct quizgrind_scope_t_*, void*);

#define quizgrind_widget_lines_answer_check  quizgrind_widget_default_answer_check

extern guint32 quizgrind_widget_lines_setup_script(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_lines_to_json(
    struct quizgrind_scope_t_*, void*);

#endif /* INC_LINEINPUT_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
