/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INC_CHOICEINPUT_H
#define INC_CHOICEINPUT_H

#include "core/template.h"

#define KEY_CHOICEINPUT_CHOICES "choices"
#define KEY_CHOICEINPUT_CHOICES_TYPE "type"
#define KEY_CHOICEINPUT_CHOICES_TEXT "text"
#define KEY_CHOICEINPUT_CHOICES_TEMPLATE "template"
#define KEY_CHOICEINPUT_CHOICES_MARKS "marks"
#define KEY_CHOICEINPUT_CHOICES_ID "id"
#define KEY_CHOICEINPUT_NUMSELECTIONS "numSelections"
#define KEY_CHOICEINPUT_MARKS "marks"
#define KEY_CHOICEINPUT_RANDOMIZE "randomize"

#define DEF_CHOICEINPUT_PADDING 3
#define DEF_CHOICEINPUT_INDSIZE 20

#define FUN_CHOICEINPUT_ADD_CHOICE "add_choice"
#define FUN_CHOICEINPUT_ADD_CHOICE_SVG "add_choice_svg"

typedef enum quizgrind_widget_choice_type_t_
{
  QUIZGRIND_WIDGET_CHOICE_TYPE_SVG,
  QUIZGRIND_WIDGET_CHOICE_TYPE_TEXT,

} quizgrind_widget_choice_type_t;

typedef enum quizgrind_widget_choice_format_t_
{
  QUIZGRIND_WIDGET_CHOICE_FORMAT_NORMAL,
  QUIZGRIND_WIDGET_CHOICE_FORMAT_TEMPLATE

} quizgrind_widget_choice_format_t;

#define QUIZGRIND_WIDGET_CHOICE_TYPE_SVG_  "svg"
#define QUIZGRIND_WIDGET_CHOICE_TYPE_TEXT_ "text"

typedef struct quizgrind_widget_choice_item_t_
{
  guint32 id;
  quizgrind_widget_choice_type_t type;
  quizgrind_widget_choice_format_t format;
  union {
    char* text;
    quizgrind_template_instance_t* template;
  };

  gint32 marks;

} quizgrind_widget_choice_item_t;

typedef struct quizgrind_widget_choice_t_
{
  quizgrind_widget_t base;

  // quizgrind_widget_choice_item_t* choices;
  // quizgrind_widget_choice_item_t** choices_end;

  GQueue* choices;

  /** The number of selectable inputs. */
  guint32 numSelections;

  /** If zero, this widget is worth marks for each correct answer. Otherwise,
   this amount of marks are awarded only if all correct answers are selected. */
  guint32 fullMarks;

  gboolean randomize;

} quizgrind_widget_choice_t;

extern guint32 quizgrind_widget_choice_init(
    struct quizgrind_scope_t_*, void* data);

extern guint32 quizgrind_widget_choice_cleanup(
    struct quizgrind_scope_t_*, void* data);

extern guint32 quizgrind_widget_choice_from_json(
    struct quizgrind_scope_t_*, void* data);

extern guint32 quizgrind_widget_choice_setup_script(
    struct quizgrind_scope_t_*, void* data);

extern guint32 quizgrind_widget_choice_answer_max(
    struct quizgrind_scope_t_*, void* data);

extern guint32 quizgrind_widget_choice_answer_check(
    struct quizgrind_scope_t_*, void* data);

extern guint32 quizgrind_widget_choice_to_json(
    struct quizgrind_scope_t_*, void* data);

#endif /* INC_CHOICEINPUT_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
