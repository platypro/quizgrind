/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_UTIL_ENUM_H_
#define LIBQUIZGRIND_UTIL_ENUM_H_

#include <glib.h>
#include <wjreader.h>
#include "core/script.h"

#define NUMELEMENTS(array) sizeof(array) / sizeof(*array)

#define LUA_KEY_ENUMFUN   "enumFun"

typedef struct quizgrind_enum_element_t_
{
  char* name;
  guint32 number;

} quizgrind_enum_element_t;

typedef struct quizgrind_enum_t_
{
  quizgrind_enum_element_t* elements;
  guint32 elements_count;

} quizgrind_enum_t;

extern guint32 quizgrind_enum_match_str(quizgrind_enum_t*, const char* str);
extern guint32 quizgrind_enum_match_str_wjr(quizgrind_enum_t*, WJReader reader);
extern guint32 quizgrind_enum_push_lua(lua_State* L, quizgrind_enum_t* enu);

extern char* quizgrind_enum_match_num(quizgrind_enum_t*, guint32 num);

#endif /* LIBQUIZGRIND_UTIL_ENUM_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
