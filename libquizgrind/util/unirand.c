/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "util/unirand.h"

guint32 primes[] = {
2  ,3  ,5  ,7  ,11 ,13 ,17 ,19 ,23, 29,
31 ,37 ,41 ,43 ,47 ,53 ,59 ,61 ,67, 71,
73 ,79 ,83 ,89 ,97 ,101,103,107,109,113,
127,131,137,139,149,151,157,163,167,173,
179,181,191,193,197,199,211,223,227,229,
233,239,241,251,257,263,269,271,277,281,
283,293,307,311,313,317,331,337,347,349,
353,359,367,373,379,383,389,397,401,409,
419,421,431,433,439,443,449,457,461,463,
467,479,487,491,499,503,509,521,523,541,
601,659,733,809,863,941,1013,1069,1151,
1283,1289,1367,1447,1499,1579,1637,1723,
429494501, 429493501, 429486647, 100001053, 100002421, 10001567
};

guint32 numprimes = sizeof(primes) / sizeof(*primes);

void unirand_seed(struct unirand_t* rand, guint32 top)
{
  if(top == 0)
    { return; }

  rand->top = top;
  if(top == 1) 
  {
    rand->prime = 1;
    return;
  }
  rand->offset = g_random_int() % (top-1) + 1;
  
  guint32* prime = primes + numprimes - 1;

  do {
    while(*prime >= top && ((top % *prime) == 0))
    {
      prime --;
      if(prime == primes) prime = primes + numprimes - 1;
    }
  } while(g_random_int_range(0, 3) > 0);
  rand->prime = *prime;
}

guint32 unirand(struct unirand_t* rand, guint32* at)
{
  guint32 result;
  if(rand->top)
  {
    result = (*at * rand->prime + rand->offset) 
                        % (rand->top);
  }
  else result = 0;

  (*at)++;
  return result;
}

guint32 unirand_state(struct unirand_full_t* rand)
  { return unirand(&rand->rand, &rand->at); }

void unirand_linear(struct unirand_t* rand, guint32 max)
{
  rand->offset = 0;
  rand->top = max;
  rand->prime = 1;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
