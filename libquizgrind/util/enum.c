/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "util/enum.h"

#include <stddef.h>

#define QUIZGRIND_ENUM_DEFINITION
#include "enum/gen_mode.h"
#include "enum/calc_type.h"
#include "enum/variable_mode.h"
#undef QUIZGRIND_ENUM_DEFINITION

guint32 quizgrind_enum_match_str(quizgrind_enum_t* enu, const char* str)
{
  if(!str)
    { return 0; }

  for(int i = enu->elements_count; i; i--)
  {
    if(!strcmp(enu->elements[i - 1].name, str))
      { return enu->elements[i - 1].number; }
  }
  return 0;
}

char* quizgrind_enum_match_num(quizgrind_enum_t* enu, guint32 num)
{
  for(int i = enu->elements_count; i; i--)
  {
    if(enu->elements[i - 1].number == num)
      { return enu->elements[i - 1].name; }
  }
  return NULL;
}

guint32 quizgrind_enum_match_str_wjr(quizgrind_enum_t* enu, WJReader reader)
{
  guint32 result = WJRStrCmp(reader,
      (char**)enu->elements,
      enu->elements_count,
      sizeof(quizgrind_enum_element_t),
      offsetof(quizgrind_enum_element_t, name));

  return (result > 0) ? (result - 1) : 0;
}

int lua_pushEnum__index(
    lua_State* L)
{
  quizgrind_script_assert_argnum(L, 2);
  quizgrind_script_assert_argtype(L, 2, LUA_TSTRING);

  const char* str = lua_tostring(L, 2);

  lua_getmetatable(L, 1);
  lua_pushstring(L, LUA_KEY_ENUMFUN);
  lua_gettable(L, -2);
  quizgrind_enum_t* enu = (quizgrind_enum_t*) lua_topointer(L, -1);

  lua_pushnumber(L, quizgrind_enum_match_str(enu, str));

  return 1;
}

guint32 quizgrind_enum_push_lua(lua_State* L, quizgrind_enum_t* enu)
{
  lua_newtable(L);
  // metatable
  lua_newtable(L);

  lua_pushstring(L, "__index");
  lua_pushcfunction(L, lua_pushEnum__index);
  lua_settable(L, -3);

  lua_pushstring(L, LUA_KEY_ENUMFUN);
  lua_pushlightuserdata(L, enu);
  lua_settable(L, -3);

  lua_setmetatable(L, -2);

  return 1;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
