/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "util/number.h"

#include <glib.h>
#include <math.h>
#include <wjwriter.h>

quizgrind_enum_element_t number_num_type_index[] = {
    {"basic",       DOUBLETYPE_BASIC},
    {"engineering", DOUBLETYPE_ENGINEERING},
    {"exponential", DOUBLETYPE_EXPONENTIAL},
    {"invalid",     DOUBLETYPE_INVALID},
    {"normalized",  DOUBLETYPE_NORMALIZED},
    {"si",          DOUBLETYPE_SI}
};

quizgrind_enum_t number_num_type =
{
    .elements = number_num_type_index,
    .elements_count = NUMELEMENTS(number_num_type_index)
};

const char* prefixes = "yzafpnum kMGTPEZY";

gint32 isnegative(char** str)
{
  if(!(*str) || !(**str))
    return 1;
  
  if(**str == '-')
  {
    (*str)++;
    return -1;
  }
  
  if(**str == '+')
  {
    (*str)++;
    return 1;
  }
  
  return 1;
}

void skipws(char** str)
{
  while(*str && g_ascii_isspace(**str))
    (*str)++;
}

guint32 tosimplenumber(char** str, guint32* numdigits, guint32* zeroes)
{
  guint32 result = 0;
  if(numdigits) (*numdigits) = 0;
  if(zeroes)    (*zeroes)    = 0;

  while(**str && g_ascii_isdigit(**str))
  {
    result *= 10;
    result += **str - '0';
    if(numdigits && zeroes)
    {
      if(**str == '0')
        (*zeroes) ++;
      else 
      {
        (*numdigits) += (*zeroes) + 1;
        (*zeroes) = 0;
      }
    }
    (*str) ++;
  }
  return result;
}

gboolean trystr(char** str, char* cmp)
{
  char* cpy = *str;
  while(TRUE)
  {
    if(*cmp == '\000') 
    {
      (*str) = cpy;
      return TRUE;
    }
    if(*cpy == '\000') return FALSE;
    if(*cmp != *cpy) return FALSE;
    cpy++;
    cmp++;
  }
}

gboolean trychar(char** str, char chr)
{
  if(**str == chr)
  {
    (*str) ++;
    return TRUE;
  }
  else
    { return FALSE; }
}

const gchar* trychars(gchar** str, const gchar* search)
{
  while(*search)
  {
    if(trychar(str, *search)) return search;
    search++;
  }
  return NULL;
}

DOUBLETYPE number_parse(char** str, DOUBLERESULT* result)
{
  DOUBLETYPE dtype = 0;
  gint32 negative = FALSE;
  guint32 zeroes = 0;
  
  if(!**str) return DOUBLETYPE_INVALID;

  result->dp = 0;
  result->sigfig = 0;

  //Mantissa
  skipws(str);
  negative = isnegative(str);
  result->value = 
    tosimplenumber(str, &result->sigfig, &zeroes);

  //Fractional component
  if(trychar(str, '.'))
  {
    result->sigfig += zeroes;
   
    result->value = result->value +
       ((gdouble)tosimplenumber(str, &result->dp, &zeroes)
         * pow(10, -((gdouble)result->dp + (gdouble)zeroes)));
    result->dp += zeroes;
    result->sigfig += result->dp;
  }
  
  double valabs = result->value;
  result->value *= negative;
  // Negative is now free to use...
  
  //Exponent
  skipws(str);
  if(trychar(str, 'e') || trychar(str, 'E') || trystr(str, "x10^") || trystr(str, "*10^"))
  {
    dtype |= DOUBLETYPE_EXPONENTIAL;
    
    if(valabs >= 1.0f && valabs < 10.0f)
      dtype |= DOUBLETYPE_NORMALIZED;
    
    negative = isnegative(str);
    guint32 numdigits, numzeros;
    int exp = tosimplenumber(str, &numdigits, &numzeros);
    
    if(!numdigits && !numzeros) return DOUBLETYPE_INVALID;
    
    exp *= negative;
    result->value *= pow(10, exp);
    
    if(exp % 3 == 0)
      dtype |= DOUBLETYPE_ENGINEERING;
  }
  
  //Metric suffix
  skipws(str);
  const char* prefix = NULL;

  dtype |= DOUBLETYPE_SI;
  if((prefix = trychars(str, prefixes)))
  {
    result->value *= pow(10, ((gint32)(prefix - prefixes) * 3) - 24);
  }
  else if(trychar(str, 'c')) result->value *= pow(10, -2);
  else if(trychar(str, 'd')) result->value *= pow(10, -1);
  else if(trystr(str, "da")) result->value *= pow(10,  1);
  else dtype &= ~DOUBLETYPE_SI;
  
  if(!dtype) dtype = DOUBLETYPE_BASIC;
  
  return dtype;
}

int getEngExponent(double rans, gint32* dp)
{
  int exp = 0;
  
  if(rans == 0) return exp;

  while(rans > 10)
  {
    rans = (rans - fmod(rans, 10.0)) / 10;
    exp++;
  }

  while(rans < 1)
  {
    rans *= 10;
    exp--;
  }
  
  int nonexp = (exp % 3);
  exp -= nonexp;
  
  *dp = (*dp) > 0 
    ? (*dp)
    : 0 - (*dp) - nonexp - 1;
    
  if(*dp < 0) *dp = 0;
  
  return exp;
}

void number_format_engineering(char* buf, guint32 len, double number, int dp)
{
  gint32 newDp = dp;
  int exp = getEngExponent(number, &dp);
  
  g_snprintf(buf, 23, "%.*fE%d",
            newDp, 
            number / pow(10, exp), 
            exp);
}

void number_format_basic(char* buf, guint32 len, double number, int dp)
{
  g_snprintf(buf, 23, "%G", number);
}

void number_format_exponential(char* buf, guint32 len, double number, int dp)
{
  gint32 newdp = dp > 0 
    ? dp 
    : dp == 0
      ? 0
      : (- dp - 1);
  g_snprintf(buf, 23, "%.*E", newdp, number);
}

void number_format_si(char* buf, guint32 len, double number, int dp)
{
  int exp = getEngExponent(number, &dp);
  
  g_snprintf(buf, 23, "%.*f%c",
            dp,
            number / pow(10, exp), 
            prefixes[(exp / 3) + 8]);
}

void number_format(char* buf, guint32 len, double number, int dp, DOUBLETYPE format)
{
  if(format & DOUBLETYPE_ENGINEERING)
      number_format_engineering(buf, len, number, dp);
  else if((format & DOUBLETYPE_NORMALIZED) || (format & DOUBLETYPE_EXPONENTIAL))
      number_format_exponential(buf, len, number, dp);
  else if((format & DOUBLETYPE_SI))
      number_format_si(buf, len, number, dp);
  else if((format & DOUBLETYPE_BASIC))
      number_format_basic(buf, len, number, dp);
}

void number_format_str(char* buf, guint32 len, double number, int dp, char* format)
{
  number_format(buf, len, number, dp, quizgrind_enum_match_str(&number_num_type, format));
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
