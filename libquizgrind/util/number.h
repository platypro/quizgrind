/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include <wjwriter.h>
#include <glib.h>

#include "util/enum.h"

#define DOUBLETYPE_BASIC       (0x01)
#define DOUBLETYPE_ENGINEERING (0x02)
#define DOUBLETYPE_EXPONENTIAL (0x04)
#define DOUBLETYPE_NORMALIZED  (0x08)
#define DOUBLETYPE_SI          (0x10)
#define DOUBLETYPE_INVALID     (0x20)

typedef guint32 DOUBLETYPE;

#define NUMBER_BUF_LEN 23

extern quizgrind_enum_t number_num_type;

typedef struct DoubleResult {
  double value;
  guint32 sigfig;
  guint32 dp;
} DOUBLERESULT;

extern DOUBLETYPE number_parse(char** str, DOUBLERESULT* result);

extern void number_format(char* buf, guint32 len, double number, int dp, DOUBLETYPE format);
extern void number_format_str(char* buf, guint32 len, double number, int dp, char* format);

extern DOUBLETYPE number_type_toType(const char* format);
extern void number_type_toWJW(DOUBLETYPE format, WJWriter writer, char* name);
extern char* number_type_toString(DOUBLETYPE format);

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
