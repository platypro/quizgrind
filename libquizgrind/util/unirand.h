/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INC_UNIRAND_H
#define INC_UNIRAND_H

#include <glib.h>

// Non-repeating random number generator
// See Linear congruential generators

struct unirand_t
{
  guint32 top;
  guint32 offset;
  guint32 prime;
};

struct unirand_full_t
{
  struct unirand_t rand;
  guint32 at;
};

extern void unirand_seed(struct unirand_t* rand, guint32 max);
extern void unirand_linear(struct unirand_t* rand, guint32 max);
extern guint32 unirand(struct unirand_t* rand, guint32* at);
extern guint32 unirand_state(struct unirand_full_t* rand);

#endif /* INC_UNIRAND_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
