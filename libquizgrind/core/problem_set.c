/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "core/problem_set.h"

#include "core/project.h"
#include "core/exercise_ref.h"
#include "enum/calc_type.h"

quizgrind_problem_set_t* quizgrind_problem_set_load_path(
    quizgrind_scope_t* scope,
    path_t** path)
{
  FILE* f = fopen(path_get_cstr(path), "r");

  path_extension_set(path, NULL);

  if(!f)
  {
    printf("Problem set %s not found!\n", path_get_basename(path));
    return NULL;
  }

  quizgrind_problem_set_t* pset = calloc(1, sizeof(quizgrind_problem_set_t));

  WJReader pset_rdr =
    WJROpenFILEDocument(f, NULL, 0);

  char* obj = WJRNext(NULL, 15, pset_rdr);

  char* name;
  while((name = WJRNext(obj, 15, pset_rdr)))
  {
  if(WJRCheck(name, WJR_TYPE_ARRAY, KEYLIST(KEY_QUESTION)))
  {
    WJElement questions = WJEOpenDocument(pset_rdr, name, NULL, NULL);
    pset->numExercises = questions->count;
    pset->fullNumExercises = 0;

    pset->exercises = calloc(questions->count, sizeof(quizgrind_exercise_ref_t));
    quizgrind_exercise_ref_t* exercise_ref = pset->exercises;

    WJElement question = NULL;
    while((question = WJEGet(questions, "[]", question)))
    {
      char* exerId = WJEString(question, KEY_ID, WJE_GET, NULL);

      if(!exerId)
      {
        printf("One of your questions is missing an ID!\n");
        pset->numExercises --;
        continue;
      }

      quizgrind_exercise_t* exercise = quizgrind_exercise_get(scope, exerId);
      if(!exercise)
      {
        pset->numExercises --;
        continue;
      }

      if(!quizgrind_exercise_ref_init(scope,
          exercise_ref, exercise,
          WJEUInt32(question, KEY_NUM_QUESTIONS, WJE_GET, 0),
          WJEObject(question, KEYLIST(KEY_OPTION), WJE_GET)))
      {
        printf("Exercise %s failed to init!\n", exercise->id);
        // Add to dead exercise list, to be reloaded in case of
        // change.
        pset->dead_exercises = g_slist_prepend(pset->dead_exercises, exercise);
        pset->numExercises --;
      }

      pset->fullNumExercises += exercise_ref->numInstances;

      exercise_ref ++;
    }
    WJECloseDocument(questions);
  }
  else if(WJRCheck(name, WJR_TYPE_STRING, KEY_CALCULATOR))
    { pset->calculator = quizgrind_enum_match_str_wjr(&quizgrind_calc_type, pset_rdr); }
  else if(WJRCheck(name, WJR_TYPE_STRING, KEY_NAME))
    { pset->name = WJRStringLoad(NULL, pset_rdr); }
  }

  if(pset->numExercises)
    { scope->current_project->problem_sets = g_list_prepend(scope->current_project->problem_sets, pset); }
  else
  {
    scope->current_project->dead_sets = g_list_prepend(scope->current_project->dead_sets, pset);
    printf("Problem set %s has no questions!\n", path_get_basename(path));
  }

  pset->id = g_strdup(path_get_basename(path));
  pset->references = 1;

  WJRCloseDocument(pset_rdr);
  fclose(f);

  return pset;
}

quizgrind_problem_set_t* quizgrind_problem_set_load(
    quizgrind_scope_t* scope,
    char* pset_name)
{
  quizgrind_problem_set_t* result = NULL;
  path_t* path = path_clone(&scope->current_project->path_pset);
  path_push(&path, pset_name);
  path_extension_set(&path, "json");

  result = quizgrind_problem_set_load_path(scope, &path);

  path_destroy(&path);
  return result;
}

quizgrind_problem_set_t* quizgrind_problem_set_get(
    quizgrind_scope_t* scope,
    char* name)
{
  GList* problem_set_list = scope->current_project->problem_sets;

  quizgrind_problem_set_t* problem_set = NULL;
  while(problem_set_list)
  {
    quizgrind_problem_set_t* problem_set_found = (quizgrind_problem_set_t*) problem_set_list->data;
    if(!strcmp(problem_set_found->id, name))
    {
      problem_set = problem_set_found;
      break;
    }
    problem_set_list = problem_set_list->next;
  }

  if(!problem_set && !scope->current_project->pset_watch)
  {
    /* Load the exercise */
    problem_set = quizgrind_problem_set_load(scope, name);
  }

  if(problem_set)
  {
    /* Reference even if new */
    problem_set->references ++;
  }

  return problem_set;
}

void quizgrind_problem_set_unref(
    quizgrind_scope_t* scope,
    quizgrind_problem_set_t* problem_set)
{
  if(!problem_set) return;

  if(problem_set->references)
    { problem_set->references --; }

  if(problem_set->references == 0)
  {
    quizgrind_exercise_ref_t* ref = problem_set->exercises;
    guint32 i = problem_set->numExercises;
    while(i)
    {
      quizgrind_exercise_ref_destroy(scope, ref);
      ref ++; i --;
    }

    g_slist_free(problem_set->dead_exercises);

    free(problem_set->id);
    free(problem_set->exercises);
    if(problem_set->name)
      { free(problem_set->name); }
    free(problem_set);
  }
}

void quizgrind_problem_set_unload(
    quizgrind_scope_t* scope,
    quizgrind_problem_set_t* problem_set)
{
  if(!problem_set) return;
  if(g_list_find(scope->current_project->problem_sets, problem_set))
    { scope->current_project->problem_sets = g_list_remove(scope->current_project->problem_sets, problem_set); }
  else if(g_list_find(scope->current_project->dead_sets, problem_set))
    { scope->current_project->dead_sets = g_list_remove(scope->current_project->dead_sets, problem_set); }

  quizgrind_problem_set_unref(scope, problem_set);
}

gboolean quizgrind_problem_set_contains_exercise(
    struct quizgrind_scope_t_* scope,
    struct quizgrind_problem_set_t_* problem_set,
    struct quizgrind_exercise_t_* exercise)
{
  if(problem_set->dead_exercises && g_slist_find(problem_set->dead_exercises, exercise))
    { return true; }

  if(problem_set->exercises)
  {
    for(int i = 0; i < problem_set->numExercises; i++)
    {
      quizgrind_exercise_ref_t* exercise_ref = &problem_set->exercises[i];
      if(exercise_ref && (exercise_ref->exercise == exercise))
        { return true; }
    }
  }
  return false;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

