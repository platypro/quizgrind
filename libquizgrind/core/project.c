/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "quizgrind.h"

#include <pathbuf.h>

gboolean quizgrind_project_find(path_t** path)
{
  while(!path_is_root(path))
  {
    path_push(path, PROJECT_META);
    if(path_exists(path))
    {
      return true;
    }
    path_pop(path);
    path_pop(path);
  }
  return false;
}

quizgrind_project_t* quizgrind_project_load(path_t* path_)
{
  /* Clone path and make absolute */
  path_t* path = path_clone(&path_);
  path_make_absolute(&path);

  if(!quizgrind_project_find(&path))
  {
    printf("Could not find project!\n");
    path_destroy(&path);
    return NULL;
  }

  FILE* f = fopen(path_get_cstr(&path), "r");
  if(!f) return NULL;
  
  WJReader rdr = WJROpenFILEDocument(f, NULL, 0);
  
  quizgrind_project_t* project = calloc(1, sizeof(quizgrind_project_t));
  project->problem_sets = NULL;
  path_pop(&path);
  project->path_project = path;
  
  char* obj = WJRNext(NULL, 15, rdr);
  char* name;
  while((name = WJRNext(obj, 15, rdr)))
  {
    if(WJRCheck(name, WJR_TYPE_STRING, KEY_NAMESPACE))
      project->namespace = WJRStringLoad(NULL, rdr);
  }

  WJRCloseDocument(rdr);
  fclose(f);
  
  project->path_exercise = path_clone(&project->path_project);
  path_push(&project->path_exercise, PROJECT_EXERCISE);

  project->path_pset = path_clone(&project->path_project);
  path_push(&project->path_pset, PROJECT_PSET);

  quizgrind_script_init(project);

  quizgrind_widget_add_builtin();

  return project;
}

path_t* quizgrind_project_get_path_pset(quizgrind_project_t* project, char* id)
{
  path_t* result = path_clone(&project->path_pset);
  path_push(&result, id);
  path_extension_set(&result, "json");
  return result;
}

path_t* quizgrind_project_get_path_exercise_ns(quizgrind_project_t* project, char* id)
{
  path_t* result = path_clone(&project->path_exercise);
  if(project->namespace)
  {
    path_push(&result, project->namespace);
    path_cat(&result, ".");
    path_cat(&result, id);
    return result;
  }
  else
  {
    path_push(&result, id);
    return result;
  }

  return result;
}

path_t* quizgrind_project_get_path_exercise(quizgrind_project_t* project, char* id)
{
  path_t* result = path_clone(&project->path_exercise);
  if(project->namespace)
  {
    path_push(&result, project->namespace);
    path_cat(&result, id);
    if(path_exists(&result))
      { return result; }
    else
      { path_pop(&result); }
  }

  path_push(&result, id);
  if(!path_exists(&result))
    { path_destroy(&result); }

  return result;
}

void quizgrind_problem_set_changed
  (GFileMonitor     *monitor,
   GFile            *file,
   GFile            *other_file,
   GFileMonitorEvent event_type,
   gpointer          user_data)
{
  quizgrind_project_t* project = (quizgrind_project_t*) user_data;
  quizgrind_scope_t scope = QUIZGRIND_SCOPE_NEW(project);

  char* pset_pathstr = g_file_get_path(file);
  if(!pset_pathstr) return;

  path_t* pset_path = path_new_from_str(pset_pathstr, 0);
  g_free(pset_pathstr);

  switch(event_type)
  {
  case G_FILE_MONITOR_EVENT_DELETED:
  {
    /* Just unref */
    path_extension_set(&pset_path, NULL);
    quizgrind_problem_set_t* problem_set = quizgrind_problem_set_get(&scope, path_get_basename(&pset_path));
    quizgrind_problem_set_unref(&scope, problem_set);
    quizgrind_problem_set_unload(&scope, problem_set);
    break;
  }
  case G_FILE_MONITOR_EVENT_CHANGES_DONE_HINT:
  {
    /* Unref and add */
    path_extension_set(&pset_path, NULL);
    quizgrind_problem_set_t* problem_set = quizgrind_problem_set_get(&scope, path_get_basename(&pset_path));
    quizgrind_problem_set_unref(&scope, problem_set);
    quizgrind_problem_set_unload(&scope, problem_set);
    path_extension_set(&pset_path, "json");
    quizgrind_problem_set_load_path(&scope, &pset_path);
    break;
  }
  default: break;
  }

  path_destroy(&pset_path);
}

void quizgrind_project_watch(struct quizgrind_project_t_* project)
{
  quizgrind_scope_t scope = QUIZGRIND_SCOPE_NEW(project);
  if(project->pset_watch)
  {
    g_signal_handler_disconnect(project->pset_watch, project->pset_watch_changed_id);
    g_object_unref(project->pset_watch);
  }

  /* Load all problem sets */
  path_iterator_t* iter = path_iter(&project->path_pset);
  path_t* subpath = NULL;

  /* Unload all problem sets */
  GList* problem_set_list = project->problem_sets;
  quizgrind_problem_set_t* problem_set = NULL;
  while(problem_set_list && (problem_set = (quizgrind_problem_set_t*) problem_set_list->data))
  {
    GList* problem_set_list_next = problem_set_list->next;
    quizgrind_problem_set_unload(&scope, problem_set);
    problem_set_list = problem_set_list_next;
  }

  /* Reload all problem sets */
  while((subpath = path_iter_next(&iter)))
  {
    path_extension_set(&subpath, NULL);
    quizgrind_problem_set_load(&scope, path_get_basename(&subpath));
  }
  path_iter_end(&iter);

  GFile* file = g_file_new_for_path(path_get_cstr(&project->path_pset));
  project->pset_watch = g_file_monitor_directory(file, G_FILE_MONITOR_NONE, NULL, NULL);
  project->pset_watch_changed_id = g_signal_connect(project->pset_watch, "changed", G_CALLBACK(quizgrind_problem_set_changed), (gpointer) project);
  g_object_unref(file);
}

gboolean quizgrind_project_cleanup(
    quizgrind_project_t* project)
{
  if(!project) 
    { return true; }

  quizgrind_scope_t scope = QUIZGRIND_SCOPE_NEW(project);
  while(project->problem_sets)
  {
    quizgrind_problem_set_t* problem_set = (quizgrind_problem_set_t*)project->problem_sets->data;
    quizgrind_problem_set_unload(&scope, problem_set);
  }
  g_list_free(project->problem_sets);

  while(project->dead_sets)
  {
    quizgrind_problem_set_t* problem_set = (quizgrind_problem_set_t*)project->dead_sets->data;
    quizgrind_problem_set_unload(&scope, problem_set);
  }
  g_list_free(project->dead_sets);

  while(project->exercises)
  {
    quizgrind_exercise_t* exercise = (quizgrind_exercise_t*)project->exercises->data;
    quizgrind_exercise_unload(&scope, exercise);
  }

  quizgrind_script_cleanup(project);
  path_destroy(&project->path_exercise);
  path_destroy(&project->path_pset);
  path_destroy(&project->path_project);
  free(project);

  return true;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
