/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "quizgrind.h"

gboolean quizgrind_quiz_destroy(
    quizgrind_scope_t* scope)
{
  if(scope->current_quiz)
  {
    quizgrind_problem_set_unref(scope, scope->current_quiz->set);
    free(scope->current_quiz->randStatus);
    free(scope->current_quiz);
    scope->current_quiz = NULL;
    return true;
  } else return false;
}

quizgrind_quiz_t* quizgrind_quiz_next(
    struct quizgrind_scope_t_* scope,
    char* pset_str,
    quizgrind_gen_mode_t type,
    guint32 numQuestions)
{
  quizgrind_problem_set_t* pset = quizgrind_problem_set_get(scope, pset_str);

  if(scope->current_question)
  {
    quizgrind_question_unref(scope, scope->current_question);
    scope->current_question = NULL;
  }

  if(scope->current_quiz)
    { quizgrind_quiz_destroy(scope); }

  if(!pset) { return NULL; }

  quizgrind_quiz_t* quiz = calloc(1, sizeof(quizgrind_quiz_t));
  scope->current_quiz = quiz;
  quiz->set = pset;
  quiz->qtype = type;

  scope->current_quiz->randStatus = calloc(pset->numExercises, sizeof(struct unirand_full_t));
  for(int i = 0; i<pset->numExercises; i++)
    { unirand_seed(&scope->current_quiz->randStatus[i].rand, quiz->set->exercises[i].max); }

  if(type == QUIZGRIND_GEN_MODE_RANDOM)
  {
    guint64 totalCount = 0;
    for(guint32 i = 0; i < pset->numExercises; i++)
      { totalCount += pset->exercises[i].max; }
    quiz->questionsLeft = (numQuestions < totalCount) ? numQuestions : totalCount;
  }
  else
  {
    quiz->questionsLeft = pset->fullNumExercises;
    if(type == QUIZGRIND_GEN_MODE_UNORDERED)
      { unirand_seed(&quiz->ordered_rand, pset->numExercises); }
    else if(type == QUIZGRIND_GEN_MODE_ORDERED)
      { unirand_linear(&quiz->ordered_rand, pset->numExercises); }
  }

  return quiz;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
