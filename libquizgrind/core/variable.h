/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_CORE_VARIABLE_H_
#define LIBQUIZGRIND_CORE_VARIABLE_H_

#include "util/unirand.h"
#include "enum/variable_mode.h"

typedef struct quizgrind_variable_t_
{
  struct quizgrind_variable_t_* next;
  struct quizgrind_variable_t_* child;
  char* name;
  guint64 value;

} quizgrind_variable_t;

extern quizgrind_variable_t* quizgrind_variable_from_lua(lua_State* L, int idx, char* exercise_id);
extern void quizgrind_variable_to_lua(lua_State* L, quizgrind_variable_t* variables);
extern void quizgrind_variable_from_json(quizgrind_variable_t** variables, WJReader reader, char* name, char* exercise_id);

extern void quizgrind_variable_destroy(quizgrind_variable_t* variables);

extern guint64 quizgrind_variable_count(quizgrind_variable_t* variables, quizgrind_variable_mode_t mode);
extern void quizgrind_variable_render_lua(lua_State* L, quizgrind_variable_t* variables, quizgrind_variable_mode_t mode, guint32 id);

#endif /* LIBQUIZGRIND_CORE_VARIABLE_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
