/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "quizgrind.h"

#include <pmustache/template.h>
#include <pmustache/provider.wje.h>

#include "template.h"

quizgrind_template_instance_t* quizgrind_template_instance(struct quizgrind_scope_t_* scope, char* name)
{
  quizgrind_template_instance_t* result = calloc(1, sizeof(quizgrind_template_instance_t));

  /* Try finding the template */
  quizgrind_template_t* template = scope->current_exercise->templates;
  while(template)
  {
    if(!strcmp(template->filename, name))
      { break; }
    template = template->next;
  }

  if(!template)
  {
    /* Load the template if not found */
    template = calloc(1, sizeof(quizgrind_template_t));
    if(!template) return false;

    path_t* path = quizgrind_project_get_path_exercise(scope->current_project, scope->current_exercise->id);
    path_push(&path, QUIZGRIND_PATH_TEMPLATE);
    path_push(&path, name);

    template->src = mustache_eatFile(path_get_cstr(&path));
    if(!template->src)
    {
      printf("Could not load template %s\n", path_get_cstr(&path));
      free(template);
      return FALSE;
    }

    template->template = mustache_mkIndex(template->src, 0);
    template->filename = g_strdup(name);
    template->next = scope->current_exercise->templates;
    scope->current_exercise->templates = template;
    path_destroy(&path);
  }

  result->template = template->template;

  return result;
}

quizgrind_template_instance_t* quizgrind_template_instance_wje(struct quizgrind_scope_t_* scope, WJElement element)
{
  char* path = WJEString(element, KEY_PATH, WJE_GET, NULL);
  WJElement subs = WJEObject(element, KEY_SUBS, WJE_GET);
  quizgrind_template_instance_t* template = quizgrind_template_instance(scope, path);
  template->baseContext = WJECopyDocument(template->baseContext, subs, NULL, NULL);
  template->provider = &WJEMustacheProvider;
  template->escape = NULL;
  return template;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
