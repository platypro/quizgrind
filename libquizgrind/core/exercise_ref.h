/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_CORE_EXERCISE_REF_H_
#define LIBQUIZGRIND_CORE_EXERCISE_REF_H_

#include "core/variable.h"
#include "enum/calc_type.h"

typedef struct quizgrind_exercise_ref_t_
{
  quizgrind_exercise_t* exercise;
  WJElement options;
  guint32 numInstances;

  quizgrind_calc_type_t calculator;
  char* name;
  GSList* preamble;
  quizgrind_variable_t* variables;

  quizgrind_variable_mode_t variable_mode;

  uint64_t max;

} quizgrind_exercise_ref_t;

extern char* quizgrind_exercise_ref_get_calc_type(
    quizgrind_exercise_ref_t* exercise);

extern gboolean quizgrind_exercise_ref_init(
    quizgrind_scope_t* scope,
    quizgrind_exercise_ref_t* exercise_ref,
    quizgrind_exercise_t* exercise,
    guint32 numInstances,
    WJElement options);

extern void quizgrind_exercise_ref_destroy(
    quizgrind_scope_t* scope,
    quizgrind_exercise_ref_t* exercise_ref);

#endif /* LIBQUIZGRIND_CORE_EXERCISE_REF_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
