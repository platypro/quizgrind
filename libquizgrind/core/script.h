/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INC_SCRIPT_H
#define INC_SCRIPT_H

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <wjelement.h>

#include "../../libquizgrind/keys.h"

#define LUA_KEY_QUESTION "qg_question"
#define LUA_KEY_EXERCISE "qg_exercise"
#define LUA_KEY_SCRIPTS  "qg_scripts"
#define LUA_KEY_STATE    "qg_state"
#define LUA_KEY_WIDGETS  "qg_widgets"
#define LUA_KEY_SCOPE    "qg_scope"

#define LUA_KEY_TABLEBASE "tableBase"

#define KEYGLOB(k) "_" k

#define FUN_CREATEWIDGET KEYGLOB("mkWidget")
#define FUN_CREATEHINT   KEYGLOB("mkHint")
#define FUN_MAKETEMPLATE KEYGLOB("mkTemplate")

#define FUN_NUMBER_FORMAT KEYGLOB("number_format")
#define FUN_NUMBER_PARSE KEYGLOB("number_parse")
#define FUN_UNIRAND_SEED KEYGLOB("unirand_seed")
#define FUN_UNIRAND KEYGLOB("unirand")
#define FUN_NUMBER_TYPE_TOSTRING KEYGLOB("number_type_toString")

#define GLOB_CALC_TYPE KEYGLOB("CALC_TYPE")
#define GLOB_NUM_TYPE KEYGLOB("NUM_TYPE")
#define GLOB_VARIABLE_MODE KEYGLOB("VARIABLE_MODE")
#define GLOB_OPTIONS   KEYGLOB(KEYLIST(KEY_OPTION))
#define GLOB_STATIC    KEYGLOB(KEY_STATIC)
#define GLOB_VARIABLES KEYGLOB(KEYLIST(KEY_VARIABLE))
#define GLOB_EXERCISE  KEYGLOB(KEY_EXERCISE)

#define DOUBLERESULT_TYPE   "type"
#define DOUBLERESULT_VALUE  "value"
#define DOUBLERESULT_SIGFIG "sigfig"
#define DOUBLERESULT_DP     "dp"

struct quizgrind_exercise_ref_t_;
struct quizgrind_project_t_;
struct quizgrind_question_t_;
struct quizgrind_scope_t_;

extern gboolean quizgrind_script_load(
    struct quizgrind_scope_t_*,
    char* id, char* path);

extern gboolean quizgrind_script_run_init(
    struct quizgrind_scope_t_*,
    struct quizgrind_exercise_ref_t_* exref,
    char* path);

extern gboolean quizgrind_script_unload(
    struct quizgrind_scope_t_*,
    char* id);

extern void quizgrind_script_init(
    struct quizgrind_project_t_*);

extern gboolean quizgrind_script_run_main(
    struct quizgrind_scope_t_*,
    guint32 questionID);

extern void quizgrind_script_cleanup(
    struct quizgrind_project_t_*);

void quizgrind_script_assert_argnum(lua_State* L, int num);
void quizgrind_script_assert_argtype(lua_State* L, int arg, int type);

#endif /*INC_SCRIPT_H*/

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
