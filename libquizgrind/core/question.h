/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_CORE_QUESTION_H_
#define LIBQUIZGRIND_CORE_QUESTION_H_

typedef struct quizgrind_question_t_
{
  guint32 reference_count;

  struct quizgrind_exercise_ref_t_* eref;
  struct quizgrind_widget_list_t_ widgets;

  WJElement hints;

} quizgrind_question_t;

extern quizgrind_question_t* quizgrind_question_next(
    struct quizgrind_scope_t_* scope);

gboolean quizgrind_question_ref(
    quizgrind_question_t* question);

gboolean quizgrind_question_unref(
    quizgrind_scope_t* scope, quizgrind_question_t* question);

extern gboolean quizgrind_question_write_json(
    struct quizgrind_scope_t_* scope,
    WJWriter w,
    char* at,
    quizgrind_widget_json_flags_t elements);

extern guint32 quizgrind_question_check_solutions_json(
    struct quizgrind_scope_t_* scope,
    WJElement solutions,
    WJWriter out);

#endif /* LIBQUIZGRIND_CORE_QUESTION_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
