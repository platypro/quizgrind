/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "quizgrind.h"

#include <math.h>

void quizgrind_variable_to_lua(lua_State* L, quizgrind_variable_t* variables)
{
  lua_newtable(L);
  if(variables)
  {
    while(variables)
    {
      lua_pushstring(L, variables->name);
      if(variables->child)
        { quizgrind_variable_to_lua(L, variables->child); }
      else
        { lua_pushinteger(L, variables->value); }
      lua_settable(L, -3);

      variables = variables->next;
    }
  }
}

quizgrind_variable_t* quizgrind_variable_from_lua(lua_State* L, int idx, char* exercise_id)
{
  quizgrind_variable_t* result = NULL;
  idx = lua_absindex(L, idx);
  lua_pushnil(L);
  while (lua_next(L, idx) != 0)
  {
    if(lua_type(L, -2) != LUA_TSTRING)
    {
      printf("Initialization error in exercise %s: Variable ID must be a string!\n",
          exercise_id);
      lua_pop(L, 1);
      continue;
    }

    if((lua_type(L, -1) != LUA_TNUMBER) && (lua_type(L, -1) != LUA_TTABLE))
    {
      printf("Initialization error in exercise %s: Variable %s must either be a table or a number!\n",
          exercise_id, lua_tostring(L, -2));
      lua_pop(L, 1);
      continue;
    }

    quizgrind_variable_t* variable = calloc(1, sizeof(quizgrind_variable_t));

    if(lua_type(L, -1) == LUA_TNUMBER)
      { variable->value = lua_tointeger(L, -1); }
    else
      { variable->child = quizgrind_variable_from_lua(L, -1, exercise_id); }

    variable->name = g_strdup(lua_tostring(L, -2));
    variable->next = result;
    result = variable;
    lua_pop(L, 1);
  }
  return result;
}

void quizgrind_variable_from_json(quizgrind_variable_t** variables, WJReader reader, char* name, char* exercise_id)
{
  quizgrind_variable_t* var_at = NULL;
  char* welem;
  while((welem = WJRNext(name, 15, reader)))
  {
    if((*welem == WJR_TYPE_NUMBER) || (*welem == WJR_TYPE_OBJECT))
    {
      // Allocate the new variable
      if(!var_at)
      {
        *variables = calloc(1, sizeof(quizgrind_variable_t));
        var_at = *variables;
      }
      else
      {
        quizgrind_variable_t* nvar = calloc(1, sizeof(quizgrind_variable_t));
        var_at->next = nvar;
        var_at = nvar;
      }
    }

    if(*welem == WJR_TYPE_NUMBER)
    {
      var_at->value = WJRUInt64(reader);
      var_at->name = g_strdup(welem + 1);
    }
    else if(*welem == WJR_TYPE_OBJECT)
    {
      var_at->name = g_strdup(welem + 1);
      quizgrind_variable_from_json(&var_at->child, reader, welem + 1, exercise_id);
    }
    else
      { printf("Variable %s in exercise %s does not have a valid value, so ignoring.\n", welem + 1, exercise_id); }
  }
}

guint64 quizgrind_variable_count(quizgrind_variable_t* variables, quizgrind_variable_mode_t mode)
{
  guint64 count = 0;

  /* Both paths of this if statement are the same, except all references to multiplication
   * and addition are swapped.
   */
  if(mode == QUIZGRIND_VARIABLE_MODE_PRODUCT)
  {
    count = 1; /* Monoid identity for product */
    while(variables)
    {
      if(variables->child)
        /* Count variables in opposite mode (So then there is a Sum-of-products or product-of-sums */
        { count *= quizgrind_variable_count(variables->child, QUIZGRIND_VARIABLE_MODE_SUM); }
      else
        /* Otherwise just take the variable value */
        { count *= variables->value; }

      variables = variables->next;
    }
  }
  else
  {
    count = 0;
    while(variables)
    {
      if(variables->child)
        { count += quizgrind_variable_count(variables->child, QUIZGRIND_VARIABLE_MODE_PRODUCT); }
      else
        { count += variables->value; }

      variables = variables->next;
    }
  }

  return count;
}

/* Render all zeroes to variable */
void quizgrind_variable_render0_lua(lua_State* L, quizgrind_variable_t* variables, quizgrind_variable_mode_t mode)
{
  lua_createtable(L, 0, 0);
  while(variables)
  {
    lua_pushstring(L, variables->name);
    if(variables->child)
    {
      quizgrind_variable_render0_lua(L, variables->child,
          (mode == QUIZGRIND_VARIABLE_MODE_PRODUCT)
          ? QUIZGRIND_VARIABLE_MODE_SUM : QUIZGRIND_VARIABLE_MODE_PRODUCT);
    }
    else
      { lua_pushinteger(L, 0); }
    lua_settable(L, -3);

    variables = variables->next;
  }
}

void quizgrind_variable_render_lua(lua_State* L, quizgrind_variable_t* variables, quizgrind_variable_mode_t mode, guint32 id)
{
  guint64 multiplier = 1;
  quizgrind_variable_mode_t mode_other = mode;

  lua_createtable(L, 0, 0);
  if(mode == QUIZGRIND_VARIABLE_MODE_SUM)
    { id ++; }
  while(variables)
  {
    guint64 val = 0;

    if(mode == QUIZGRIND_VARIABLE_MODE_PRODUCT)
    {
      mode_other = QUIZGRIND_VARIABLE_MODE_SUM;

      guint64 maxval = variables->child
          ? quizgrind_variable_count(variables->child, mode_other)
          : variables->value;

      if(maxval)
        { val = ceil((id % (multiplier * maxval)) / multiplier); }
      if(val==0) val = maxval;

      multiplier *= maxval;
    }
    else if(mode == QUIZGRIND_VARIABLE_MODE_SUM)
    {
      mode_other = QUIZGRIND_VARIABLE_MODE_PRODUCT;

      guint64 maxval = variables->child
          ? quizgrind_variable_count(variables->child, mode_other)
          : variables->value;

      if(id > maxval)
      {
        val = 0;
        id -= maxval;
      }
      else if(id == 0)
        { val = 0; }
      else
      {
        val = id;
        id = 0;
      }
    }

    lua_pushstring(L, variables->name);
    if(variables->child)
    {
      if(val)
        { quizgrind_variable_render_lua(L, variables->child, mode_other, val - 1); }
      else
        { quizgrind_variable_render0_lua(L, variables->child, mode_other); }
    }
    else
      { lua_pushinteger(L, val); }
    lua_settable(L, -3);

    variables = variables->next;
  }
}

void quizgrind_variable_destroy(quizgrind_variable_t* variables)
{
  while(variables)
  {
    quizgrind_variable_t* variables_next = variables->next;

    if(variables->child)
      { quizgrind_variable_destroy(variables->child); }

    free(variables->name);
    free(variables);

    variables = variables_next;
  }
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
