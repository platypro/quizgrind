/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "core/exercise.h"

#include <wjelement.h>
#include <pathbuf.h>
#include <stdio.h>
#include <gio/gio.h>

#include "core/template.h"
#include "enum/calc_type.h"
#include "core/problem_set.h"
#include "core/exercise_ref.h"

void quizgrind_exercise_unref(quizgrind_scope_t* scope, quizgrind_exercise_t* exercise)
{
  QUIZGRIND_SCOPE_SET_EXERCISE(scope, exercise);
  if(exercise->references)
    { exercise->references --; }

  if(exercise->references == 0)
  {
    g_signal_handler_disconnect(exercise->monitor, exercise->monitor_changed_id);
    g_signal_handler_disconnect(exercise->monitor_template, exercise->monitor_template_changed_id);
    g_object_unref(exercise->monitor);
    g_object_unref(exercise->monitor_template);
    g_slist_free_full(exercise->basePreamble, free);

    quizgrind_script_unload(scope, exercise->scriptid);

    WJECloseDocument(exercise->baseOptions);
    WJECloseDocument(exercise->baseHints);
    WJECloseDocument(exercise->staticStore);
    quizgrind_widget_list_destroy(scope, &exercise->baseWidgets);
    quizgrind_variable_destroy(exercise->baseVariables);

    quizgrind_template_t* temp = exercise->templates;
    while(temp)
    {
      quizgrind_template_t* temp_next = temp->next;
      free(temp->src);
      mustache_destroyIndex(temp->template);
      free(temp->filename);
      free(temp);
      temp = temp_next;
    }

    free(exercise->baseName);
    free(exercise->id);
    free(exercise);
  }
  QUIZGRIND_SCOPE_UNSET_EXERCISE(scope);
}

void quizgrind_exercise_unload(quizgrind_scope_t* scope, quizgrind_exercise_t* exercise)
{
  if(!exercise) return;

  scope->current_project->exercises = g_list_remove(scope->current_project->exercises, exercise);
  quizgrind_exercise_unref(scope, exercise);
}

static void quizgrind_exercise_reload_(quizgrind_scope_t* scope, GList* problem_set_list, quizgrind_exercise_t* exercise)
{
  while(problem_set_list)
  {
    GList* problem_set_list_next = problem_set_list->next;

    quizgrind_problem_set_t* problem_set =
        (quizgrind_problem_set_t*) problem_set_list->data;

    if(quizgrind_problem_set_contains_exercise(scope, problem_set, exercise))
    {
      /* Reload problem set */
      quizgrind_problem_set_load(scope, problem_set->id);
      quizgrind_problem_set_unload(scope, problem_set);
    }

    problem_set_list = problem_set_list_next;
  }
}

void quizgrind_exercise_reload(quizgrind_scope_t* scope, quizgrind_exercise_t* exercise)
{
  /* Unref and attempt re-add */
  quizgrind_exercise_unload(scope, exercise);

  quizgrind_exercise_reload_(scope, scope->current_project->problem_sets, exercise);
  quizgrind_exercise_reload_(scope, scope->current_project->dead_sets, exercise);

  quizgrind_exercise_unref(scope, exercise);
}

void quizgrind_exercise_template_changed
  (GFileMonitor     *monitor,
   GFile            *file,
   GFile            *other_file,
   GFileMonitorEvent event_type,
   gpointer          user_data)
{
  if(event_type != G_FILE_MONITOR_EVENT_CHANGES_DONE_HINT)
    { return; }
  quizgrind_project_t* project = (quizgrind_project_t*) user_data;
  quizgrind_scope_t scope = QUIZGRIND_SCOPE_NEW(project);

  char* exercise_pathstr = g_file_get_path(file);
  if(!exercise_pathstr) return;

  path_t* exercise_path = path_new_from_str(exercise_pathstr, 0);
  g_free(exercise_pathstr);

  path_t* template_path = path_clone(&exercise_path);
  path_pop(&exercise_path);
  path_pop(&exercise_path);

  quizgrind_exercise_t* exercise = quizgrind_exercise_get(&scope, path_get_basename(&exercise_path));

  quizgrind_template_t* find = exercise->templates;
  while(find)
  {
    if(!strcmp(find->filename, path_get_basename(&template_path)))
    {
      printf("Reload template %s\n", path_get_cstr(&template_path));
      quizgrind_exercise_reload(&scope, exercise);
      break;
    }
    find = find->next;
  }

  path_destroy(&template_path);
  path_destroy(&exercise_path);
}

void quizgrind_exercise_changed
  (GFileMonitor     *monitor,
   GFile            *file,
   GFile            *other_file,
   GFileMonitorEvent event_type,
   gpointer          user_data)
{
  quizgrind_project_t* project = (quizgrind_project_t*) user_data;
  quizgrind_scope_t scope = QUIZGRIND_SCOPE_NEW(project);

  char* exercise_pathstr = g_file_get_path(file);
  if(!exercise_pathstr) return;

  path_t* exercise_path = path_new_from_str(exercise_pathstr, 0);
  g_free(exercise_pathstr);

  if(!strcmp(path_get_basename(&exercise_path), PROJECT_EXERCISE_META)
  || !strcmp(path_get_basename(&exercise_path), PROJECT_EXERCISE_SCRIPT)
  || !strcmp(path_get_basename(&exercise_path), PROJECT_EXERCISE_INIT))
  {
    path_pop(&exercise_path);

    if(path_get_basename(&exercise_path))
    {
      switch(event_type)
      {
      case G_FILE_MONITOR_EVENT_DELETED:
      case G_FILE_MONITOR_EVENT_CHANGES_DONE_HINT:
      {
        quizgrind_exercise_t* exercise = quizgrind_exercise_get(
            &scope, path_get_basename(&exercise_path));
        quizgrind_exercise_reload(&scope, exercise);
        break;
      }
      default: break;
      }
    }
  }

  path_destroy(&exercise_path);
}

quizgrind_exercise_t* quizgrind_exercise_load(
    quizgrind_scope_t* scope,
    char* exerId)
{
  quizgrind_exercise_t* exercise = NULL;
  path_t* exercise_path = quizgrind_project_get_path_exercise(scope->current_project, exerId);
  if(!exercise_path)
  {
    printf("Exercise %s not found!\n", exerId);
    return NULL;
  }

  path_push(&exercise_path, PROJECT_EXERCISE_META);
  FILE* f = fopen(path_get_cstr(&exercise_path), "r");
  if(!f) 
  { 
    printf("File %s not found, aborting.\n", path_get_cstr(&exercise_path));
    return NULL;
  }
  path_pop(&exercise_path);
  
  WJReader exer_rdr =
    WJROpenFILEDocument(f, NULL, 0);

  //Load the exercise
  exercise = calloc(1, sizeof(quizgrind_exercise_t));
  exercise->id  = g_strdup(exerId);
  exercise->references = 1;

  scope->current_project->exercises = g_list_prepend(scope->current_project->exercises, exercise);

  GFile* file = g_file_new_for_path(path_get_cstr(&exercise_path));
  exercise->monitor = g_file_monitor_directory(file, G_FILE_MONITOR_NONE, NULL, NULL);
  exercise->monitor_changed_id =
      g_signal_connect(exercise->monitor, "changed", G_CALLBACK(quizgrind_exercise_changed), (gpointer) scope->current_project);
  g_object_unref(file);

  path_push(&exercise_path, QUIZGRIND_PATH_TEMPLATE);
  file = g_file_new_for_path(path_get_cstr(&exercise_path));
  exercise->monitor_template = g_file_monitor_directory(file, G_FILE_MONITOR_NONE, NULL, NULL);
  exercise->monitor_template_changed_id =
      g_signal_connect(exercise->monitor_template, "changed", G_CALLBACK(quizgrind_exercise_template_changed), (gpointer) scope->current_project);
  g_object_unref(file);
  path_pop(&exercise_path);

  path_push(&exercise_path, PROJECT_EXERCISE_SCRIPT);
  exercise->hasScript = path_exists(&exercise_path);

  char* obj = WJRNext(NULL, 15, exer_rdr);
  
  char* name;
  while((name = WJRNext(obj, 15, exer_rdr)))
  {
  if(WJRCheck(name, WJR_TYPE_STRING, KEY_NAME))
    exercise->baseName = WJRStringLoad(NULL, exer_rdr);
  else if(WJRCheck(name, WJR_TYPE_STRING, KEY_CALCULATOR))
    exercise->baseCalculator = quizgrind_enum_match_str_wjr(&quizgrind_calc_type, exer_rdr);
  else if(WJRCheck(name, WJR_TYPE_STRING, KEY_PREAMBLE))
  {
    // Preamble is single string
    exercise->basePreamble = g_slist_prepend(exercise->basePreamble, WJRStringLoad(NULL, exer_rdr));
  }
  else if(WJRCheck(name, WJR_TYPE_ARRAY, KEY_PREAMBLE))
  {
    // Preamble is multiple string (Multiple lines)
    char* wobj;
    while((wobj = WJRNext(name, 15, exer_rdr)))
      { exercise->basePreamble = g_slist_prepend(exercise->basePreamble, WJRStringLoad(NULL, exer_rdr)); }

    exercise->basePreamble = g_slist_reverse(exercise->basePreamble);
  }
  else if(WJRCheck(name, WJR_TYPE_OBJECT, KEYLIST(KEY_OPTION)))
    exercise->baseOptions = WJEOpenDocument(exer_rdr, name, NULL, NULL);
  else if(WJRCheck(name, WJR_TYPE_OBJECT, KEYLIST(KEY_VARIABLE)))
    { quizgrind_variable_from_json(&exercise->baseVariables, exer_rdr, name, exerId); }
  else if(!strcmp(name + 1, KEY_STATIC))
    exercise->staticStore = WJEOpenDocument(exer_rdr, name, NULL, NULL);
  else if(WJRCheck(name, WJR_TYPE_ARRAY, KEYLIST(KEY_HINT)))
    exercise->baseHints = WJEOpenDocument(exer_rdr, name, NULL, NULL);
  else if(WJRCheck(name, WJR_TYPE_STRING, KEY_VARIABLE_MODE))
    { exercise->baseVariable_mode = quizgrind_enum_match_str_wjr(&quizgrind_variable_mode, exer_rdr); }
  else if(WJRCheck(name, WJR_TYPE_ARRAY, KEYLIST(KEY_WIDGET)))
  {
    char* wobj;
    while((wobj = WJRNext(name, 15, exer_rdr)))
    {
      quizgrind_widget_interface_t* iface = NULL;
      WJElement element = NULL;
      char* welem;
      while((welem = WJRNext(wobj, 15, exer_rdr)))
      {
        if(WJRCheck(welem, WJR_TYPE_STRING, KEY_TYPE))
          { iface = quizgrind_widget_interface_get_wjr(exer_rdr); }
        else if(WJRCheck(welem, WJR_TYPE_OBJECT, KEYLIST(KEY_OPTION)))
          { element = WJEOpenDocument(exer_rdr, welem, NULL, NULL); }
      }

      if(iface)
      {
        QUIZGRIND_SCOPE_SET_EXERCISE(scope, exercise);
        quizgrind_widget_create(scope, &scope->current_exercise->baseWidgets, iface);
        if(element)
        {
          quizgrind_widget_from_json(scope, element);
          WJECloseDocument(element);
        }
        QUIZGRIND_SCOPE_UNSET_EXERCISE(scope);
      }
    }
  }
  }

  *((uint32_t*)exercise->scriptid) = g_random_int();
  exercise->scriptid[4] = 0;

  if(exercise->hasScript && !quizgrind_script_load(scope, exercise->scriptid, path_get_cstr(&exercise_path)))
  {
    path_destroy(&exercise_path);
    WJRCloseDocument(exer_rdr); 
    return NULL; 
  }

  path_destroy(&exercise_path);
  WJRCloseDocument(exer_rdr);
  fclose(f);
  return exercise;
}

quizgrind_exercise_t* quizgrind_exercise_get(
    quizgrind_scope_t* scope,
    char* name)
{
  //First see if exercise is already loaded
  GList* exercise_list = scope->current_project->exercises;
  quizgrind_exercise_t* exercise = NULL;
  while(exercise_list)
  {
    quizgrind_exercise_t* exercise_found = (quizgrind_exercise_t*)exercise_list->data;
    if(!strcmp(exercise_found->id, name))
    {
      exercise = exercise_found;
      break;
    }
    exercise_list = exercise_list->next;
  }

  if(!exercise)
    { exercise = quizgrind_exercise_load(scope, name); }

  if(exercise)
  {
    /* Reference even if new */
    exercise->references ++;
  }

  return exercise;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
