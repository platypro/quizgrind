/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_CORE_QUIZ_H_
#define LIBQUIZGRIND_CORE_QUIZ_H_

#include "enum/gen_mode.h"

typedef struct quizgrind_quiz_t_
{
  struct quizgrind_problem_set_t_* set;
  enum quizgrind_gen_mode_t_ qtype;

  struct unirand_full_t* randStatus;
  guint32 questionsLeft;

  struct unirand_t ordered_rand;
  guint32 rand_at;

  struct quizgrind_exercise_ref_t_* currentExercise;

  /* Current question/subquestion status.
   * If zero, then this is a single question.
   * If one, this is the first in a series of similar questions.
   * This number continues to go up as the series progresses.
   */
  guint32 sequenceID;

} quizgrind_quiz_t;

extern quizgrind_quiz_t* quizgrind_quiz_next(
    struct quizgrind_scope_t_* scope,
    char* pset,
    quizgrind_gen_mode_t type,
    guint32);

#endif /* LIBQUIZGRIND_CORE_QUIZ_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
