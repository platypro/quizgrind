/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INC_WIDGET_H
#define INC_WIDGET_H

#include <stdarg.h>
#include <wjelement.h>
#include <lua.h>

struct quizgrind_question_t_;
struct quizgrind_widget_t_;
struct quizgrind_pdf_ctx_t_;
struct quizgrind_scope_t_;

typedef guint32 quizgrind_widget_json_flags_t;
// Give the question
#define QUIZGRIND_WIDGET_JSON_QUESTION (1<<0)

// Give the solution
#define QUIZGRIND_WIDGET_JSON_SOLUTION (1<<1)

// Give the full path
#define QUIZGRIND_WIDGET_JSON_FULLPATH (1<<2)

typedef guint32 (*quizgrind_widget_function_t) (
    struct quizgrind_scope_t_* scope,
    gpointer data);

/* This is needed because function pointers and data pointers are incompatible in C99 */
typedef struct quizgrind_widget_function_container_t_
{
  quizgrind_widget_function_t function;

} quizgrind_widget_function_container_t;

#define QUIZGRIND_WIDGET_CHOICE "choice"
#define QUIZGRIND_WIDGET_LINES "lines"
#define QUIZGRIND_WIDGET_ELEMENT "element"
#define QUIZGRIND_WIDGET_IMAGE "image"

#define QUIZGRIND_WIDGET_FUN_INIT         "init"
#define QUIZGRIND_WIDGET_FUN_CLEANUP      "cleanup"
#define QUIZGRIND_WIDGET_FUN_TO_JSON      "to_json"
#define QUIZGRIND_WIDGET_FUN_ANSWER_CHECK "answer_check"
#define QUIZGRIND_WIDGET_FUN_ANSWER_MAX   "answer_max"
#define QUIZGRIND_WIDGET_FUN_FROM_JSON    "from_json"
#define QUIZGRIND_WIDGET_FUN_SETUP_SCRIPT "setup_script"

typedef struct quizgrind_widget_interface_t_
{
  char* name;
  gsize size;

  GHashTable* functions;

} quizgrind_widget_interface_t;

typedef struct quizgrind_widget_t_
{
  struct quizgrind_widget_t_* next;
  
  guint32 id;
  quizgrind_widget_interface_t* type;
  
} quizgrind_widget_t;

typedef char* quizgrind_widget_answer_check_t;

typedef struct quizgrind_widget_to_json_t_
{
  WJWriter writer;
  quizgrind_widget_json_flags_t flags;

} quizgrind_widget_to_json_t;

typedef WJElement quizgrind_widget_from_json_t;

typedef struct quizgrind_widget_setup_script_t_
{
  lua_State* L;
  int idx;

} quizgrind_widget_setup_script_t;

typedef struct quizgrind_widget_iter_t_
{
  quizgrind_widget_t* baseWidgetAt;
  quizgrind_widget_t* questionWidgetAt;

} quizgrind_widget_iter_t;

typedef struct quizgrind_widget_list_t_
{
  quizgrind_widget_t* widget;
  quizgrind_widget_t* widget_end;

} quizgrind_widget_list_t;

extern void quizgrind_widget_list_append(
    quizgrind_widget_list_t* list,
    quizgrind_widget_t* widget);

extern quizgrind_widget_interface_t* quizgrind_widget_interface_create(
    char* name,
    size_t struct_size);

extern guint32 quizgrind_widget_interface_run_handler(
    quizgrind_widget_interface_t* iface,
    char* name,
    struct quizgrind_scope_t_* scope,
    gpointer data);

extern quizgrind_widget_function_t quizgrind_widget_interface_get_handler(
    quizgrind_widget_interface_t* iface,
    char* name);

extern void quizgrind_widget_interface_set_handler(
    quizgrind_widget_interface_t* iface,
    char* name,
    quizgrind_widget_function_t function);

extern void quizgrind_widget_add_builtin();

extern guint32 quizgrind_widget_default_answer_max(
    struct quizgrind_scope_t_*, void*);

extern guint32 quizgrind_widget_default_answer_check(
    struct quizgrind_scope_t_*, void*);

#define quizgrind_widget_default_secureWrite quizgrind_widget_to_json

extern quizgrind_widget_interface_t* quizgrind_widget_interface_get(
    const char* string);

extern quizgrind_widget_interface_t* quizgrind_widget_interface_get_wjr(
    WJReader reader);

extern quizgrind_widget_t* quizgrind_widget_create(
    struct quizgrind_scope_t_* state,
    struct quizgrind_widget_list_t_* list,
    struct quizgrind_widget_interface_t_* iface);

extern void quizgrind_widget_from_json(
    struct quizgrind_scope_t_* w,
    WJElement element);

extern quizgrind_widget_t* quizgrind_widget_set_type(
    quizgrind_widget_t* widget,
    const char* type);

extern void quizgrind_widget_setup_script(
    struct quizgrind_scope_t_* w,
    lua_State* L);

extern guint32 quizgrind_widget_answer_check(
    struct quizgrind_scope_t_* w,
    char* uans);

extern guint32 quizgrind_widget_answer_max(
    struct quizgrind_scope_t_* w);

extern gboolean quizgrind_widget_to_json(
    struct quizgrind_scope_t_* state,
    WJWriter w,
    char* name,
    quizgrind_widget_json_flags_t elements);

extern size_t quizgrind_widget_writeWJW(
    char *data,
    size_t length,
    void *userdata);

void quizgrind_widget_list_destroy(
    quizgrind_scope_t* scope,
    quizgrind_widget_list_t* widget);

void quizgrind_widget_iter(
    quizgrind_widget_iter_t* iter,
    quizgrind_scope_t* scope);

quizgrind_widget_t* quizgrind_widget_iter_next(
    quizgrind_widget_iter_t* iter,
    quizgrind_scope_t* scope);

void quizgrind_widget_iter_end(
    quizgrind_widget_iter_t* iter,
    quizgrind_scope_t* scope);

#endif

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
