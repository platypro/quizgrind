/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INCLUDE_PROJECT_H
#define INCLUDE_PROJECT_H

struct quizgrind_project_t_;

#include <pathbuf.h>

#include "core/exercise.h"
#include "core/script.h"

typedef struct quizgrind_project_t_
{
  path_t* path_project;
  path_t* path_exercise;
  path_t* path_pset;

  int pset_watch_changed_id;
  GFileMonitor* pset_watch;

  char* namespace;

  lua_State* L;
  GList* problem_sets;
  GList* dead_sets;
  GList* exercises;

} quizgrind_project_t;

#define KEY_NAMESPACE "namespace"

#define PROJECT_META "quizgrind.conf"
#define PROJECT_EXERCISE "exercise"
#define PROJECT_PSET "pset"
#define PROJECT_EXERCISE_TEMPLATE "template"
#define PROJECT_EXERCISE_META   "exercise.json"
#define PROJECT_EXERCISE_SCRIPT "script.lua"
#define PROJECT_EXERCISE_INIT   "init.lua"

extern struct quizgrind_project_t_* quizgrind_project_load(
    path_t* path);

extern path_t* quizgrind_project_get_path_pset(
    quizgrind_project_t* project,
    char* id);

/** Get exercise path with namespace. */
extern path_t* quizgrind_project_get_path_exercise_ns(
    quizgrind_project_t* project,
    char* id);

/** Get exercise path, if available. */
extern path_t* quizgrind_project_get_path_exercise(
    quizgrind_project_t* project,
    char* id);

extern void quizgrind_project_watch(
    struct quizgrind_project_t_* project);

extern gboolean quizgrind_project_cleanup(
    struct quizgrind_project_t_* project);

#endif /* INCLUDE_PROJECT_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

