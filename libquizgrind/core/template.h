/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_CORE_TEMPLATE_H_
#define LIBQUIZGRIND_CORE_TEMPLATE_H_

#define QUIZGRIND_PATH_TEMPLATE "templates"

typedef struct quizgrind_template_t_
{
  struct quizgrind_template_t_* next;
  char* filename;
  char* src;
  PMUS_TEMPLATE* template;

} quizgrind_template_t;

typedef PMUS_BUILDER quizgrind_template_instance_t;

extern quizgrind_template_instance_t* quizgrind_template_instance_wje(
    struct quizgrind_scope_t_* scope,
    WJElement element);

extern quizgrind_template_instance_t* quizgrind_template_instance(
    struct quizgrind_scope_t_* scope,
    char* name);

#endif /* LIBQUIZGRIND_CORE_TEMPLATE_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
