/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INCLUDE_EXERCISE_H
#define INCLUDE_EXERCISE_H

#include <wjelement.h>
#include <pmustache/template.h>
#include <glib.h>
#include <gio/gio.h>

#include "core/project.h"
#include "core/scope.h"
#include "core/script.h"
#include "core/widget.h"
#include "core/template.h"
#include "core/variable.h"
#include "enum/calc_type.h"
#include "enum/variable_mode.h"
#include "util/unirand.h"

typedef struct quizgrind_exercise_t_
{
  struct quizgrind_widget_list_t_ baseWidgets;
  WJElement baseHints;
  struct quizgrind_template_old_instance_t_* baseTemplates;
  
  WJElement staticStore;
  WJElement baseOptions;
  
  quizgrind_calc_type_t baseCalculator;
  char* baseName;
  //char* basePreamble;
  GSList* basePreamble;

  char* id;
  char scriptid[5];

  quizgrind_variable_mode_t baseVariable_mode;
  quizgrind_variable_t* baseVariables;

  quizgrind_template_t* templates;
  gboolean hasScript;
  
  GFileMonitor* monitor;
  int monitor_changed_id;

  GFileMonitor* monitor_template;
  int monitor_template_changed_id;

  guint32 references;

} quizgrind_exercise_t;

#define QUIZGRIND_PROBLEM_SET_DEFAULT_NAME ""

quizgrind_exercise_t* quizgrind_exercise_get(
    struct quizgrind_scope_t_* scope,
    char* name);

extern void quizgrind_exercise_unref(
    struct quizgrind_scope_t_* scope,
    quizgrind_exercise_t* exercise);

extern void quizgrind_exercise_unload(
    struct quizgrind_scope_t_* scope,
    quizgrind_exercise_t* exercise);

#endif /*INCLUDE_EXERCISE_H*/

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
