/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "quizgrind.h"
#include "enum/calc_type.h"

char* quizgrind_exercise_ref_get_calc_type(
    quizgrind_exercise_ref_t* exercise)
{
  return quizgrind_enum_match_num(&quizgrind_calc_type, exercise->calculator);
}

gboolean quizgrind_exercise_ref_init(
    quizgrind_scope_t* scope,
    quizgrind_exercise_ref_t* exercise_ref,
    quizgrind_exercise_t* exercise,
    guint32 numInstances,
    WJElement options)
{
  exercise_ref->exercise = exercise;
  exercise_ref->options = WJEObject(NULL, NULL, WJE_NEW);

  exercise_ref->numInstances = numInstances;
  if(exercise_ref->numInstances == 1) exercise_ref->numInstances = 0;

  WJEMergeObjects(exercise_ref->options, exercise->baseOptions, false);
  WJEMergeObjects(exercise_ref->options, options, true);

  path_t* initpath = quizgrind_project_get_path_exercise(scope->current_project, exercise_ref->exercise->id);
  path_push(&initpath, PROJECT_EXERCISE_INIT);
  if(path_exists(&initpath))
  {
    if(!quizgrind_script_run_init(scope, exercise_ref, path_get_cstr(&initpath)))
    {
      path_destroy(&initpath);
      return FALSE;
    }
  }
  else
  {
    if(exercise->baseName)
      { exercise_ref->name = g_strdup(exercise->baseName); }
    if(exercise->basePreamble)
      { exercise_ref->preamble = exercise->basePreamble; }
    if(exercise->baseVariables)
      { exercise_ref->variables = exercise->baseVariables; }

    exercise_ref->variable_mode = exercise->baseVariable_mode;
    exercise_ref->calculator = exercise->baseCalculator;
  }
  path_destroy(&initpath);

  // Initalize unirand
  exercise_ref->max = quizgrind_variable_count(exercise_ref->variables, exercise_ref->variable_mode);

  return TRUE;
}

void quizgrind_exercise_ref_destroy(
    quizgrind_scope_t* scope,
    quizgrind_exercise_ref_t* exercise_ref)
{
  if(exercise_ref->name)
    { free(exercise_ref->name); }

  if(exercise_ref->variables != exercise_ref->exercise->baseVariables)
    { quizgrind_variable_destroy(exercise_ref->variables); }

  if(exercise_ref->preamble != exercise_ref->exercise->basePreamble)
    { g_slist_free_full(exercise_ref->preamble, free); }

  WJECloseDocument(exercise_ref->options);
  quizgrind_exercise_unref(scope, exercise_ref->exercise);
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
