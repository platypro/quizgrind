/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_CORE_PROBLEM_SET_C_
#define LIBQUIZGRIND_CORE_PROBLEM_SET_C_

#include <wjelement.h>
#include <glib.h>

#include "enum/calc_type.h"
#include "core/project.h"

typedef struct quizgrind_problem_set_t_
{
  guint32 references;

  char* name;
  struct quizgrind_exercise_ref_t_* exercises;
  GSList* dead_exercises;
  guint32 numExercises;
  guint32 fullNumExercises;

  char* id;

  quizgrind_calc_type_t calculator;

} quizgrind_problem_set_t;

extern void quizgrind_problem_set_unref(
    struct quizgrind_scope_t_*,
    quizgrind_problem_set_t*);

extern void quizgrind_problem_set_unload(
    struct quizgrind_scope_t_*,
    quizgrind_problem_set_t*);

extern quizgrind_problem_set_t* quizgrind_problem_set_load_path(
    struct quizgrind_scope_t_*,
    path_t**);

extern quizgrind_problem_set_t* quizgrind_problem_set_load(
    struct quizgrind_scope_t_*,
    char*);

extern quizgrind_problem_set_t* quizgrind_problem_set_get(
    struct quizgrind_scope_t_*,
    char*);

extern gboolean quizgrind_problem_set_contains_exercise(
    struct quizgrind_scope_t_*,
    struct quizgrind_problem_set_t_*,
    struct quizgrind_exercise_t_*);

#endif /* LIBQUIZGRIND_CORE_PROBLEM_SET_C_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
