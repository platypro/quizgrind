/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "lua.h"
#include "quizgrind.h"
#include "core/script.h"

#include <ctype.h>
#include <string.h>
#include <dirent.h>
#include <stdlib.h>
#include <math.h>
#include <wjelement.h>
#include <pmustache/provider.wje.h>

#include "keys.h"
#include "core/template.h"
#include "util/number.h"

void lua_pushJSON(lua_State* L, WJElement src);

void quizgrind_script_assert_argnum(
    lua_State* L,
    int num)
{
  if(lua_gettop(L) < num)
  {
    luaL_error(L, "Wrong number of arguments. %d required", num);
  }
}

void quizgrind_script_assert_argtype(
    lua_State* L,
    int arg,
    int type)
{
  if(lua_type(L, arg) != type)
  {
    luaL_error(L, "Argument %d is not a %s", arg, lua_typename(L, type));
  }
}

WJElement lua_pushJSON__get_element(
    lua_State* L,
    int idx)
{
  WJElement result;
  // Get WJElement base from metatable
  lua_getmetatable(L, idx);
  lua_pushstring(L, LUA_KEY_TABLEBASE);
  lua_gettable(L, -2);
  result = (WJElement) lua_topointer(L, -1);
  lua_pop(L, 2);
  return result;
}

int lua_pushJSON__len(
    lua_State* L)
{
  WJElement base = lua_pushJSON__get_element(L, 1);

  lua_pushnumber(L, base->count);
  
  return 1;
}

int lua_pushJSON__index(
    lua_State* L)
{
    WJElement base = lua_pushJSON__get_element(L, 1);
    
    switch(lua_type(L, 2))
    {
      case LUA_TNUMBER:
      {
        char key[13];
        int index = lua_tonumber(L, 2);
        g_snprintf(key, 13, "[%d]", index - 1);
        base = WJEGet(base, key, NULL);
        break;
      }
      case LUA_TSTRING:
      {
        char* index = (char*) lua_tostring(L, 2);
        base = WJEGet(base, index, NULL);
        break;
      }
      default: return 0;
    }
    
    // Lookup element
    lua_pushJSON(L, base);
    
    return 1;
}

int lua_pushJSON__next(
  lua_State* L)
{
  guint32 index = 1;
  WJElement base = lua_pushJSON__get_element(L, 1)->child;
  if(lua_type(L, 2) == LUA_TNUMBER)
  {
    for(int num = lua_tonumber(L, 2); num > 0; num --)
    {
      base = base->next;
      index ++;
    }
  }
  else if(lua_type(L, 2) == LUA_TSTRING)
  {
    while(strcmp(base->name, lua_tostring(L, 2)))
      { base = base->next; }
  }

  if(base != NULL)
  {
    if(base->name)
      { lua_pushstring(L, base->name); }
    else
      { lua_pushnumber(L, index); }
    lua_pushJSON(L, base);
  }
  else 
  {
    lua_pushnil(L);
    lua_pushnil(L);
  }
  return 2;
}

int lua_pushJSON__pairs(
    lua_State* L)
{
  lua_pushcfunction(L, lua_pushJSON__next);
  lua_pushvalue(L, 1);
  lua_pushnil(L);
  return 3;
}

void lua_pushJSON(
    lua_State* L,
    WJElement opt)
{
  if(!opt)
  {
    lua_pushnil(L);
    return;
  }
  
  switch(opt->type)
  {
  case WJR_TYPE_STRING:
    lua_pushstring(L, WJEString(opt, NULL, WJE_GET, NULL));
    break;
  case WJR_TYPE_TRUE:
  case WJR_TYPE_FALSE:
  case WJR_TYPE_BOOL:
    lua_pushboolean(L, WJEBool(opt, NULL, WJE_GET, 0));
    break;
  case WJR_TYPE_NUMBER:
    lua_pushnumber(L, WJEDouble(opt, NULL, WJE_GET, 0));
    break;
  case WJR_TYPE_ARRAY:
  case WJR_TYPE_OBJECT:
    // Create table
    lua_createtable(L, 0, 0);
    
    // Create metatable
    lua_createtable(L, 3, 0);
    
    // Set index function
    lua_pushstring(L, "__index");
    lua_pushcfunction(L, lua_pushJSON__index);
    lua_settable(L, -3);
    
    // Set len function
    lua_pushstring(L, "__len");
    lua_pushcfunction(L, lua_pushJSON__len);
    lua_settable(L, -3);

    // Set pairs function
    lua_pushstring(L, "__pairs");
    lua_pushcfunction(L, lua_pushJSON__pairs);
    lua_settable(L, -3);
    
    // Set table base
    lua_pushstring(L, LUA_KEY_TABLEBASE);
    lua_pushlightuserdata(L, opt);
    lua_settable(L, -3);
    
    // Set metatable
    lua_setmetatable(L, -2);
    
    break;
  case WJR_TYPE_NULL:
  default:
    lua_pushnil(L);
  }
}

quizgrind_scope_t* quizgrind_script_get_scope(
    lua_State* L)
{
  quizgrind_scope_t* result;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_KEY_SCOPE);
  result = (quizgrind_scope_t*) lua_topointer(L,-1);
  lua_pop(L, 1);
  return result;
}


quizgrind_exercise_ref_t* quizgrind_script_get_exercise_ref(
    lua_State* L)
{
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_KEY_EXERCISE);
  return ((quizgrind_exercise_ref_t*) lua_topointer(L,-1));
}

WJElement lua_toJSON(
    lua_State* L,
    WJElement src,
    int index,
    const char* oname)
{
  const char* name = NULL;
  WJElement elem = NULL;
  
  if(lua_getmetatable(L, index))
  {
    lua_pushstring(L, LUA_KEY_TABLEBASE);
    if(lua_gettable(L, -2) != LUA_TNIL)
    {
      elem = WJECopyDocument(elem, (WJElement) lua_topointer(L, -1), NULL, NULL);
    }
    lua_pop(L, 2);
  }
  
  // Copy widget info
  lua_pushnil(L);
  
  while (lua_next(L, index) != 0) {
    // uses 'key' (at index -2) and 'value' (at index -1)
    if(!lua_isnumber(L, -2))
      name = lua_tostring(L, -2);
    
    if(!elem)
    {
      if(lua_isnumber(L, -2))
      {
        elem = WJEArray(src, oname, WJE_NEW);
        name = "[$]";
      }
      else
      {
        elem = WJEObject(src, oname, WJE_NEW);
      }
    }
    int i;
    switch(i=lua_type(L, -1))
    {
    case LUA_TSTRING:
      WJEString(elem, name, WJE_SET, lua_tostring(L, -1));
      break;
    case LUA_TNUMBER:
      WJEDouble(elem, name, WJE_SET, lua_tonumber(L, -1));
      break;
    case LUA_TNIL:
      WJENull(elem, name, WJE_SET);
      break;
    case LUA_TBOOLEAN:
      WJEBool(elem, name, WJE_SET, lua_toboolean(L, -1));
      break;
    case LUA_TTABLE:
    {
      lua_toJSON(L, elem, lua_absindex(L, -1), name);
      break;
    }
    default:
      luaL_error(L, "Could not parse table.");
    }
    lua_pop(L, 1);
  }
  if(!elem) elem = WJEObject(src, NULL, WJE_NEW);
  return elem;
}

int quizgrind_script_fun_mkWidget(
    lua_State* L)
{
  quizgrind_script_assert_argnum(L, 1);
  quizgrind_script_assert_argtype(L, 1, LUA_TSTRING);
  
  quizgrind_scope_t* scope = quizgrind_script_get_scope(L);
  quizgrind_widget_interface_t* iface = quizgrind_widget_interface_get((char*)lua_tostring(L, 1));
  quizgrind_widget_t* widget = quizgrind_widget_create(scope, &scope->current_question->widgets, iface);

  if(!widget)
  {
    luaL_error(L, "Bad widget type %s!\n", lua_tostring(L, 1));
    return 0;
  }

  quizgrind_widget_t** wid = lua_newuserdata(L, sizeof(quizgrind_widget_t*));
  (*wid) = widget;
  
  /* If metatable for this type of widget has already been generated, grab it again */
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_KEY_WIDGETS);
  if(widget && widget->type && (lua_getfield(L, -1, widget->type->name) == LUA_TNIL))
  {
    lua_pop(L, 1);

    // Load the widget
    quizgrind_widget_setup_script(scope, L);

    lua_pushvalue(L, -1);
    lua_setfield(L, -3, widget->type->name);
  }

  lua_setmetatable(L, -3);
  lua_pop(L, 1);
  
  return 1;
}

int quizgrind_script_fun_mkHint(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 1);
  quizgrind_script_assert_argtype(L, 1, LUA_TSTRING);
  
  quizgrind_scope_t* scope = quizgrind_script_get_scope(L);
    
  WJEString(scope->current_question->hints, "[$]", WJE_NEW, lua_tostring(L, 1));
  return 0;
}

int quizgrind_script_fun_mkTemplate(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 2);
  quizgrind_script_assert_argtype(L, 1, LUA_TSTRING);
  quizgrind_script_assert_argtype(L, 2, LUA_TTABLE);
  
  quizgrind_scope_t* scope = quizgrind_script_get_scope(L);
  quizgrind_template_instance_t* template =
      quizgrind_template_instance(scope, (char*) lua_tostring(L, 1));

  if(!template)
    { return 0; }

  template->baseContext = lua_toJSON(L, NULL, 2, NULL);
  template->escape = NULL;
  template->provider = &WJEMustacheProvider;

  lua_pushlightuserdata(L, template);
  
  return 1;
}

int quizgrind_script_fun_number_format(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 3);
  quizgrind_script_assert_argtype(L, 1, LUA_TNUMBER);
  quizgrind_script_assert_argtype(L, 2, LUA_TNUMBER);
  quizgrind_script_assert_argtype(L, 3, LUA_TNUMBER);
  
  double num = lua_tonumber(L, 1);
  int dp = lua_tonumber(L, 2);
  guint32 type = lua_tonumber(L, 3);
  
  char buf[NUMBER_BUF_LEN];
  number_format(buf, NUMBER_BUF_LEN, num, dp, type);
  
  lua_pushstring(L, buf);
  return 1;
}

int quizgrind_script_fun_number_parse(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 1);
  quizgrind_script_assert_argtype(L, 1, LUA_TSTRING);
  
  char* parse = (char*)lua_tostring(L, 1);

  DOUBLERESULT value;
  DOUBLETYPE type = number_parse(&parse, &value);
  
  lua_newtable(L);
  
  lua_pushstring(L, DOUBLERESULT_TYPE);
  lua_pushnumber(L, type);
  lua_settable(L, -3);
  
  lua_pushstring(L, DOUBLERESULT_DP);
  lua_pushnumber(L, value.dp);
  lua_settable(L, -3);  
  
  lua_pushstring(L, DOUBLERESULT_SIGFIG);
  lua_pushnumber(L, value.sigfig);
  lua_settable(L, -3);

  lua_pushstring(L, DOUBLERESULT_VALUE);
  lua_pushnumber(L, value.value);
  lua_settable(L, -3);
  
  return 1;
}

int quizgrind_script_fun_number_type_toString(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 1);
  quizgrind_script_assert_argtype(L, 1, LUA_TNUMBER);
  
  lua_pushstring(L, quizgrind_enum_match_num(&number_num_type, lua_tonumber(L, 1)));
  return 1;
}

int quizgrind_script_fun_unirand(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 2);
  quizgrind_script_assert_argtype(L, 1, LUA_TUSERDATA);
  quizgrind_script_assert_argtype(L, 2, LUA_TNUMBER);
  
  guint32 at = lua_tonumber(L, 2);
  struct unirand_t* rand = (struct unirand_t*) lua_topointer(L, 1);
  guint32 result = unirand(rand, &at);
  
  lua_pushnumber(L, result);
  
  return 1;
}

int quizgrind_script_fun_unirand_seed(lua_State* L)
{
  quizgrind_script_assert_argnum(L, 1);
  quizgrind_script_assert_argtype(L, 1, LUA_TNUMBER);
  
  struct unirand_t* rand = lua_newuserdata(L, sizeof(struct unirand_t));
  unirand_seed(rand, lua_tonumber(L, 1));
  return 1;  
}

int quizgrind_script_dopanic(lua_State* L)
{
  printf("Lua internal panic!\n");
  return 0;
}

void quizgrind_script_init(quizgrind_project_t* project)
{
  lua_State* L = luaL_newstate();
  luaL_openlibs(L);
  
  lua_atpanic(L, quizgrind_script_dopanic);
  
  //Push C functions
  lua_pushcfunction(L, quizgrind_script_fun_mkWidget);
  lua_setglobal(L, FUN_CREATEWIDGET);

  lua_pushcfunction(L, quizgrind_script_fun_mkHint);
  lua_setglobal(L, FUN_CREATEHINT);
  
  lua_pushcfunction(L, quizgrind_script_fun_mkTemplate);
  lua_setglobal(L, FUN_MAKETEMPLATE);

  lua_pushcfunction(L, quizgrind_script_fun_number_format);
  lua_setglobal(L, FUN_NUMBER_FORMAT);
  
  lua_pushcfunction(L, quizgrind_script_fun_number_parse);
  lua_setglobal(L, FUN_NUMBER_PARSE);
  
  lua_pushcfunction(L, quizgrind_script_fun_unirand_seed);
  lua_setglobal(L, FUN_UNIRAND_SEED);
  
  lua_pushcfunction(L, quizgrind_script_fun_unirand);
  lua_setglobal(L, FUN_UNIRAND);
  
  lua_pushcfunction(L, quizgrind_script_fun_number_type_toString);
  lua_setglobal(L, FUN_NUMBER_TYPE_TOSTRING);

  // Push _CALC_TYPE
  quizgrind_enum_push_lua(L, &quizgrind_calc_type);
  lua_setglobal(L, GLOB_CALC_TYPE);
  
  // Push _NUM_TYPE
  quizgrind_enum_push_lua(L, &number_num_type);
  lua_setglobal(L, GLOB_NUM_TYPE);
  
  // Push _VARIABLE_MODE
  quizgrind_enum_push_lua(L, &quizgrind_variable_mode);
  lua_setglobal(L, GLOB_VARIABLE_MODE);

  //Load function table
  lua_newtable(L);
  lua_setfield(L, LUA_REGISTRYINDEX, LUA_KEY_SCRIPTS);
  
  //Start widget table
  lua_newtable(L);
  lua_setfield(L, LUA_REGISTRYINDEX, LUA_KEY_WIDGETS);
  
  project->L = L;
}

static gboolean lua_loadFile_(lua_State* L, char* path)
{
  if(luaL_loadfile (L, path) != LUA_OK)
  {
    printf("Error in file %s: %s\n",
           path, lua_tostring(L,-1));
    return FALSE;
  }
  return TRUE;
}

gboolean quizgrind_script_load(quizgrind_scope_t* scope, char* id, char* path)
{
  lua_State* L = scope->current_project->L;
  gboolean result = TRUE;
  
  //Load the function into the function table
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_KEY_SCRIPTS);
  
  result = lua_loadFile_(L, path);

  if(result)
    lua_setfield(L, -2, id);
  
  lua_pop(L, 1);
  return result;
}

gboolean quizgrind_script_run_init(quizgrind_scope_t* scope, quizgrind_exercise_ref_t* exref, char* path)
{
  gboolean result = TRUE;
  lua_State* L = scope->current_project->L;
  
  result = lua_loadFile_(L, path);
  if(result)
  {
    lua_pushJSON(L, exref->options);
    lua_setglobal(L, GLOB_OPTIONS);

    lua_pushJSON(L, exref->exercise->staticStore);
    lua_setglobal(L, GLOB_STATIC);
    
    // Push exercise globals
    lua_newtable(L);
    
    lua_pushstring(L, KEY_NAME);
    lua_pushstring(L, exref->exercise->baseName);
    lua_settable(L, -3);
    
    lua_pushstring(L, KEY_PREAMBLE);
    if(g_slist_length(exref->exercise->basePreamble) == 1)
    {
      lua_pushstring(L, exref->exercise->basePreamble->data);
    }
    else
    {
      lua_createtable(L, g_slist_length(exref->exercise->basePreamble), 0);
      GSList* ptr = exref->exercise->basePreamble;
      int idx = 1;
      while(ptr)
      {
        lua_pushnumber(L, idx);
        lua_pushstring(L, ptr->data);
        lua_settable(L, -3);
        ptr = g_slist_next(ptr);
        idx ++;
      }
    }
    lua_settable(L, -3);
    
    lua_pushstring(L, KEY_CALCULATOR);
    lua_pushnumber(L, exref->exercise->baseCalculator);
    lua_settable(L, -3);

    lua_pushstring(L, KEY_VARIABLE_MODE);
    lua_pushnumber(L, exref->exercise->baseVariable_mode);
    lua_settable(L, -3);
        
    lua_setglobal(L, GLOB_EXERCISE);
    
    // Push variable table
    quizgrind_variable_to_lua(L, exref->exercise->baseVariables);
    lua_setglobal(L, GLOB_VARIABLES);
  }
  
  if(lua_pcall(L, 0, LUA_MULTRET, 0) != LUA_OK)
  {
    printf("Initialization error in exercise %s: %s\n", exref->exercise->id, lua_tostring(L, -1));
    result = FALSE;
  }
  
  if(result)
  {
    // Populate variables
    lua_getglobal(L, GLOB_VARIABLES);
    exref->variables = quizgrind_variable_from_lua(L, -1, exref->exercise->id);

    // Load exercise globals
    lua_getglobal(L, GLOB_EXERCISE);
    
    const char* newstring = NULL;
    
    lua_pushstring(L, KEY_NAME);
    lua_gettable(L, -2);
    newstring = lua_tostring(L, -1);
    if(newstring)
      exref->name = g_strdup(newstring);
    lua_pop(L, 1);

    lua_pushstring(L, KEY_PREAMBLE);
    lua_gettable(L, -2);
    if(lua_type(L, -1) == LUA_TSTRING)
    {
      newstring = lua_tostring(L, -1);
      if(newstring)
        { exref->preamble = g_slist_prepend(exref->preamble, g_strdup(newstring)); }
    }
    else if(lua_type(L, -1) == LUA_TTABLE)
    {
      int table = lua_absindex(L, -1);
      lua_pushnil(L);
      while (lua_next(L, table) != 0)
      {
        // uses 'key' (at index -2) and 'value' (at index -1)
        newstring = lua_tostring(L, -1);
        if(newstring)
          { exref->preamble = g_slist_prepend(exref->preamble, g_strdup(newstring)); }
        lua_pop(L, 1);
      }
      exref->preamble = g_slist_reverse(exref->preamble);
    }
    lua_pop(L, 1);
    
    lua_pushstring(L, KEY_CALCULATOR);
    lua_gettable(L, -2);
    exref->calculator = lua_tonumber(L, -1);
    lua_pop(L, 1);

    lua_pushstring(L, KEY_VARIABLE_MODE);
    lua_gettable(L, -2);
    exref->variable_mode = lua_tonumber(L, -1);
    lua_pop(L, 1);
  }
  
  lua_pop(L, 1);
  return result;
}

gboolean quizgrind_script_unload(quizgrind_scope_t* scope, char* id)
{
  lua_State* L = scope->current_project->L;
  gboolean result = TRUE;
  
  //Free the function into the function table
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_KEY_SCRIPTS);
  lua_pushnil(L);
  lua_setfield(L, -2, id);
  lua_pop(L, 1);

  return result;
}

gboolean quizgrind_script_run_main(quizgrind_scope_t* scope, guint32 questionID)
{
  gboolean result = TRUE;
  lua_State* L = scope->current_project->L;

  //Push the scope (for C callbacks)
  lua_pushlightuserdata(L, scope);
  lua_setfield(L, LUA_REGISTRYINDEX, LUA_KEY_SCOPE);
  
  //Push question variables
  quizgrind_variable_render_lua(L, scope->current_exercise_ref->variables, scope->current_exercise_ref->variable_mode, questionID);
  lua_setglobal(L, GLOB_VARIABLES);

  lua_pushJSON(L, scope->current_exercise_ref->options);
  lua_setglobal(L, GLOB_OPTIONS);

  lua_pushJSON(L, scope->current_exercise->staticStore);
  lua_setglobal(L, GLOB_STATIC);
  
  // Seed with question ID
  srand(questionID);
  
  //Load function table and copy out the function
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_KEY_SCRIPTS);
  lua_getfield(L, -1, scope->current_exercise->scriptid);
  
  if(lua_pcall(L, 0, LUA_MULTRET,0) != LUA_OK)
  {
    printf("Lua Error in exercise %s: %s\n", scope->current_exercise->id, lua_tostring(L, -1));
    result = FALSE;
  }

  //Pop function table
  lua_pop(L, 1);
  return result;
}

void quizgrind_script_cleanup(quizgrind_project_t* project)
{
  lua_close(project->L);
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
