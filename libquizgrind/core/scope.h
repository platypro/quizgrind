/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_CORE_SCOPE_H_
#define LIBQUIZGRIND_CORE_SCOPE_H_

/* Quizgrind scope object.
 *
 * This structure stores pointers to all state which functions in
 * QuizGrind might want access to. Many functions take a reference
 * to a scope object alongside their usual arguments.
 */
typedef struct quizgrind_scope_t_
{
  struct quizgrind_project_t_* current_project;
  struct quizgrind_problem_set_t_* current_problem_set;
  struct quizgrind_quiz_t_* current_quiz;
  struct quizgrind_exercise_ref_t_* current_exercise_ref;
  struct quizgrind_exercise_t_* current_exercise;
  struct quizgrind_question_t_* current_question;
  struct quizgrind_widget_t_* current_widget;

} quizgrind_scope_t;

#define QUIZGRIND_SCOPE_SET_EXERCISE(scope, exercise) do { \
  quizgrind_exercise_t* _qg_scope_old_exercise = scope->current_exercise; \
  scope->current_exercise = exercise
#define QUIZGRIND_SCOPE_UNSET_EXERCISE(scope) scope->current_exercise = _qg_scope_old_exercise; } while(0);

#define QUIZGRIND_SCOPE_NEW(project) (quizgrind_scope_t){.current_project = project}

#endif /* LIBQUIZGRIND_CORE_SCOPE_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
