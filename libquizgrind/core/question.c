/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "keys.h"
#include "quizgrind.h"
#include "wjwriter.h"

quizgrind_question_t* quizgrind_question_next(
    struct quizgrind_scope_t_* scope)
{
  if(!scope->current_quiz)
    { return NULL; }

  quizgrind_question_unref(scope, scope->current_question);

  quizgrind_question_t* result;
  guint32 choice, questionID;

  // Pick the next exercise
  switch(scope->current_quiz->qtype)
  {
    case QUIZGRIND_GEN_MODE_RANDOM:
    {
      choice = (scope->current_quiz->set->numExercises > 1) ? g_random_int() % scope->current_quiz->set->numExercises : 0;
      quizgrind_exercise_ref_t* mark;
      mark = scope->current_exercise_ref = scope->current_quiz->set->exercises + choice;
      while(scope->current_quiz->randStatus[choice].at >= scope->current_exercise_ref->max)
      {
        if(scope->current_exercise_ref != (scope->current_quiz->set->exercises + scope->current_quiz->set->numExercises - 1))
          scope->current_exercise_ref++;
        else scope->current_exercise_ref = scope->current_quiz->set->exercises;

        if(mark == scope->current_exercise_ref) return NULL;
      }

      if(!scope->current_quiz->questionsLeft--) return NULL;

      break;
    }
    default:
      scope->current_exercise_ref = scope->current_quiz->currentExercise;
      if(scope->current_exercise_ref && scope->current_exercise_ref->numInstances
        && scope->current_quiz->sequenceID < scope->current_exercise_ref->numInstances
        && scope->current_quiz->sequenceID < scope->current_exercise_ref->max)
        scope->current_quiz->sequenceID++;
      else
      {
        scope->current_exercise_ref = scope->current_quiz->currentExercise = scope->current_quiz->set->exercises + unirand(&scope->current_quiz->ordered_rand, &scope->current_quiz->rand_at);
        if(scope->current_exercise_ref->numInstances > 1)
          scope->current_quiz->sequenceID = 1;
        else scope->current_quiz->sequenceID = 0;
        if(scope->current_quiz->rand_at > scope->current_quiz->set->numExercises)
          return NULL;
      }
      choice = scope->current_quiz->rand_at - 1;
      break;
  }
  scope->current_exercise = scope->current_exercise_ref->exercise;

  questionID = unirand_state(&scope->current_quiz->randStatus[choice]);

  result = calloc(1, sizeof(quizgrind_question_t));
  result->hints = WJEArray(NULL, NULL, WJE_NEW);
  result->eref = scope->current_exercise_ref;
  quizgrind_question_ref(result);
  scope->current_question = result;
  if(scope->current_exercise->hasScript && !quizgrind_script_run_main(scope, questionID))
  {
    quizgrind_question_unref(scope, scope->current_question);
    scope->current_question = NULL;
    return NULL;
  }
  if(!result->widgets.widget && !result->eref->exercise->baseWidgets.widget)
  {
    printf("Question %s has no widgets!!\n", scope->current_exercise_ref->exercise->id);
    quizgrind_question_unref(scope, scope->current_question);
    scope->current_question = NULL;
    return NULL;
  }

  return result;
}

gboolean quizgrind_question_ref(
    quizgrind_question_t* question)
{
  question->reference_count ++;
  return true;
}

gboolean quizgrind_question_unref(
    quizgrind_scope_t* scope, quizgrind_question_t* question)
{
  if(!question)
    { return false; }

  question->reference_count --;
  if(question->reference_count == 0)
  {
    WJECloseDocument(question->hints);
    quizgrind_widget_list_destroy(scope, &question->widgets);
    free(question);
  }

  return true;
}

gboolean quizgrind_question_write_json(
    struct quizgrind_scope_t_* scope,
    WJWriter w,
    char* at,
    quizgrind_widget_json_flags_t elements)
{
  quizgrind_widget_iter_t iter = {0};

  WJWOpenArray(at, w);
  quizgrind_widget_iter(&iter, scope);

  quizgrind_widget_t* widget;
  while((widget = quizgrind_widget_iter_next(&iter, scope)))
  {
    WJWOpenObject(NULL, w);
    WJWUInt32("id", widget->id, w);
    WJWString("type", widget->type->name, TRUE, w);

    quizgrind_widget_to_json(scope, w, KEYLIST(KEY_OPTION), elements);

    WJWCloseObject(w);
  }

  quizgrind_widget_iter_end(&iter, scope);

  WJWCloseArray(w);
  return true;
}

guint32 quizgrind_question_check_solutions_json(
    quizgrind_scope_t* scope,
    WJElement usolution_base,
    WJWriter out)
{
  guint32 result = 0;
  gboolean correct = TRUE;
  WJElement usolution;

  WJWOpenArray(KEY_MARKS, out);

  quizgrind_widget_iter_t iter = {0};
  quizgrind_widget_iter(&iter, scope);

  quizgrind_widget_t* widget;
  while((widget = quizgrind_widget_iter_next(&iter, scope)))
  {
    guint32 marks_max = quizgrind_widget_answer_max(scope);
    if(marks_max == 0)
      { continue; }

    WJWOpenObject(NULL, out);
    WJWUInt32(KEY_ID, widget->id, out);

    for(usolution = usolution_base->child; usolution; usolution = usolution->next)
    {
      if(widget->id == WJEUInt32(usolution, KEY_ID, WJE_GET, 0))
      {
        guint32 marks = quizgrind_widget_answer_check(scope,
          WJEString(usolution, KEY_KEY, WJE_GET, NULL));
        WJWUInt32(KEY_MARKS, marks, out);
        WJWUInt32(KEY_MARKS_MAX, marks_max, out);
        if(marks != marks_max)
          { correct = FALSE; }
      }
    }
    WJWCloseObject(out);
  }
  quizgrind_widget_iter_end(&iter, scope);

  WJWCloseArray(out);

  WJWBoolean(KEY_CORRECT, correct, out);

  return result;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
