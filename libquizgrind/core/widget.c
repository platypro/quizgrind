/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "quizgrind.h"

#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <math.h>

#include "widget/choice.h"
#include "widget/element.h"
#include "widget/image.h"
#include "widget/lines.h"

GHashTable* quizgrind_widget_index = NULL;

quizgrind_widget_interface_t* quizgrind_widget_interface_create(
    char* name,
    size_t struct_size)
{
  quizgrind_widget_interface_t* result = calloc(1, sizeof(quizgrind_widget_interface_t));
  result->name = g_strdup(name);
  result->size = struct_size;
  result->functions = g_hash_table_new(g_str_hash, g_str_equal);

  if(!quizgrind_widget_index)
    { quizgrind_widget_index = g_hash_table_new(g_str_hash, g_str_equal); }

  g_hash_table_replace(quizgrind_widget_index, name, result);

  return result;
}

void quizgrind_widget_interface_set_handler(
    quizgrind_widget_interface_t* iface,
    char* name,
    quizgrind_widget_function_t function)
{
  quizgrind_widget_function_container_t* function_container = malloc(sizeof(quizgrind_widget_function_container_t));
  function_container->function = function;
  g_hash_table_replace(iface->functions, name, function_container);
}

quizgrind_widget_function_t quizgrind_widget_interface_get_handler(
    quizgrind_widget_interface_t* iface,
    char* name)
{
  quizgrind_widget_function_container_t* function_container = g_hash_table_lookup(iface->functions, name);
  return (function_container ? function_container->function : NULL);
}

guint32 quizgrind_widget_interface_run_handler(
    quizgrind_widget_interface_t* iface,
    char* name,
    struct quizgrind_scope_t_* scope,
    gpointer data)
{
  quizgrind_widget_function_t function = quizgrind_widget_interface_get_handler(iface, name);
  return function ? function(scope, data) : 0;
}

quizgrind_widget_interface_t* quizgrind_widget_interface_get(
    const char* name)
{
  return (quizgrind_widget_interface_t*) g_hash_table_lookup(quizgrind_widget_index, name);
}

quizgrind_widget_interface_t* quizgrind_widget_interface_get_wjr(
    WJReader reader)
{
  char* name = WJRStringLoad(NULL, reader);
  quizgrind_widget_interface_t* result = quizgrind_widget_interface_get(name);
  free(name);
  return result;
}

#define QUIZGRIND_WIDGET_INTERFACE_ADD(name) do{\
	  quizgrind_widget_interface_t* iface = quizgrind_widget_interface_create(#name, sizeof(quizgrind_widget_## name ##_t)); \
	  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_INIT, quizgrind_widget_## name ##_init); \
	  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_CLEANUP, quizgrind_widget_## name ##_cleanup); \
	  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_FROM_JSON, quizgrind_widget_## name ##_from_json); \
	  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_TO_JSON, quizgrind_widget_## name ##_to_json); \
	  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_SETUP_SCRIPT, quizgrind_widget_## name ##_setup_script); \
	  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_ANSWER_MAX, quizgrind_widget_## name ##_answer_max); \
	  quizgrind_widget_interface_set_handler(iface, QUIZGRIND_WIDGET_FUN_ANSWER_CHECK, quizgrind_widget_## name ##_answer_check); \
    } while(0);

void quizgrind_widget_add_builtin()
{
  /* Return if table already exists */
  if(quizgrind_widget_index) { return; }

  QUIZGRIND_WIDGET_INTERFACE_ADD(element)
  QUIZGRIND_WIDGET_INTERFACE_ADD(choice)
  QUIZGRIND_WIDGET_INTERFACE_ADD(image)
  QUIZGRIND_WIDGET_INTERFACE_ADD(lines)
}

size_t quizgrind_widget_writeWJW(char *data, size_t length, void *userdata)
{
  WJWriter w = (WJWriter) userdata;
  return (WJWStringN(NULL, data, length, FALSE, w) ? length : 0);
}

guint32 quizgrind_widget_default_answer_max(quizgrind_scope_t* state, void* data)
  { return 0; }

guint32 quizgrind_widget_default_answer_check(
    struct quizgrind_scope_t_* state, void* data)
  { return quizgrind_widget_answer_max(state); }

guint32 quizgrind_widget_answer_check(quizgrind_scope_t* state, char* uans)
{
  if(!uans) return false;

  return quizgrind_widget_interface_run_handler(
      state->current_widget->type, QUIZGRIND_WIDGET_FUN_ANSWER_CHECK, state, uans);
}

guint32 quizgrind_widget_answer_max(quizgrind_scope_t* state)
{
  return quizgrind_widget_interface_run_handler(
      state->current_widget->type, QUIZGRIND_WIDGET_FUN_ANSWER_MAX, state, NULL);
}

gboolean quizgrind_widget_to_json(
    quizgrind_scope_t* state,
    WJWriter w,
    char* name,
    quizgrind_widget_json_flags_t elements)
{
  if(!state->current_widget) return false;

  quizgrind_widget_to_json_t to_json = {0};
  to_json.writer = w;
  to_json.flags = elements;

  WJWOpenObject(name, w);

  guint32 result = quizgrind_widget_interface_run_handler(
        state->current_widget->type, QUIZGRIND_WIDGET_FUN_TO_JSON, state, &to_json);

  WJWCloseObject(w);

  return result;
}

quizgrind_widget_t* quizgrind_widget_create(quizgrind_scope_t* state, quizgrind_widget_list_t* list, quizgrind_widget_interface_t* iface)
{
  if(iface == NULL)
    { return NULL; }

  quizgrind_widget_t* widget = calloc(1, iface->size);

  state->current_widget = widget;

  widget->type = iface;
  quizgrind_widget_interface_run_handler(
          iface, QUIZGRIND_WIDGET_FUN_INIT, state, NULL);

  //Generate ID
  widget->id = g_random_int();

  quizgrind_widget_list_append(list, widget);

  return widget;
}

void quizgrind_widget_list_append(
    quizgrind_widget_list_t* list,
    quizgrind_widget_t* widget)
{
  if(list->widget_end)
    { list->widget_end->next = widget; }
  else
    { list->widget = widget; }
  list->widget_end = widget;
}

void quizgrind_widget_from_json(
    struct quizgrind_scope_t_* state,
    WJElement element)
{
  quizgrind_widget_interface_run_handler(
          state->current_widget->type, QUIZGRIND_WIDGET_FUN_FROM_JSON, state, element);
}

quizgrind_widget_t* quizgrind_widget_set_type(quizgrind_widget_t* w, const char* type)
{
  //Get widget type
  w->type = quizgrind_widget_interface_get(type);
  
  if(!w->type)
  {
    free(w);
    return NULL;
  } 
  return w;
}

void quizgrind_widget_setup_script(quizgrind_scope_t* state, lua_State* L)
{
  quizgrind_widget_setup_script_t setup_script = {0};

  lua_createtable(L, 1, 0);

  setup_script.L = L;
  setup_script.idx = lua_absindex(L, -1);

  quizgrind_widget_interface_run_handler(
          state->current_widget->type, QUIZGRIND_WIDGET_FUN_SETUP_SCRIPT, state, &setup_script);
}

void quizgrind_widget_iter(
    quizgrind_widget_iter_t* iter,
    quizgrind_scope_t* scope)
{
  iter->baseWidgetAt = scope->current_exercise->baseWidgets.widget;
  iter->questionWidgetAt = scope->current_question->widgets.widget;
}

quizgrind_widget_t* quizgrind_widget_iter_next(
    quizgrind_widget_iter_t* iter,
    quizgrind_scope_t* scope)
{
  quizgrind_widget_t* result = NULL;
  if(iter->baseWidgetAt)
  {
    result = iter->baseWidgetAt;
    iter->baseWidgetAt = iter->baseWidgetAt->next;
  }
  else if(iter->questionWidgetAt)
  {
    result = iter->questionWidgetAt;
    iter->questionWidgetAt = iter->questionWidgetAt->next;
  }

  scope->current_widget = result;
  return result;
}

void quizgrind_widget_iter_end(
    quizgrind_widget_iter_t* iter,
    quizgrind_scope_t* scope)
{
  iter->baseWidgetAt = NULL;
  iter->questionWidgetAt = NULL;
  scope->current_widget = NULL;
}

void quizgrind_widget_list_destroy(
    quizgrind_scope_t* scope,
    quizgrind_widget_list_t* widgets)
{
  while(widgets->widget)
  {
    quizgrind_widget_t* widget_next = widgets->widget->next;
    scope->current_widget = widgets->widget;
    quizgrind_widget_interface_run_handler(widgets->widget->type, QUIZGRIND_WIDGET_FUN_CLEANUP, scope, NULL);
    scope->current_widget = NULL;
    free(widgets->widget);
    widgets->widget = widget_next;
  }
  widgets->widget_end = NULL;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
