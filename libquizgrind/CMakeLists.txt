#
# CMakeLists.txt file for QuizGrind Core Library
# (c) 2020 Aeden McClain
#

set(CMAKE_INSTALL_RPATH ${CMAKE_INSTALL_FULL_LIBDIR} ${CMAKE_INSTALL_PREFIX}/lib)

set(LIBQUIZGRIND_SRCS
  util/unirand.c
  util/number.c
  util/enum.c

  core/exercise.c
  core/exercise_ref.c
  core/problem_set.c
  core/question.c
  core/quiz.c
  core/template.c
  core/script.c
  core/project.c
  core/widget.c
  core/variable.c

  # Widget sources
  widget/choice.c
  widget/image.c
  widget/lines.c
  widget/element.c
  
  ${DEPS_SRC}
)

set(LIBQUIZGRIND_INCLUDE_DIRS
	${CMAKE_CURRENT_SOURCE_DIR}/..
	PARENT_SCOPE)

find_package(PkgConfig REQUIRED)
pkg_search_module(GLIB glib-2.0)

add_library(libquizgrind ${LIBQUIZGRIND_SRCS})
  
target_link_libraries(libquizgrind
  ${GLIB_LIBRARIES}
  ${DEPS_LIBS}
)

target_include_directories(libquizgrind PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${GLIB_INCLUDE_DIRS}
  ${DEPS_INCLUDE}
)

#Get version from git
find_package(Git)

set(VERSION_FMT_STRING "'--format=#define" "QUIZGRIND_VERSION" "\"Version" "%(describe)," "committed" "on" "%as.\"'")
add_custom_target(libquizgrind_vers_gen
  COMMAND ${GIT_EXECUTABLE} log -1 ${VERSION_FMT_STRING} > ${CMAKE_CURRENT_BINARY_DIR}/quizgrind_version.h
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/quizgrind_version.h)

add_dependencies(libquizgrind libquizgrind_vers_gen)

target_include_directories(libquizgrind PUBLIC
  ${CMAKE_CURRENT_BINARY_DIR})
