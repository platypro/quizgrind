/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INCLUDE_KEYS_H
#define INCLUDE_KEYS_H

// Commonly used keys
#define KEY_ID        "id"
#define KEY_KEY       "key"
#define KEY_UID       "uid"
#define KEY_WIDGET    "widget"
#define KEY_SOLUTION  "solution"
#define KEY_HINT      "hint"
#define KEY_TYPE      "type"

// Session keys
#define KEY_ACTION    "action"
#define KEY_PSET      "pset"
#define KEY_GIVEFIRST "giveFirst"
#define KEY_GIVEQSET  "giveQset"
#define KEY_GENMODE   "genMode"
#define KEY_MARKS     "marks"
#define KEY_MARKS_MAX "marks_max"
#define KEY_CORRECT   "correct"
#define KEY_ERROR     "error"

// Exercise keys
#define KEY_EXERCISE   "exercise"
#define KEY_NAME       "name"
#define KEY_QUESTION   "question"
#define KEY_CALCULATOR "calculator"
#define KEY_OPTION     "option"
#define KEY_STATIC     "static"
#define KEY_TEMPLATE   "template"
#define KEY_PATH       "path"
#define KEY_SUBS       "subs"
#define KEY_SVG        "svg"
#define KEY_IMAGE      "image"
#define KEY_VARIABLE   "variable"
#define KEY_PREAMBLE   "preamble"
#define KEY_VARIABLE_MODE "variable_mode"

// PSet keys
#define KEY_NUM_QUESTIONS "numQuestions"

#define KEYLIST(k) k "s"
#define KEYCOUNT(k) k "Count"

#endif /* INCLUDE_KEYS_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
