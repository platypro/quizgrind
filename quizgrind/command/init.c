/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "command/command.h"

#include <stdio.h>
#include <string.h>
#include <libquizgrind/quizgrind.h>

#include "args.h"

#define OPT_NAMESPACE 'n'

struct option_info quizgrind_cmd_init_opts[] = 
{
  {0, OPT_HELP, "help", NULL, "Show help and exit"},
  {ARGS_ARGUMENT, OPT_NAMESPACE, NULL, "NAME", "Namespace to use (i.e. com.example.asdf)"},
  {0}
};

typedef struct quizgrind_cmd_init_t_
{
  char* namespace;

} quizgrind_cmd_init_t;

gboolean quizgrind_cmd_init_parse_arg (int key, char *arg, gpointer data)
{
  quizgrind_cmd_init_t* optdata = data;
  switch(key)
  {
    case OPT_HELP:
    {
      puts("QuizGrind Project Initializer\n"
        "Usage: quizgrind init");
      args_show_short(quizgrind_cmd_init_opts);
      puts("\n\nOptions:");
      args_show_long(quizgrind_cmd_init_opts, 2);
      exit(0);
    }
    case OPT_NAMESPACE:
    {
      optdata->namespace = arg;
      break;
    }
  }
  return TRUE;
}

int quizgrind_cmd_init_run(gpointer data, int argc, char** argv)
{
  struct quizgrind_entry_t_* state = data;
  quizgrind_cmd_init_t optdata = {.namespace = NULL};
  args_parse(quizgrind_cmd_init_opts, quizgrind_cmd_init_parse_arg, &optdata, &argc, argv);
  
  path_push(&state->path_base, PROJECT_META);
  if(path_exists(&state->path_base))
  {
    printf("Project at %s already exists\n", path_get_cstr(&state->path_base));
    path_destroy(&state->path_base);
    return 0;
  }

  printf("Creating project at %s\n", path_get_cstr(&state->path_base));
  FILE* f = fopen(path_get_cstr(&state->path_base), "w");
  if(!f)
  {
    free(path_get_cstr(&state->path_base));
    path_destroy(&state->path_base);
    return 1;
  }
  path_pop(&state->path_base);

  WJWriter w = WJWOpenFILEDocument(true, f);
  WJWOpenObject(NULL, w);
  WJWString(KEY_NAMESPACE, optdata.namespace ? optdata.namespace : "", TRUE, w);
  WJWCloseObject(w);
  WJWCloseDocument(w);
  fclose(f);

  path_push(&state->path_base, PROJECT_EXERCISE);
  path_mkdir(&state->path_base);
  path_pop(&state->path_base);

  path_push(&state->path_base, PROJECT_PSET);
  path_mkdir(&state->path_base);
  path_pop(&state->path_base);

  return 0;
}

void quizgrind_cmd_init_stop()
  { return; }

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
