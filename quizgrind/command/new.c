/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "command/command.h"

#include <stdio.h>
#include <string.h>
#include <pathbuf.h>
#include <libquizgrind/quizgrind.h>

#include "args.h"

#define OPT_PSET 'p'
#define OPT_PSET_ "pset"

#define OPT_CALCULATOR 'c'
#define OPT_CALCULATOR_ "calculator"
#define OPT_CALCULATOR_DEFAULT "none"

#define OPT_NAME 'n'
#define OPT_NAME_ "name"

#define OPT_DESCRIPTION 'd'
#define OPT_DESCRIPTION_ "description"

#define OPT_TEMPLATE 't'
#define OPT_TEMPLATE_DEFAULT "basic"

int quizgrind_cmd_new_help_run(gpointer data, int argc, char** argv);
int quizgrind_cmd_new_exercise_run(gpointer data, int argc, char** argv);
int quizgrind_cmd_new_pset_run(gpointer data, int argc, char** argv);

struct command quizgrind_cmd_new_commands[] = 
{
  {.name = "--help", .hint = "View help", .fn = quizgrind_cmd_new_help_run, .abort = NULL},
  {.name = "exercise", .hint = "Create a new exercise", 
    .fn = quizgrind_cmd_new_exercise_run, .abort = NULL},
  {.name = "pset", .hint = "Create a new problem set",
    .fn = quizgrind_cmd_new_pset_run, .abort = NULL},
  {0}
};

struct option_info quizgrind_cmd_new_exercise_opts[];
struct option_info quizgrind_cmd_new_pset_opts[];

int quizgrind_cmd_new_help_run(gpointer data, int argc, char** argv)
{
  puts("QuizGrind Creator\n"
    "Usage: quizgrind new <command> <id> [<opts>]\n\n"
    "Commands available:");
  args_show_commands(quizgrind_cmd_new_commands, 2);
  puts("The exercise command has the following options:");
  args_show_long(quizgrind_cmd_new_exercise_opts, 2);
  puts("The pset command has the following options:");
  args_show_long(quizgrind_cmd_new_pset_opts, 2);
  return 0;
}

struct option_info quizgrind_cmd_new_exercise_opts[] = 
{
  {ARGS_ARGUMENT, OPT_PSET, NULL, "PSET", "Add to problem set"},
  {ARGS_ARGUMENT, OPT_TEMPLATE, NULL, "TEMPLATE", "Template to use, default: basic"},
  {ARGS_ARGUMENT, OPT_CALCULATOR, NULL, "CALCULATOR", "Calculator type (none|basic|scientific|graphing), default: none"},
  {ARGS_ARGUMENT, OPT_NAME, NULL, "NAME", "Name of exercise"},
  {ARGS_ARGUMENT, OPT_DESCRIPTION, NULL, "DESC", "Exercise Description"},
  {0}
};

struct quizgrind_cmd_new_exercise_t_ {
  char* name;
  char* template;
  char* calculator;
  char* pset;
  char* description;

};

gboolean quizgrind_cmd_new_exercise_parse_arg (int key, char *arg, gpointer data)
{
  struct quizgrind_cmd_new_exercise_t_* exercise = data;
  switch(key)
  {
    case OPT_PSET:
      exercise->pset = arg;
      break;
    case OPT_CALCULATOR:
      exercise->calculator = arg;
      break;
    case OPT_TEMPLATE:
      exercise->template = arg;
      break;
    case OPT_NAME:
      exercise->name = arg;
      break;
    case OPT_DESCRIPTION:
      exercise->description = arg;
      break;
    default: return FALSE;
  }
  return TRUE;
}

PMUS_OBJECT quizgrind_cmd_new_exercise_callback_get(gpointer base, char* name)
{
  struct quizgrind_cmd_new_exercise_t_* exercise = base;
  PMUS_OBJECT result = {0};
  result.type = PMUS_TYPE_STRING;
  if(!strcmp(name, OPT_CALCULATOR_))
    result.stringValue = exercise->calculator;
  else if(!strcmp(name, OPT_DESCRIPTION_))
    result.stringValue = exercise->description;
  else if(!strcmp(name, OPT_NAME_))
    result.stringValue = exercise->name;
    
  if(!result.stringValue) result.type = PMUS_TYPE_INVALID;
  return result;
}

size_t quizgrind_cmd_new_exercise_callback_write(char *data, size_t length, void *userdata)
{
  return fwrite(data, sizeof(guint8), length, (FILE*)userdata);
}

int quizgrind_cmd_new_exercise_run(gpointer data, int argc, char** argv)
{
  struct quizgrind_entry_t_* state = data;
  quizgrind_project_t* project = quizgrind_project_load(state->path_base);
  if(!project)
    { return 0; }

  struct quizgrind_cmd_new_exercise_t_ exercise = { 0 };
  char* exid = NULL;
  if(!(argv = args_parse(quizgrind_cmd_new_exercise_opts, quizgrind_cmd_new_exercise_parse_arg, &exercise, &argc, argv)))
  {
    printf("Missing exercise name!");
    return 0;
  }

  exid = *argv;
  path_t* exercise_path = quizgrind_project_get_path_exercise_ns(project, exid);
  if(path_exists(&exercise_path))
  {
    printf("Exercise '%s' already exists!\n", exid);
    path_destroy(&exercise_path);
    quizgrind_project_cleanup(project);
    return 0;
  }
  
  // Parse the last of the args
  if((argv = args_parse(quizgrind_cmd_new_exercise_opts, quizgrind_cmd_new_exercise_parse_arg, &exercise, &argc, argv)))
  {
    printf("Invalid argument '%s'\n", *argv);
    quizgrind_project_cleanup(project);
    return 0;
  }

  if(!exercise.name)
    exercise.name = exid;
  if(!exercise.calculator)
    exercise.calculator = OPT_CALCULATOR_DEFAULT;
  if(!exercise.template) 
    exercise.template = OPT_TEMPLATE_DEFAULT;

  path_t* template_path = path_new_from_str(QUIZGRIND_DATA_PATH, 0);
  path_push(&template_path, "template");
  path_push(&template_path, exercise.template);
  if(!path_exists(&template_path))
  {
    printf("Template at %s is invalid!\n", path_get_cstr(&template_path));
    return 0;
  }

  path_mkdir(&exercise_path);

  path_iterator_t* iter = path_iter(&template_path);
  path_t* subpath;
  while((subpath = path_iter_next(&iter)))
  {
    path_push(&exercise_path, path_get_basename(&subpath));

    char* file = mustache_eatFile(path_get_cstr(&subpath));
    PMUS_PROVIDER provider = {
      .getValue = quizgrind_cmd_new_exercise_callback_get,
      .changeContext = NULL
    };

    PMUS_BUILDER builder = {
      .template = mustache_mkIndex(file, 0),
      .provider = &provider,
      .escape = NULL,
      .baseContext = &exercise
    };

    FILE* f = fopen(path_get_cstr(&exercise_path), "w");
    mustache_generate(&builder, quizgrind_cmd_new_exercise_callback_write, f);
    fclose(f);

    mustache_destroyIndex(builder.template);
    free(file);
    path_pop(&exercise_path);
  }

  printf("Exercise '%s' created. \n", exid);

  if(exercise.pset)
  {
    path_t* pset_path = quizgrind_project_get_path_pset(project, exercise.pset);
    if(pset_path)
    {
      FILE* f = fopen(path_get_cstr(&pset_path), "r");
      WJReader r = WJROpenFILEDocument(f, NULL, 0);
      WJElement pset = WJEOpenDocument(r, NULL, NULL, NULL);
      WJRCloseDocument(r);
      fclose(f);

      f = fopen(path_get_cstr(&pset_path), "w");
      WJElement obj = WJEObject(pset, KEYLIST(KEY_QUESTION) "[$]", WJE_NEW);
      WJEString(obj, KEY_ID, WJE_NEW, path_get_basename(&exercise_path));
      WJEArray(obj, KEYLIST(KEY_OPTION), WJE_NEW);
      WJEWriteFILE(pset, f);
      fclose(f);
    }
    else
      { printf("Problem set '%s' does not exist!\n", exercise.pset); }
  }

  path_destroy(&exercise_path);
  quizgrind_project_cleanup(project);

  return 0;
}

struct option_info quizgrind_cmd_new_pset_opts[] = 
{
  {ARGS_ARGUMENT, OPT_NAME, NULL, "NAME", "Name of problem set"},
  {0}
};

struct quizgrind_cmd_new_pset_t_ {
  char* name;
};

gboolean quizgrind_cmd_new_pset_parse_arg (int key, char *arg, gpointer data)
{
  struct quizgrind_cmd_new_pset_t_* pset = data;
  switch(key)
  {
    case OPT_NAME:
      pset->name = arg;
      break;
  }
  return TRUE;
}

int quizgrind_cmd_new_pset_run(gpointer data, int argc, char** argv)
{
  struct quizgrind_entry_t_* state = data;
  quizgrind_project_t* project = quizgrind_project_load(state->path_base);
  if(!project)
  {
    return 0;
  }
  struct quizgrind_cmd_new_pset_t_ pset;
  
  argv = args_parse(quizgrind_cmd_new_pset_opts, quizgrind_cmd_new_pset_parse_arg, &pset, &argc, argv);
  if(!argv)
  {
    puts("Missing problem set name!"); 
    quizgrind_project_cleanup(project);
    return 0;
  }
  
  char* exid = *argv;
  
  path_t* path = quizgrind_project_get_path_pset(project, exid);
  if(!path)
  {
    printf("Problem Set '%s' already exists!\n", exid);
    quizgrind_project_cleanup(project);
    path_destroy(&path);
    return 0;
  }

  FILE* f = fopen(path_get_cstr(&path), "w");
  WJWriter w = WJWOpenFILEDocument(true, f);
  pset.name = exid;

  // Get any last options
  args_parse(quizgrind_cmd_new_pset_opts, quizgrind_cmd_new_pset_parse_arg, &pset, &argc, argv);
  WJWOpenObject(NULL, w);
  WJWString(KEY_NAME, pset.name, TRUE, w);
  WJWOpenArray(KEYLIST(KEY_QUESTION), w);
  WJWCloseArray(w);
  WJWCloseObject(w);
  WJWCloseDocument(w);
  fclose(f);

  printf("Problem set '%s' created\n", exid);

  path_destroy(&path);
  quizgrind_project_cleanup(project);
  return 0;
}

int quizgrind_cmd_new_run(gpointer state, int argc, char** argv)
{
  argv++;
  argc--;
  if(argc)
  {
    if(args_runCommand(quizgrind_cmd_new_commands, argv, argc, state, NULL) != 0)
       printf("Invalid subcommand %s\n", *argv);
    return 0;
  }
  
  puts("Missing argument!\n");
  return 0;
}

void quizgrind_cmd_new_stop()
  { return; }


/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
