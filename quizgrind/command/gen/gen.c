/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "command/command.h"

#include <string.h>
#include <libquizgrind/quizgrind.h>
#if _QUIZGRIND_BUILD_PDF
#include <libquizgrind_pdf/quizgrind_pdf.h>
#endif
#include "args.h"
#include "command/gen/gen.h"
#include "enum/gen_mode.h"

#define OPT_OUTPUT_SFILE  's'
#define OPT_OUTPUT_TYPE   't'
#define OPT_INPUT_GENMODE 'g'
#define OPT_INPUT_NUM     'n'
#if _QUIZGRIND_BUILD_PDF
  #include <libquizgrind_pdf/quizgrind_pdf.h>

  #define OPT_PDF_HEADERS ARGS_LONGONLY(1)
  #define OPT_PDF_MARGIN  ARGS_LONGONLY(2)
  #define OPT_PDF_WIDTH   ARGS_LONGONLY(3)
  #define OPT_PDF_HEIGHT  ARGS_LONGONLY(4)
#endif /* _QUIZGRIND_BUILD_PDF */

struct option_info quizgrind_cmd_gen_opts[] = 
{
  {0, OPT_HELP, "help", NULL, "Show help and exit"},
// File options
  {ARGS_ARGUMENT, OPT_OUTPUT_SFILE,  NULL, "FILE", 
    "Solution output. If not set, added to regular output file."},
  {ARGS_ARGUMENT, OPT_OUTPUT_TYPE,   NULL, "TYPE", "Output type (auto, json" 
#ifdef _QUIZGRIND_BUILD_PDF
", pdf" 
#endif
    ")"},
  {ARGS_ARGUMENT, OPT_INPUT_GENMODE, NULL, "MODE",
    "Question generation type (random, ordered, unordered)"},
  {ARGS_ARGUMENT, OPT_INPUT_NUM,     NULL, "NUM",  "Number of questions"},
#ifdef _QUIZGRIND_BUILD_PDF
// PDF options
  {ARGS_ARGUMENT, OPT_PDF_MARGIN,  "pdf-margin" , "SIZE", "PDF margin (in centimeters)"},
  {ARGS_ARGUMENT, OPT_PDF_WIDTH,   "pdf-width"  , "WIDTH", "PDF Page width (in centimeters)"},
  {ARGS_ARGUMENT, OPT_PDF_HEIGHT,  "pdf-height" , "HEIGHT", "PDF Page height (in centimeters)"},
  {ARGS_ARGUMENT, OPT_PDF_HEADERS, "pdf-headers", "HEADERS", "PDF Header fields (comma seperated with semicolon between columns)."},
#endif /* _QUIZGRIND_BUILD_PDF */
  {0}
};

gboolean quizgrind_cmd_gen_json(quizgrind_cmd_gen_t* opts, quizgrind_project_t* project, char* pset)
{
  quizgrind_scope_t scope = {0};
  scope.current_project = project;

  quizgrind_quiz_next(&scope, opts->pset, opts->mode, opts->num);
  if(!scope.current_quiz) return false;

  quizgrind_question_t* q = NULL;

  FILE *sf = NULL, *f = fopen(path_get_cstr(&opts->file), "w");
  if(!f) return false;
  WJWriter writer = WJWOpenFILEDocument(true, f);
  WJWriter solwriter = writer;

  if(opts->sfile)
  {
    sf = fopen(path_get_cstr(&opts->sfile), "w");
    if(!sf) return false;
    solwriter = WJWOpenFILEDocument(true, sf);
  }
  
  WJWOpenArray(NULL, writer);
  while((q = quizgrind_question_next(&scope)))
  {
    WJWOpenObject(NULL, writer);
    WJWString(KEY_CALCULATOR, quizgrind_exercise_ref_get_calc_type(q->eref), true, writer);
    WJWString(KEY_NAME, q->eref->name, true, writer);
    if(opts->sfile)
    {
      quizgrind_question_write_json(&scope, writer, KEYLIST(KEY_WIDGET), QUIZGRIND_WIDGET_JSON_QUESTION | QUIZGRIND_WIDGET_JSON_FULLPATH);
      quizgrind_question_write_json(&scope, solwriter, KEYLIST(KEY_SOLUTION), QUIZGRIND_WIDGET_JSON_SOLUTION | QUIZGRIND_WIDGET_JSON_FULLPATH);
    }
    else
    {
      quizgrind_question_write_json(&scope, writer, KEYLIST(KEY_WIDGET), QUIZGRIND_WIDGET_JSON_QUESTION | QUIZGRIND_WIDGET_JSON_SOLUTION | QUIZGRIND_WIDGET_JSON_FULLPATH);
    }
    
    WJEWriteDocument(q->hints, writer, KEYLIST(KEY_HINT));
    WJWCloseObject(writer);
  }
  WJWCloseArray(writer);
  WJWCloseDocument(writer);
  if(sf) fclose(sf);
  fclose(f);
  return true;
}

void quizgrind_cmd_gen_show_help()
{
  printf("QuizGrind Generator\nUsage: quizgrind gen <pset> <file> ");
  args_show_short(quizgrind_cmd_gen_opts);
  puts("\n\nOptions:");
  args_show_long(quizgrind_cmd_gen_opts, 2);
}

gboolean quizgrind_cmd_gen_parse_arg(int id, char* arg, gpointer udata)
{
  quizgrind_cmd_gen_t* gen = udata;
  switch(id)
  {
    case OPT_HELP:
      quizgrind_cmd_gen_show_help();
      exit(0);
      break;
    case OPT_OUTPUT_SFILE:
      gen->sfile = path_new_from_str(arg, 0);
#ifdef _QUIZGRIND_BUILD_PDF
      gen->pdf.sfile = gen->sfile;
#endif
      break;
    case OPT_INPUT_GENMODE:
      gen->mode = quizgrind_enum_match_str(&quizgrind_gen_mode, arg);
#ifdef _QUIZGRIND_BUILD_PDF
      gen->pdf.gen_mode = gen->mode;
#endif
      break;
    case OPT_OUTPUT_TYPE:
      if(!strcmp(arg, "auto"))
        gen->type = QUIZGRIND_CMD_GEN_TYPE_AUTO;
      if(!strcmp(arg, "json"))
        gen->type = QUIZGRIND_CMD_GEN_TYPE_JSON;
#ifdef _QUIZGRIND_BUILD_PDF
      if(!strcmp(arg, "pdf"))
        gen->type = QUIZGRIND_CMD_GEN_TYPE_PDF;
#endif
      break;
    case OPT_INPUT_NUM:
      gen->num = strtoul(arg, 0, 0);
#ifdef _QUIZGRIND_BUILD_PDF
      gen->pdf.gen_num = gen->num;
#endif
      break;
#ifdef _QUIZGRIND_BUILD_PDF
    case OPT_PDF_HEADERS:
      gen->pdf.headers = arg;
      break;
    case OPT_PDF_HEIGHT:
      gen->pdf.pageHeight = strtod(arg, NULL);
      break;
    case OPT_PDF_WIDTH:
      gen->pdf.pageWidth = strtod(arg, NULL);
      break;
    case OPT_PDF_MARGIN:
      gen->pdf.margin = strtod(arg, NULL);
      break;
#endif /* _QUIZGRIND_BUILD_PDF */
    default: return false;
  }
  return true;
}

int quizgrind_cmd_gen_run(gpointer data, int argc, char** argv)
{
  #define do_handle_file_error(msg) { printf(msg); goto handle_file_error; }
  int result = 0;
  struct quizgrind_entry_t_* state = data;
  quizgrind_project_t* project = NULL;

  quizgrind_cmd_gen_t gen = {0};
  gen.type         = QUIZGRIND_CMD_GEN_TYPE_AUTO;
  gen.num          = 10;
  gen.mode         = QUIZGRIND_GEN_MODE_ORDERED;
  
#ifdef _QUIZGRIND_BUILD_PDF
  gen.pdf.gen_num  = gen.num;
  gen.pdf.gen_mode = gen.mode;
  gen.pdf.headers = QUIZGRIND_PDF_DEFAULT_HEADERS;
  gen.pdf.margin  = QUIZGRIND_PDF_DEFAULT_MARGIN;
  gen.pdf.pageHeight = QUIZGRIND_PDF_DEFAULT_PAGE_HEIGHT;
  gen.pdf.pageWidth  = QUIZGRIND_PDF_DEFAULT_PAGE_WIDTH;
#endif /* _QUIZGRIND_BUILD_PDF */  

  argv = args_parse(quizgrind_cmd_gen_opts,
            quizgrind_cmd_gen_parse_arg,
            state,
            &argc, argv);
  
  if(argc)
  {
    gen.pset = *argv;
#ifdef _QUIZGRIND_BUILD_PDF
    gen.pdf.problem_set = *argv;
#endif
  } else do_handle_file_error("Missing problem set!!\n");
  
  argv = args_parse(quizgrind_cmd_gen_opts,
            quizgrind_cmd_gen_parse_arg,
            state,
            &argc, argv);

  project = quizgrind_project_load(state->path_base);

  if(!project)
    { return 1; }

  if(argc)
  {
    gen.file = path_new_from_str(*argv, 0);
  } else do_handle_file_error("Missing file!\n");    

  if(!gen.file || !gen.pset) return 1;
    
  if(gen.type == QUIZGRIND_CMD_GEN_TYPE_AUTO)
  {
    gen.type = QUIZGRIND_CMD_GEN_TYPE_JSON;
#ifdef _QUIZGRIND_BUILD_PDF
    if(!strcmp(path_extension_get(&gen.file), "pdf"))
    {
      gen.type = QUIZGRIND_CMD_GEN_TYPE_PDF;
    }
#endif
  }
#ifdef _QUIZGRIND_BUILD_PDF
  gen.pdf.file = gen.file;
  gen.pdf.sfile = gen.sfile;
#endif

  switch(gen.type)
  {
    case QUIZGRIND_CMD_GEN_TYPE_JSON:
      result = quizgrind_cmd_gen_json(&gen, project, gen.pset);
      break;
#ifdef _QUIZGRIND_BUILD_PDF
    case QUIZGRIND_CMD_GEN_TYPE_PDF:
      result = quizgrind_pdf_gen(project, &gen.pdf);
      break;
#endif
    default: do_handle_file_error("Invalid type")
  }
  
  quizgrind_project_cleanup(project);
  path_destroy(&gen.file);
  if(gen.sfile)
    { path_destroy(&gen.sfile); }
  return result;
  #undef do_handle_file_error
handle_file_error:
  quizgrind_cmd_gen_show_help();
  quizgrind_project_cleanup(project);
  return false;  
}

void quizgrind_cmd_gen_stop()
  { return; }

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
