/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INCLUDE_GEN_H
#define INCLUDE_GEN_H

#ifdef _QUIZGRIND_BUILD_PDF
#include <libquizgrind_pdf/quizgrind_pdf.h>
#endif
#include <quizgrind.h>

typedef enum quizgrind_cmd_gen_type_t_
{
  QUIZGRIND_CMD_GEN_TYPE_AUTO,
  QUIZGRIND_CMD_GEN_TYPE_JSON,
#ifdef _QUIZGRIND_BUILD_PDF
  QUIZGRIND_CMD_GEN_TYPE_PDF
#endif
  
} quizgrind_cmd_gen_type_t;

typedef struct quizgrind_cmd_gen_t_
{
#ifdef _QUIZGRIND_BUILD_PDF
  quizgrind_pdf_t pdf;
#endif

  path_t* file;
  path_t* sfile;
  char* pset;
  quizgrind_cmd_gen_type_t type;
  quizgrind_gen_mode_t mode;
  
  guint32 num;

} quizgrind_cmd_gen_t;

extern int quizgrind_cmd_gen_run(gpointer state, int argc, char** argv);
extern void quizgrind_cmd_gen_stop();

#endif /* INCLUDE_GEN_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
