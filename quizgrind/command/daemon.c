/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "command/command.h"

#include <unistd.h>
#include <glib.h>
#include <libquizgrind/quizgrind.h>
#include <libquizgrind_http/quizgrind_http.h>

#include "command/command.h"
#include "args.h"
#include "core/project.h"

#define OPT_HELP    ARGS_LONGONLY(0)
#define OPT_PORT    'p'
#define OPT_ADDRESS 'a'
#define OPT_BROWSER 'b'
#define OPT_HTMLTEMPLATE  ARGS_LONGONLY(1)
#define QUIZGRIND_DAEMON_TYPE_HTTP (1<<0)

quizgrind_daemon_http_t* daemon_running = NULL;
guint8 daemon_type = 0;

struct option_info quizgrind_cmd_daemon_opts[] = 
{
  {0, OPT_HELP, "help", NULL, "Show help and exit"},
  {0, OPT_BROWSER, "browser", NULL, "Open QuizGrind in your default web browser."},
  {ARGS_ARGUMENT, OPT_PORT, "port", "portnum", "Port to host on (Default 1245)"},
  {ARGS_ARGUMENT, OPT_ADDRESS, "address", "ip", "Address to host on (default any)"},
  {ARGS_ARGUMENT, OPT_HTMLTEMPLATE, "html-template", "path", "Path to html template for web"},
  {0}
};

gboolean quizgrind_cmd_daemon_parse_arg(int id, char* arg, gpointer init_)
{
  quizgrind_daemon_http_init_t* init = (quizgrind_daemon_http_init_t*) init_;
  if(id == OPT_HELP)
  {
    printf("QuizGrind HTTP Daemon\n"
     "Usage: quizgrind daemon ");
    args_show_short(quizgrind_cmd_daemon_opts);
    puts("\n");
    args_show_long(quizgrind_cmd_daemon_opts, 2);
    exit(0);
  }
  else if(id == OPT_PORT)
  {
    init->port = atoi(arg);
  }
  else if(id == OPT_ADDRESS)
  {
    init->address = arg;
  }
  else if (id == OPT_BROWSER)
  {
    init->open_browser = true;
  }
  else if (id == OPT_HTMLTEMPLATE)
  {
    init->html_template = arg;
  }
  return TRUE;
}

void quizgrind_cmd_daemon_stop()
{
  if(!daemon_running)
    { return; }

  if(daemon_type & QUIZGRIND_DAEMON_TYPE_HTTP)
    quizgrind_daemon_http_stop(daemon_running);

  quizgrind_project_cleanup(daemon_running->init.project);
}

int quizgrind_cmd_daemon_run(gpointer data, int argc, char** argv)
{
  struct quizgrind_entry_t_* state = data;
  quizgrind_daemon_http_t daemon = {0};

  quizgrind_daemon_http_init_t init = {0};

  init.address = "0.0.0.0";
  init.port = QUIZGRIND_DAEMON_HTTP_DEFAULT_PORT;

  args_parse(quizgrind_cmd_daemon_opts,
             quizgrind_cmd_daemon_parse_arg,
             &init,
             &argc, argv);

  quizgrind_project_t* project = quizgrind_project_load(state->path_base);

  daemon_type = QUIZGRIND_DAEMON_TYPE_HTTP;

  init.project = project;

  if(!project)
    { return 1; }

  printf("Starting Daemon\n");
  fflush(stdout);

  daemon_running = &daemon;
  quizgrind_daemon_http_start(&daemon, &init);
  quizgrind_daemon_http_join(&daemon);
  quizgrind_daemon_http_stop(&daemon);

  return 0;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
