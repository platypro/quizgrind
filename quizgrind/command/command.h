/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef LIBQUIZGRIND_COMMAND_COMMAND_H_
#define LIBQUIZGRIND_COMMAND_COMMAND_H_

#include <pathbuf.h>

#include <glib.h>

extern char* QUIZGRIND_DATA_PATH;

extern int quizgrind_cmd_init_run(gpointer state, int argc, char** argv);
extern void quizgrind_cmd_init_stop();

extern int quizgrind_cmd_new_run(gpointer state, int argc, char** argv);
extern void quizgrind_cmd_new_stop();

extern int quizgrind_cmd_daemon_run(gpointer state, int argc, char** argv);
extern void quizgrind_cmd_daemon_stop();

struct quizgrind_entry_t_
{
  path_t* path_base;

};

#endif /* LIBQUIZGRIND_COMMAND_COMMAND_H_ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
