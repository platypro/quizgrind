/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#ifndef INCLUDE_ARGS_H
#define INCLUDE_ARGS_H

#define ARGS_LONGONLY(opt) (opt + 128)

#define ARGS_ARGUMENT 1
#define ARGS_OPTIONAL 2

#define OPT_HELP    ARGS_LONGONLY(0)

#define ARGS_ALIGNAT 22

#include <glib.h>

typedef int (*command_fn)(gpointer, int argc, char** argv);
typedef void (*command_abort)();

struct option_info
{
  guint32 flags;
  int id;
  char* name;
  char* argname;
  char* description;
}; 

struct command
{
  char* name;
  char* hint;
  command_fn fn;
  command_abort abort;
};

typedef gboolean (*args_parser) (int id, char* arg, gpointer udata);

extern void args_show_commands(struct command* commands, int indent);
extern void args_show_long(struct option_info* opt_info, int indent);
extern void args_show_short(struct option_info* opt_info);

extern char** args_parse(struct option_info* opts, 
                 args_parser parser, 
                 gpointer udata, 
                 int* argc, char** argv);

extern gboolean args_runCommand(struct command* commands,
                            char** arg, int argc, gpointer udata, command_abort* abort);

#endif /* INCLUDE_ARGS_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
