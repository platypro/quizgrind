set(QUIZGRIND_FRONTEND_SRCS
  quizgrind.c
  args.c

  command/init.c
  command/new.c
  command/gen/gen.c)

if(QUIZGRIND_BUILD_DAEMON)
  set(QUIZGRIND_FRONTEND_SRCS ${QUIZGRIND_FRONTEND_SRCS} command/daemon.c)
endif()

add_executable(quizgrind ${QUIZGRIND_FRONTEND_SRCS})

install(TARGETS quizgrind DESTINATION ${CMAKE_INSTALL_FULL_BINDIR})

if(QUIZGRIND_BUILD_DAEMON)
  add_definitions(-D_QUIZGRIND_BUILD_DAEMON)
  target_link_libraries(quizgrind libquizgrind_http)
  target_include_directories(quizgrind PUBLIC ${LIBQUIZGRIND_HTTP_INCLUDE_DIRS})
endif()

if(QUIZGRIND_BUILD_PDF)
  add_definitions(-D_QUIZGRIND_BUILD_PDF)
  target_link_libraries(quizgrind libquizgrind_pdf)
  target_include_directories(quizgrind PUBLIC ${LIBQUIZGRIND_PDF_INCLUDE_DIRS})
endif()

target_link_libraries(quizgrind libquizgrind)
target_include_directories(quizgrind PUBLIC 
	${CMAKE_CURRENT_SOURCE_DIR}
	${LIBQUIZGRIND_INCLUDE_DIRS})
