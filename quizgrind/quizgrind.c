/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "glib.h"

#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <pathbuf.h>
#include <libquizgrind/quizgrind.h>

#include "command/command.h"
#include "command/gen/gen.h"
#include "args.h"

char* QUIZGRIND_DATA_PATH = NULL;

#define OPT_PATH_PROJECT 'C'
#define OPT_HELP_ALL ARGS_LONGONLY(1)
#define OPT_VERSION ARGS_LONGONLY(2)

static command_abort abortfn;

static struct option_info opts[] = {
  {0, OPT_HELP, "help", NULL, "Show help and exit"},
  {0, OPT_VERSION, "version", NULL, "Show version info and exit"},
  {ARGS_ARGUMENT, OPT_PATH_PROJECT, NULL, "PATH", "Path to alternative project (default is working directory)"},
  {0}
};

struct command commands[] = 
{
#ifdef _QUIZGRIND_BUILD_DAEMON 
  {.name = "daemon", .hint = "Start the daemon"    , .fn = quizgrind_cmd_daemon_run, .abort = quizgrind_cmd_daemon_stop},
#endif
  {.name = "gen",    .hint = "Generate a quiz file", .fn = quizgrind_cmd_gen_run, .abort = quizgrind_cmd_gen_stop},
  {.name = "init",   .hint = "Start a new project", .fn = quizgrind_cmd_init_run, .abort = quizgrind_cmd_init_stop},
  {.name = "new",    .hint = "Create a new Exercise or Problem Set", .fn = quizgrind_cmd_new_run, .abort = quizgrind_cmd_new_stop},
  {0}
};

gboolean interrupt(gpointer user_data)
{
  if(abortfn)
    { abortfn(); }
  exit(0);
}

int cli_version()
{
  puts(QUIZGRIND_VERSION);
  return 0;
}

int quizgrind_cmd_help()
{
  printf(QUIZGRIND_VERSION "\nUsage: quizgrind ");
  args_show_short(opts);
  puts("<command> [<args>]\n\n"
        "Commands available:");
  args_show_commands(commands, 2);
  puts("For per-command help, run --help as an additional argument.\n\n"
       "General Options:");
  args_show_long(opts, 2);
  puts("\nPractice makes perfect!");
  return 0;
}

gboolean arg_parser (int key, char *arg, gpointer data)
{
  struct quizgrind_entry_t_* state = data;
  switch(key)
  {
    case OPT_PATH_PROJECT:
      state->path_base = path_new_from_str(arg, 0);
      break;
    case OPT_HELP:
      quizgrind_cmd_help();
      break;
    case OPT_VERSION:
      cli_version();
      break;
    default: return FALSE;
  }
  return TRUE;
}

void qg_exit()
  { interrupt(NULL); }

int main(int argc, char* argv[])
{
  struct quizgrind_entry_t_ entry = {0};

  atexit(qg_exit);

#ifdef _QUIZGRIND_PORTABLE_BUILD
  path_t* exec_path = NULL;
#ifdef G_OS_UNIX
  char* pathstr = g_file_read_link ("/proc/self/exe", NULL);

  if(!pathstr) { return 1; }
  exec_path = path_new_from_str(pathstr, 0);
  free(pathstr);
#endif

#ifdef G_OS_WIN32
  TCHAR szPath[MAX_PATH];
  if(!GetModuleFileName(NULL, szPath, MAX_PATH))
    { return 1; }

  exec_path = path_new_from_str(szPath, 0);
#endif

  path_pop(&exec_path);
  path_pop(&exec_path);
  path_push(&exec_path, "share");
  path_push(&exec_path, "quizgrind");
  path_make_absolute(&exec_path);

  QUIZGRIND_DATA_PATH = path_get_cstr(&exec_path);
#else
  QUIZGRIND_DATA_PATH = _QUIZGRIND_DATA_PATH;
#endif
  if(argc < 2)
    { exit(quizgrind_cmd_help()); }

  char** lastarg = args_parse(opts, arg_parser, &entry, &argc, argv);
  if(!lastarg) return 1;

  if(!entry.path_base)
    { entry.path_base = path_new_from_cwd(0); }

  int result = args_runCommand(commands, lastarg, argc, &entry, &abortfn);

  path_destroy(&entry.path_base);
  #ifdef _QUIZGRIND_PORTABLE_BUILD
  path_destroy(&exec_path);
  #endif
  return result;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
