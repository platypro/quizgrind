/* Aeden McClain (c) 2019-2022
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of QuizGrind.
 */

#include "args.h"

#include <stdio.h>
#include <string.h>

void args_show_short(struct option_info* opts)
{
  while(opts->id)
  {
    if(opts->name)
      printf("[--%s", opts->name);
    else printf("[-%c", opts->id);
    
    if(opts->flags & ARGS_ARGUMENT)
    {
      putchar(opts->name ? '=' : ' ');
      if(opts->flags & ARGS_OPTIONAL)
        putchar('[');
      
      printf("%s", opts->argname);
      
      if(opts->flags & ARGS_OPTIONAL)
        putchar(']');
    }

    printf("] ");
    opts++;
  }
}

void args_show_long(struct option_info* opts, int indent)
{
  while(opts->id)
  {
    int yat = 0;
    if(opts->name)
      yat += printf("%*c--%s", indent, ' ', opts->name);
    else
      yat += printf("%*c-%c", indent, ' ', opts->id);
    
    printf("%*c%s\n", ARGS_ALIGNAT - yat, ' ', opts->description);
    opts++;
  }
}

void args_show_commands(struct command* cmd, int indent)
{
  while(cmd->name)
  {
    int cmdlen = ARGS_ALIGNAT - strlen(cmd->name) - indent;
    printf("%*c%s%*c%s\n", indent, ' ', cmd->name, cmdlen, ' ', cmd->hint);
    cmd++;
  }
}

char** args_parse(struct option_info* opts, 
                 args_parser parser, 
                 gpointer udata, 
                 int* argc, char** argv)
{
  argv++;
  (*argc)--;
  while(*argc)
  {
    char *mark = *argv;
    struct option_info* opt;
    if(*mark == '-')
    {
      mark++;
    } else return argv;
    
    if(*mark == '-')
    {
      mark++;
      int optlen = 0;
      // Long option
      while(*mark) 
      {
        if(*(mark+optlen) == '='
        || *(mark+optlen) == '\000')
          break;
        optlen++;
      }
      opt = opts;
      while(opt && opt->id)
      {
        if(opt->name && !strncmp(mark, opt->name, optlen)) break;
        opt++;
      }
      if(opt && (opt->flags & ARGS_ARGUMENT))
      {
        if(*(mark+optlen) == '\000' && *argc > 1)
        {
          if(!parser(opt->id, argv[1], udata)) return argv;
          argv++;
          (*argc)--;
        }
        else if(!parser(opt->id, mark + optlen + 1, udata)) return argv;
      }
      else
        if(opt && !parser(opt->id, NULL, udata)) return argv;
    }
    else
    {
      //Parse options
      while(*mark)
      {
        opt = opts;
        while(opt && opt->id)
        {
          if(opt->id == *mark) break;
          opt++;
        }
        mark++;
        if(opt)
        {
          if(opt->flags & ARGS_ARGUMENT)
          {
            if(*mark == '\000' && *argc > 1)
            {
              if(!parser(opt->id, argv[1], udata)) return argv;
              argv++;
              (*argc)--;
            }
            else if(!parser(opt->id, mark, udata)) return argv;
            break;
          }
          else if(!parser(opt->id, NULL, udata)) return argv;
        }
      }
    }

    argv++;
    (*argc)--;
  }
  return NULL;
}

gboolean args_runCommand(struct command* commands,
                     char** lastarg,
                     int argc,
                     gpointer udata,
                     command_abort* abort
                    )
{
  struct command* cmd = commands;
  while(cmd->name)
  {
    if(!strcmp(*lastarg, cmd->name))
    {
      if(abort)
        { *abort = cmd->abort; }
      return cmd->fn(udata, argc, lastarg);
    }
    cmd++;
  }
  return 1;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
