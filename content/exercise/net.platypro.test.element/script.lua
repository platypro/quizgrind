wid = _mkWidget("element")

wid:add_text("an apple is a type of ")
text = wid:add_input_text()
	:add_answer("fruit", 2)
text.caps = true;

wid:add_text(". If a single apple costs $4.55, then four apples will cost $")
num = wid:add_input_number()
	num:formats_add(_NUM_TYPE.ENGINEERING)
	num:formats_clear()
	num:formats_add(_NUM_TYPE.BASIC)
	num:formats_add(_NUM_TYPE.NORMALIZED)
	num:formats_set_display(_NUM_TYPE.BASIC)
	num:add_answer(18.2, 0, 2)
num.mindp = 2;
num.mindp_penalty = 1;

wid:add_text(".")
