elem = _mkWidget("element")
elem:add_text("Test SI input (created using script)")
elem:add_input_number()
  :formats_add(_NUM_TYPE.EXPONENTIAL)
  :formats_clear()
  :formats_add(_NUM_TYPE.SI)
  :add_answer(12000.4, 0.1, 1)
