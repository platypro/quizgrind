-- QuizGrind script for exercise net.platypro.test.unirand

cnt = 5
uni = _unirand_seed(cnt)

str = "Unirand numbers:"

while cnt > 0 do
  str = str .. " " .. _unirand(uni, cnt)
  cnt = cnt - 1
end

_mkWidget("element")
  :add_text(str)
