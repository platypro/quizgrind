thisElement = _static[_variables.elementID];

local qText

if _options.missing == "number" then
  qText = "What is the atomic number of " .. thisElement.n .. "?"
  answer = math.floor(thisElement.a)
  thisElement.a = "???"
elseif _options.missing == "name" then
  qText = "What is the name of this element?"
  answer = thisElement.n
  thisElement.n = "???"
else
  error("Invalid option for missing ptable field!")
end

_mkWidget("element")
  :add_text(qText);

_mkWidget("image")
  :set_image_svg(_mkTemplate("default.svg", thisElement))

elem = _mkWidget("element")
elem:add_text("???=")

if _options["missing"] == "number" then
  elem:add_input_number()
    :add_answer(answer, 0, 1)
else
  elem:add_input_text()
    :add_answer(answer, 1)
end
