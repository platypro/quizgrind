-- QuizGrind script for exercise net.platypro.test.numparse

local testparse = function(value)
  parse = _number_parse(value)
  wid = _mkWidget("element")
  
  if parse.type == _NUM_TYPE.INVALID then
    wid:add_text(string.format("%s is invalid!", value))
  else
    wid:add_text(string.format("Test of %s: (type=%s, dp=%d, sigfig=%d value=%f)", value, _number_type_toString(parse.type), parse.dp, parse.sigfig, parse.value))
  end
end

testparse("12.5")
testparse("1.25k")
testparse("1.2e11")
testparse("12.x10^4")
testparse("12ss.5k")
testparse(".42M")
