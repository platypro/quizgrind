_mkWidget("element")
	:add_text("Multiple Choice by script (Choice 1 and red square are correct, but marks are only awarded if both are selected. Randomized.) ")

wid = _mkWidget("choice")
wid.numSelections = 2
wid.marks = 2
wid.randomize = true

wid:add_choice("Choice 1", 1)
wid:add_choice("Choice 2", 0)
wid:add_choice_svg(_mkTemplate("square.svg", {color="#FF0000"}), 1)
