local a = _variables["val_1"] + _options["min_1"]                 
local b = _variables["val_2"] + _options["min_2"]

function doWidget(ans, name, av, bv)
  elem = _mkWidget("element")
  elem:add_text(string.format("%d%s%d=", av, name, bv))
  elem:add_input_number()
    :add_answer(ans, 0, 1)
end

function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

if _options["format"] == "real" then
  a = a + round(math.random(), 2)
  b = b + round(math.random(), 2)
end

if _options["operation"] == "sub" then
  doWidget(a - b, "-", a, b)
  _mkHint("Subtraction is the opposite of addition!")
elseif _options["operation"] == "mul" then
  doWidget(a * b, "x", a, b);
  _mkHint("Multiplication is repeated addition!")
elseif _options["operation"] == "div" then
  doWidget(a, "÷", a*b, b);
  _mkHint("Division is the opposite of multiplication!")
else -- _options["operation"] == "add"
  doWidget(a + b, "+", a, b)
  _mkHint("Addition is repeated counting!")
end
