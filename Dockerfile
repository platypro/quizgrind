FROM alpine:latest AS base
WORKDIR /src

VOLUME /var/qg_project

RUN apk add cmake ninja gcc git pkgconfig libsoup-dev glib-dev libc-dev npm

COPY CMakeLists.txt /src/
COPY deps /src/deps
COPY libquizgrind /src/libquizgrind
COPY libquizgrind_http /src/libquizgrind_http
COPY libquizgrind_pdf /src/libquizgrind_pdf
COPY template /src/template
COPY quizgrind /src/quizgrind
COPY .git /src/.git

RUN mkdir /src/build && cd /src/build && cmake \
	-G"Ninja" \
	-DCMAKE_BUILD_TYPE=Release \
	-DQUIZGRIND_BUILD_PDF=OFF \
	-DQUIZGRIND_BUILD_DAEMON=ON \
	-DQUIZGRIND_PORTABLE_BUILD=OFF \
	-DQUIZGRIND_BUILD_BINARIES=ON \
	-DQUIZGRIND_BUILD_WEBAPP=ON \
	-DQUIZGRIND_OFFLINE_BUILD=OFF \
	-DQUIZGRIND_HOST_DOCS=ON \
	-DCMAKE_INSTALL_PREFIX=/usr \
	/src && \
	ninja install

FROM sphinxdoc/sphinx AS documentation
WORKDIR /docs
COPY docs /docs/

RUN pip3 install -r requirements.txt
RUN make html

FROM alpine:latest  
WORKDIR /

RUN apk --no-cache add libsoup glib shared-mime-info
COPY --from=base /usr/bin/quizgrind /usr/bin/quizgrind
COPY --from=base /usr/share/quizgrind /usr/share/quizgrind/
COPY --from=documentation /docs/build/html/ /usr/share/quizgrind/doc

CMD ["/usr/bin/quizgrind", "-C/var/qg_project", "daemon", "--html-template=/var/qg_project/quizgrind.html"]
EXPOSE 1245/tcp
