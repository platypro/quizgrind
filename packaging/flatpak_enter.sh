FLATPAK_ROOT=`dirname $0`/../../flatpak
FLATPAK_BUILD_ARGS="--socket=wayland --socket=x11 --socket=fallback-x11 --share=ipc --share=network --env=PATH=/usr/lib/sdk/node16/bin:/usr/bin $FLATPAK_ROOT" 

if (( $# != 0 )); then
	flatpak build $FLATPAK_BUILD_ARGS dbus-launch "$@"
else
	flatpak build $FLATPAK_BUILD_ARGS dbus-launch /bin/bash
fi
