FLATPAK_ROOT=`dirname $0`/../../flatpak

# Configure
mkdir -p $FLATPAK_ROOT
flatpak build-init $FLATPAK_ROOT net.platypro.QuizGrind org.gnome.Sdk/x86_64/41 org.gnome.Platform/x86_64/41 --sdk-extension=org.freedesktop.Sdk.Extension.node16
