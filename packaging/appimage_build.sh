export VERSION=0.2.0
APPIMG_ARCH=${ARCH:-x86_64}

REPO_ROOT=`dirname $0`/..

BUILD_DIR="$REPO_ROOT/../build"
APPDIR="$REPO_ROOT/../AppDir"

LINUXDEPLOY_EXE=`which linuxdeploy-$APPIMG_ARCH.AppImage`
LINUXDEPLOY_ARGS="--appdir=$APPDIR -e$APPDIR/usr/bin/quizgrind -i${REPO_ROOT}/libquizgrind_http/webapp/media/icon.svg -d${REPO_ROOT}/packaging/quizgrind.desktop"

cmake -S$REPO_ROOT -B$BUILD_DIR -DCMAKE_INSTALL_PREFIX=$APPDIR/usr
make -C$BUILD_DIR install

$LINUXDEPLOY_EXE --appimage-extract-and-run $LINUXDEPLOY_ARGS --output appimage
