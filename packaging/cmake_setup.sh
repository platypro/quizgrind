REPO_ROOT=`dirname $0`/..

BUILD_DIR="$REPO_ROOT/../build"
APPDIR="$REPO_ROOT/../AppDir"
FLATPAK_ROOT="$REPO_ROOT/../flatpak"

cmake -S$REPO_ROOT -B$BUILD_DIR -DCMAKE_INSTALL_PREFIX=$APPDIR/usr

